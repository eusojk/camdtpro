Site_characteristics: SC
Station:   ALLE                            
Startyear:   1988
Endyear:   2019
!
Season_characteritics:
Currentyear:   2018
Planting_month:    5
Harvest_month_plus:   10
!
!Seasonal_climate_forecasts:
Lead:    0
Startmonth:    5
Endmonth:    7
RAINFALL FORECAST
 Below_Normal:   22.00000    
 Near_Normal:   26.00000    
 Above_Normal:   52.00000    
FactorR(>=1):    1.0
TEMPERATURE FORECAST
 Below_Normal:   59.00000    
 Near_Normal:   35.00000    
 Above_Normal:   6.000000    
FactorR(>=1):    1.0
Iterations:           2
