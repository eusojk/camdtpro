Site_characteristics:
Station:   ALLE                            
Startyear:   1988
Endyear:   2019
!
Season_characteritics:
Currentyear:   2018
Planting_month:    5
Harvest_month_plus:   10
!
!Seasonal_climate_forecasts:
Lead:    0
Startmonth:    5
Endmonth:    7
RAINFALL FORECAST
 Below_Normal:  0.0000000E+00
 Near_Normal:   22.00000    
 Above_Normal:   78.00000    
FactorR(>=1):    1.0
TEMPERATURE FORECAST
 Below_Normal:   99.00000    
 Near_Normal:   1.000000    
 Above_Normal:  0.0000000E+00
FactorR(>=1):    1.0
Iterations:             10
