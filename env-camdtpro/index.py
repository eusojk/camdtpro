import pandas as pd
import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUiType

from FRE_Sampler import *
from dssat_scripts import setupDSSAT, updateImgPaths

Wdir = os.path.dirname(os.path.abspath(__file__))
wdirInp = Wdir + "\\inputs"

ui,_ = loadUiType('uis\\winMi.ui')

class MainApp(QMainWindow, ui):
    """"""
    fiInputs   = 'data\\inputsDefault.xlsx'
    fiToSave = 'data\\inputsToSave.xlsx'
    fiDefault = 'data\\defaultVars.xlsx'
    
    dfSave                = pd.DataFrame()
    dfSavingStp1      = pd.read_excel(fiToSave, sheet_name='setup1')
    dfSavingStp2      = pd.read_excel(fiToSave, sheet_name='setup2')
    dfSavingScen      = pd.read_excel(fiToSave, sheet_name='scenario')
    dfSavingDisp     = pd.read_excel(fiToSave, sheet_name='display')
    dfDefaultSetup1 = pd.read_excel(fiDefault, sheet_name='setup1')
    dfDefaultSetup2 = pd.read_excel(fiDefault, sheet_name='setup2')
    dfInpSetup1        = pd.read_excel(fiInputs, sheet_name='setup1')
    dfInpSetup2        = pd.read_excel(fiInputs, sheet_name='setup2')
    dfInpScenario      = pd.read_excel(fiInputs, sheet_name='scenario')
    dfInpDisplay       = pd.read_excel(fiInputs, sheet_name='display')
    
    scfChkBoxYear1 = []
    scfChkBoxYear2 = []
    pltChkBoxYear1 = []
    pltChkBoxYear2 = []
    
    fertAppList1 = []
    fertAppList2 = []
    fertAppList3 = []
    
    irrAutoList = []
    irrPerioList = []
    
    genMode = 1 # Generator used: 1 (WGEN) and 0 (FRESAMPLER)
    # The following are for viewing
    dssatMode1 = 0 # 0-seasonal, 1-historical, 2-seas vs. hist --Scenario 1
    dssatMode2 = 0 # 0-seasonal, 1-historical, 2-seas vs. hist --Scenario 2
    dssatMode3 = 0 # 0-seasonal, 1-historical, 2-seas vs. hist --Scenario 3
    
    # These are for running
    dssatRun1 = (0, '') # 0-seasonal, 1-historical, 2-seas vs. hist --Scenario 1
    dssatRun2 = 0 # 0-seasonal, 1-historical, 2-seas vs. hist --Scenario 2
    dssatRun3 = 0 # 0-seasonal, 1-historical, 2-seas vs. hist --Scenario 3
    
    scnMode = 1 # Scenario mode: 1, 2, 3
    needFert  = 0 # 0 for no fertilizer and 1 for
    numFert   = 0 # max is 3, 0 means no fertilizer
    numIrrig   = 0 # 0-No, 1-Auto, 2-Periodic

    def __init__(self):
        """Constructor"""
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.handleUIChanges()

        self.iniCheckBoxes()        
        self.iniSetup1()
        self.iniSetup2()

        self.handleButtons()        
        
    def handleUIChanges(self):
        """"""
        self.hideThemes()
        self.tabAllWidget.tabBar().setVisible(False)
        self.fresgroup.hide()
        
    def handleButtons(self):
        """"""
        self.btnHideThemeBar.clicked.connect(self.hideThemes)
        self.btnTabTheme.clicked.connect(self.showThemes)
        self.btnTabSetup1.clicked.connect(self.goToSetup1)
        self.btnTabSetup2.clicked.connect(self.goToSetup2)
        self.btnTabScenario.clicked.connect(self.goToScenario)
        self.btnTabDisplay.clicked.connect(self.goToDisplay)
        #self.btnTabDisplay.clicked.connect(self.setSetupFRE)
        
        self.radioButtonWGEN.clicked.connect(self.activateWGEN)
        self.radioButtonFRS.clicked.connect(self.activateFRESampler)
        self.radioButtonFRS.clicked.connect(self.makeWTDdir)
        
        self.comboBoxTabYear.currentIndexChanged.connect(self.whatYear)
        self.comboBoxTabYear.activated.connect(self.stackedWidgetSCF.setCurrentIndex)
        
        self.tableSCFYear1.itemChanged.connect(self.computeNN)
        
        self.tableSCFYear2.itemChanged.connect(self.computeNN)
        
        self.btnResetSCF1.clicked.connect(self.resetSCF)
        self.btnResetSCF2.clicked.connect(self.resetSCF)
        self.btnResetPlanting1.clicked.connect(self.resetPlanting)
        self.btnResetPlanting2.clicked.connect(self.resetPlanting)        
        self.btnResetBN1.clicked.connect(self.resetBN)
        self.btnResetTBN1.clicked.connect(self.resetBN)
        self.btnResetBN2.clicked.connect(self.resetBN)
        self.btnResetTBN2.clicked.connect(self.resetBN)
        self.btnResetAN1.clicked.connect(self.resetAN)
        self.btnResetTAN1.clicked.connect(self.resetAN)
        self.btnResetAN2.clicked.connect(self.resetAN)        
        self.btnResetTAN2.clicked.connect(self.resetAN)        
        self.btnComputeNN1.clicked.connect(self.setNN)        
        self.btnComputeTNN1.clicked.connect(self.setNN)        
        self.btnComputeNN2.clicked.connect(self.setNN)        
        self.btnComputeTNN2.clicked.connect(self.setNN)    
        
        self.optimizeFRE.clicked.connect(self.setSetupFRE)
        
        self.irrMethList.activated.connect(self.stackedWidgetIrr.setCurrentIndex)
        self.btnFertYes.clicked.connect(self.enableNumFert) 
        self.btnFertNo.clicked.connect(self.disableNumFert) 
        self.btnFert1.clicked.connect(self.enableNumFert1) 
        self.btnFert2.clicked.connect(self.enableNumFert2) 
        self.btnFert3.clicked.connect(self.enableNumFert3) 
        

        # This guarantees that the irri + fertilizer apps are saved into df
        self.irrMethList.currentIndexChanged.connect(self.whatIrrig)
        #self.btnTabSetup2.clicked.connect(self.saveSetup1) 
        #self.btnTabScenario.clicked.connect(self.saveSetup2) 
        
        # This saves each scenario paramter into df and enables the buttons
        self.nameScn1.textChanged.connect(self.activateBtnSc1)
        self.nameScn2.textChanged.connect(self.activateBtnSc2)
        self.nameScn3.textChanged.connect(self.activateBtnSc3)
        self.stepScn1.valueChanged.connect(self.activateBtnSc1)
        self.stepScn2.valueChanged.connect(self.activateBtnSc2)
        self.stepScn3.valueChanged.connect(self.activateBtnSc3)
        self.cmtScn1.textChanged.connect(self.activateBtnSc1)
        self.cmtScn2.textChanged.connect(self.activateBtnSc2)
        self.cmtScn3.textChanged.connect(self.activateBtnSc3)
        
        self.btnSeasScn1.clicked.connect(self.runDssatSeas1) 
        self.btnSeasScn2.clicked.connect(self.runDssatSeas2) 
        self.btnSeasScn3.clicked.connect(self.runDssatSeas3) 
        
        self.btnHistScn1.clicked.connect(self.runDssatHist1) 
        self.btnHistScn2.clicked.connect(self.runDssatHist2) 
        self.btnHistScn3.clicked.connect(self.runDssatHist3) 
        
        
        
        # the following buttons are to display the images/outputs
        self.resultsTabs.currentChanged.connect(self.setScenTab)
        
        self.btnViewHist_1.clicked.connect(self.setDssatHist1)
        self.btnViewHist_2.clicked.connect(self.setDssatHist2)
        self.btnViewHist_3.clicked.connect(self.setDssatHist3)
        self.btnViewSeas_1.clicked.connect(self.setDssatSeas1)
        self.btnViewSeas_2.clicked.connect(self.setDssatSeas2)
        self.btnViewSeas_3.clicked.connect(self.setDssatSeas3)
        self.btnViewComparison_1.clicked.connect(self.setDssatSeasHist1)
        self.btnViewComparison_2.clicked.connect(self.setDssatSeasHist2)
        self.btnViewComparison_3.clicked.connect(self.setDssatSeasHist3)
        
        self.btnViewYieldBox_1.clicked.connect(self.showYieldBox)
        self.btnViewYieldBox_2.clicked.connect(self.showYieldBox)
        self.btnViewYieldBox_3.clicked.connect(self.showYieldBox)
        
        self.btnViewYieldExc_1.clicked.connect(self.showYieldExc)
        self.btnViewYieldExc_2.clicked.connect(self.showYieldExc)
        self.btnViewYieldExc_2.clicked.connect(self.showYieldExc)
        
        self.btnViewMDATBox_1.clicked.connect(self.showMDATBox)
        self.btnViewMDATBox_2.clicked.connect(self.showMDATBox)
        self.btnViewMDATBox_3.clicked.connect(self.showMDATBox)
        
        self.btnViewMDATExc_1.clicked.connect(self.showMDATExc)
        self.btnViewMDATExc_2.clicked.connect(self.showMDATExc)
        self.btnViewMDATExc_3.clicked.connect(self.showMDATExc)
        
        self.btnViewADATBox_1.clicked.connect(self.showADATBox)
        self.btnViewADATBox_2.clicked.connect(self.showADATBox)
        self.btnViewADATBox_3.clicked.connect(self.showADATBox)
        
        self.btnViewADATExc_1.clicked.connect(self.showADATExc)
        self.btnViewADATExc_2.clicked.connect(self.showADATExc)
        self.btnViewADATExc_3.clicked.connect(self.showADATExc)
        
        self.btnViewCumRain_1.clicked.connect(self.showCumRain)
        self.btnViewCumRain_2.clicked.connect(self.showCumRain)
        self.btnViewCumRain_3.clicked.connect(self.showCumRain)
        
        self.btnViewNSTD_1.clicked.connect(self.showNitroStress)
        self.btnViewNSTD_2.clicked.connect(self.showNitroStress)
        self.btnViewNSTD_3.clicked.connect(self.showNitroStress)
        
        self.btnViewSRAD_1.clicked.connect(self.showSRAD)
        self.btnViewSRAD_2.clicked.connect(self.showSRAD)
        self.btnViewSRAD_3.clicked.connect(self.showSRAD)
        
        self.btnViewTMAX_1.clicked.connect(self.showTMAX)
        self.btnViewTMAX_2.clicked.connect(self.showTMAX)
        self.btnViewTMAX_3.clicked.connect(self.showTMAX)
        
        self.btnViewTMIN_1.clicked.connect(self.showTMIN)
        self.btnViewTMIN_2.clicked.connect(self.showTMIN)
        self.btnViewTMIN_3.clicked.connect(self.showTMIN)
        
        self.btnViewWSGD_1.clicked.connect(self.showWaterStress)
        self.btnViewWSGD_2.clicked.connect(self.showWaterStress)
        self.btnViewWSGD_3.clicked.connect(self.showWaterStress)
        
        self.btnRunAnalysis.clicked.connect(self.runAnalysis)
        self.resML1.clicked.connect(self.showAnalysis1)
        self.resML2.clicked.connect(self.showAnalysis2)
        
        
        
    def showThemes(self):
        """"""
        self.themeGroup.show()
        
    def hideThemes(self):
        """"""
        self.themeGroup.hide()
        
    ############## Connect the icons to the tabs ##################
    
    def goToSetup1(self):
        """"""
        self.tabAllWidget.setCurrentIndex(0)       
        
    def goToSetup2(self):
        """"""
        self.tabAllWidget.setCurrentIndex(1)    
        
    def goToScenario(self):
        """"""
        self.tabAllWidget.setCurrentIndex(2)       
        
    def goToDisplay(self):
        """"""
        self.tabAllWidget.setCurrentIndex(3)      
        
    ############## Setup1 parameters ####################
    def fillStations(self):
        """"""
        stations = list(self.dfDefaultSetup1['stations'])
        for st in stations:
            if isinstance(st, str): # drop 'nan' values
                self.WstationList.addItem(st)
            
        
    def fillCultivars(self):
        """"""
        cultivars = list(self.dfDefaultSetup1['cultivars'])
        for cu in cultivars:
            if isinstance(cu, str): # drop 'nan' values
                self.culTypeList.addItem(cu)        
        
    def fillIniH2O(self):
        """"""
        h2o = list(self.dfDefaultSetup1['h2o'])
        for h in h2o:
            if isinstance(h, str): # drop 'nan' values
                self.wetSoilList.addItem(h)  
        
    def fillIniNO3(self):
        """"""
        no3 = list(self.dfDefaultSetup1['no3'])
        for n in no3:
            if isinstance(n, str): # drop 'nan' values
                self.NO3SoilList.addItem(n)  
        
    def fillSoilType(self):
        """"""
        soils = list(self.dfDefaultSetup1['soils'])
        for so in soils:
            if isinstance(so, str): # drop 'nan' values
                self.soilTypeList.addItem(so)  
        
    def iniSetup1(self):
        """"""
        self.fillStations()
        self.fillCultivars()
        self.fillIniH2O()
        self.fillIniNO3()
        self.fillSoilType()
        self.fillSetup1Tabs()
        
    def iniSetup2(self):
        """"""     
        self.prepFertApp()
        self.fillIrrigAuto()
        self.fillIrrigPeriodic()
        
    ############## Prediction Horizon buttons: ##############
    
    def activateWGEN(self):
        self.genMode = 1
        self.chooseGen()
        
    
    def activateFRESampler(self):
        self.genMode = 0
        self.chooseGen()
        
    def chooseGen(self):
        """"""
        if self.genMode == 1:
                self.wgenModeView1.show()
                self.wgenModeView2.show()
                self.fresgroup.hide()
        # Switching modes ....
        elif self.genMode == 0:         
                self.wgenModeView1.hide()
                self.wgenModeView2.hide()
                self.fresgroup.show()
        
    
    def whatYear(self):
        """switch the table between year 1 and 2""" 
        year = self.comboBoxTabYear.currentText()
        #print(self.scfChkBoxYear1)
        #print(len(self.scfChkBoxYear1))
        return year
            
            
        
    
    def iniCheckBoxes(self):
        """"""     
        self.scfChkBoxYear1 = self.makeCheckBoxes(12, 'SCF')
        self.scfChkBoxYear2 = self.makeCheckBoxes(12, 'SCF')
        self.pltChkBoxYear1 = self.makeCheckBoxes(12, 'PLT')
        self.pltChkBoxYear2 = self.makeCheckBoxes(12, 'PLT')        
        
        self.fertAppList1 = self.fillFertApp()
        self.fertAppList2 = self.fillFertApp()
        self.fertAppList3 = self.fillFertApp()      
        
        self.irrAutoList = self.irrigAutoCmb()
        self.irrPerioList = self.irrigPerioCmb()
    
    def fillSetup1Tabs(self):
        """"""
        self.fillSCF()
        self.fillPlanting()
        self.fillBN(1, 1)
        self.fillAN(1, 1)
        self.setNNIni(1, 1)
        self.computeNN()
        
        
    def fillSCF(self):
        """"""
        for i in range(len(self.scfChkBoxYear1)):
            box1 = self.scfChkBoxYear1[i]
            box2 = self.scfChkBoxYear2[i]
            self.tableSCFYear1.setCellWidget(i, 0, box1)
            self.tableSCFYear2.setCellWidget(i, 0, box2)
            
        
      
    def fillPlanting(self):
        """""" 
        for i in range(len(self.pltChkBoxYear1)):
            box1 = self.pltChkBoxYear1[i]
            box2 = self.pltChkBoxYear2[i]
            self.tableSCFYear1.setCellWidget(i, 1, box1)
            self.tableSCFYear2.setCellWidget(i, 1, box2)    
    
    def fillBN(self, tab1=0, tab2=0):
        """"""
        
        if tab1 == 1:
            bn1List = list(self.dfInpSetup1['BN1'])
            tbn1List = list(self.dfInpSetup1['TBN1'])
            for i in range(len(bn1List )):
                bn1  = bn1List [i]
                tbn1 =  tbn1List [i]
                bnCell = QTableWidgetItem(str(bn1))
                tbnCell = QTableWidgetItem(str(tbn1))
                self.tableSCFYear1.setItem(i, 2, bnCell)
                self.tableSCFYear1.setItem(i, 5, tbnCell)
                
        if tab2 == 1:
            bn2List = list(self.dfInpSetup1['BN2'])
            tbn2List = list(self.dfInpSetup1['TBN2'])
            for i in range(len(bn2List )):
                bn2 = bn2List [i]
                tbn2 = tbn2List [i]
                bnCell = QTableWidgetItem(str(bn2))
                tbnCell = QTableWidgetItem(str(tbn2))
                self.tableSCFYear2.setItem(i, 2, bnCell)        
                self.tableSCFYear2.setItem(i, 5, tbnCell)        
            
        
 
    def fillAN(self, tab1=0, tab2=0):
        """"""     
        
        if tab1 == 1:
            an1List = list(self.dfInpSetup1['AN1'])
            tan1List = list(self.dfInpSetup1['TAN1'])
            for i in range(len(an1List )):
                an1 = an1List [i]
                tan1 = tan1List [i]
                bnCell = QTableWidgetItem(str(an1))
                tbnCell = QTableWidgetItem(str(tan1))
                self.tableSCFYear1.setItem(i, 3, bnCell)
                self.tableSCFYear1.setItem(i, 6, tbnCell)
                
        if tab2 == 1:
            an2List = list(self.dfInpSetup1['AN2'])
            tan2List = list(self.dfInpSetup1['TAN2'])
            for i in range(len(an2List )):
                an2 = an2List [i]
                tan2 = tan2List [i]
                bnCell = QTableWidgetItem(str(an2))
                tbnCell = QTableWidgetItem(str(tan2))
                self.tableSCFYear2.setItem(i, 3, bnCell) 
                self.tableSCFYear2.setItem(i, 6, tbnCell) 
        
    def resetSCF(self):
        """"""     
        year = self.whatYear()
        if year == 'Year 1':
            tab = self.scfChkBoxYear1
        else:
            tab = self.scfChkBoxYear2
            
        for cb in tab:
            cb.setChecked(False)        
            
                   
    def resetPlanting(self):
        """"""     
        year = self.whatYear()
        if year == 'Year 1':
            tab = self.pltChkBoxYear1
        else:
            tab = self.pltChkBoxYear2
            
        for cb in tab:
            cb.setChecked(False) 
        
    def resetBN(self):
        """"""     
        year = self.whatYear()
        if year == 'Year 1':
            self.fillBN(1, 0)
        else:
            self.fillBN(0, 1)
     
    def resetAN(self):
        """"""      
        year = self.whatYear()
        if year == 'Year 1':
            self.fillAN(1, 0)
        else:
            self.fillAN(0, 1)
        
    def computeNN(self):
        """"""
        year = self.whatYear()
        if year == 'Year 1':
            tab = self.tableSCFYear1
        else:
            tab = self.tableSCFYear2
            
        curItem = tab.currentItem()
        
        if curItem != None:
     
            curRow = tab.currentRow()
            curCol   = tab.currentColumn()
            curText   = curItem.text()
            
            colName = ''
            #print("Current Value {} at row {} and colmun {}".format(curText, curRow, curCol))
            
            if (curCol == 2 and year == 'Year 1'):
                colName = 'BN1'
                #col = 1
            elif (curCol == 2 and year == 'Year 2'):
                colName = 'BN2'
                #col = 2
            elif (curCol == 3 and year == 'Year 1'):
                colName = 'AN1'
                #col = 3
            elif (curCol == 3 and year == 'Year 2'):
                colName = 'AN2'
                #col = 4
            elif (curCol == 4 and year == 'Year 1'):
                #colName = 'AN2'
                #col = 4
                #tab = self.tableSCFYear1
                #self.makeNNUneditable(tab, curRow, curCol)
                pass
            elif (curCol == 5 and year == 'Year 1'):
                colName = 'TBN1'
                
            elif (curCol == 5 and year == 'Year 2'):
                colName = 'TBN2'
                 
            elif (curCol == 6 and year == 'Year 1'):
                colName = 'TAN1'
                
            elif (curCol == 6 and year == 'Year 2'):
                colName = 'TAN2'
                           
            else:
                print('Something wrong')
                print(type(curRow), curRow)
            
            try:           
                curInt =  int(curText) 
                #curInt = self.correctRange(curInt)
            except ValueError: # need to make a warning dialog here?
                #print("ValueError -  Got this: {} as input".format(curText))
                curInt = 33
                nnCell1 = QTableWidgetItem(str(curInt))
                tab.setItem(curRow, curCol,  nnCell1)                 
            finally:
                if  curInt > 100:
                    self.dfSavingStp1[colName].iloc[curRow] = 100
                elif curInt < 0:
                    self.dfSavingStp1[colName].iloc[curRow] = 0
                else:
                    self.dfSavingStp1[colName].iloc[curRow] = curInt                
            
            
        
    def calcNN(self, df):
        an = df['AN']
        bn = df['BN']
        nn = 100 - int(bn ) - int(an)
        if nn < 0:
            return 0
        elif nn > 100:
            return 100
        else:
            return nn
        
    def setNN(self):
        """"""
        year = self.whatYear()
        if year == 'Year 1':
            self.setNNIni(1,0)
        else:
            self.setNNIni(0,1)
        
        
    
    def setNNIni(self, tab1=0, tab2=0):
        """"""  
            
        if tab1 == 1:
            dfAB1 = self.dfSavingStp1[['AN1', 'BN1']]
            dfTAB1 = self.dfSavingStp1[['TAN1', 'TBN1']]
            dfAB1.columns = ['AN', 'BN']
            dfTAB1.columns = ['AN', 'BN']
            dfi1 = dfAB1.apply(  self.calcNN,  axis=1)
            dfi1t = dfTAB1.apply(  self.calcNN,  axis=1)
            #print(dfi1)
            # save the changes in dfSaving
            self.dfSavingStp1['NN1'] = dfi1
            self.dfSavingStp1['TNN1'] = dfi1t
            #print(self.dfSaving['NN1'] )
            
            nnList1 = list(dfi1)
            tnnList1 = list(dfi1t)
            for i in range(len(nnList1 )):
                nn1 = int(nnList1 [i])
                tnn1 = int(tnnList1 [i])
                nnCell1 = QTableWidgetItem(str(nn1))
                tnnCell1 = QTableWidgetItem(str(tnn1))
                nnCell1.setFlags(Qt.ItemIsEnabled)
                tnnCell1.setFlags(Qt.ItemIsEnabled)
                self.tableSCFYear1.setItem(i, 4,  nnCell1) 
                self.tableSCFYear1.setItem(i, 7,  tnnCell1) 
                
        if tab2 == 1:
            dfAB2 = self.dfSavingStp1[['AN2', 'BN2']]
            dfTAB2 = self.dfSavingStp1[['TAN2', 'TBN2']]
            dfAB2.columns = ['AN', 'BN']
            dfTAB2.columns = ['AN', 'BN']
            dfi2 = dfAB2.apply(  self.calcNN,  axis=1)
            dfi2t = dfTAB2.apply(  self.calcNN,  axis=1)
            
            self.dfSavingStp1['NN2'] = dfi2
            self.dfSavingStp1['TNN2'] = dfi2t
            
            nnList2 = list(dfi2)
            tnnList2 = list(dfi2t)
            for i in range(len(nnList2 )):
                nn2 = int(nnList2 [i])
                tnn2 = int(tnnList2 [i])
                nnCell2 = QTableWidgetItem(str(nn2))
                tnnCell2 = QTableWidgetItem(str(tnn2))
                nnCell2.setFlags(Qt.ItemIsEnabled)
                tnnCell2.setFlags(Qt.ItemIsEnabled)
                self.tableSCFYear2.setItem(i, 4,  nnCell2) 
                self.tableSCFYear2.setItem(i, 7,  tnnCell2)        
                
        
        
    def getSCF(self):
        """"""
        lstYears = []
        lstY1 = []
        lstY2 = []
        
        for cb1 in self.scfChkBoxYear1:
                state = cb1.isChecked()
                if (state):
                    lstY1.append(1)
                else:
                    lstY1.append(0)
                    
        for cb2 in self.scfChkBoxYear2:
            state = cb2.isChecked()
            if (state):
                lstY2.append(1)
            else:
                lstY2.append(0)
            
        lstYears.append(lstY1)
        lstYears.append(lstY2)
        
        #print (lstYears)
        return lstYears
            
    def getPlanting(self):
        """"""
        lstYears = []
        lstY1 = []
        lstY2 = []
        
        for cb1 in self.pltChkBoxYear1:
                state = cb1.isChecked()
                if (state):
                    lstY1.append(1)
                else:
                    lstY1.append(0)
                    
        for cb2 in self.pltChkBoxYear2:
            state = cb2.isChecked()
            if (state):
                lstY2.append(1)
            else:
                lstY2.append(0)
            
        lstYears.append(lstY1)
        lstYears.append(lstY2)
        
        #print (lstYears)
        return lstYears               
            
        
        
     ############ MISC #########
    def makeCheckBoxes(self, N, mode):
        """ Make N number of checkboxes"""
        
        boxes = []
        for i in range(N):
            box = QCheckBox()
            boxes.append(box)
        # In Michigan, May-June-July is by default for SCF
        if mode == 'SCF':
            boxes[4].setChecked(True)
            boxes[5].setChecked(True)
            boxes[6].setChecked(True)
        # In Michigan, May-June is by default for Planting    
        elif mode == 'PLT':
            boxes[4].setChecked(True)
            boxes[5].setChecked(True)            
        else:
            print('Error while baking the checkboxes')
            
        return boxes
    
        
    
    def prepFertApp(self):
        """"""
        tab = self.tableFert1
        cols = tab.columnCount() 
        rows =  tab.rowCount() 
        
        for col in range(cols):
            if col == 0:
                lst = self.fertAppList1
            elif col == 1:
                lst = self.fertAppList2
            elif col == 2:
                lst = self.fertAppList3
            else:
                print('Error... this is wrong')    
                
            for row in range(rows):
                item = lst[row]
                self.tableFert1.setCellWidget(row, col, item)
                  
    
    
    
    def fillFertApp(self):
        fertAppList = []
        
        cbDays = QSpinBox()
        cbAmn = QSpinBox()
        cbDays.setMaximum(200)
        cbAmn.setMaximum(200)
        cbMat  = self.fertMatOptions()
        cbMeth = self.fertMethOptions()  
        
        fertAppList.append(cbDays)
        fertAppList.append(cbAmn)
        fertAppList.append(cbMat)
        fertAppList.append(cbMeth)
        
        return fertAppList
    
    def  getFertApps(self):
        """"""
    
        fertAppList = []
        # retrieve the values in the fertilizer app table
        for i in range(3):
            
            if i == 0:      # Application 1
                lst = self.fertAppList1
            elif i == 1:   # Application 2
                lst = self.fertAppList2      
            elif i == 2:   # Application 3
                lst = self.fertAppList3      
                
            lsta = []    
            
            for index, item in enumerate(lst):
                
                if index < 2: #QSpinBox
                    val = item.value()
                else: # QComboBox string
                    val = item.currentText()
                    if val != 'None':
                        val = val[:5]
                lsta.append(val)
            
            
            fertAppList.append(lsta)
            
        #print(fertAppList)
                
        return fertAppList
                
        
    
    def fillIrrigAuto(self):
        tab = self.automaticIrrTable
        for i in range(len(self.irrAutoList)):
            item = self.irrAutoList[i]
            self.automaticIrrTable.setCellWidget(i, 0, item)
        
    def fillIrrigPeriodic(self):
        tab = self.periodicIrrTable   
        for i in range(len(self.irrPerioList)):
            item = self.irrPerioList[i]
            self.periodicIrrTable.setCellWidget(i, 0, item) 
    
    def correctRange(self, val):
        if val < 0 or val > 99:
            return 0
        
    def enableNumFert(self):
        """"""
        self.needFert = 1
        self.groupBoxNumApp.setEnabled(True)
        self.fertView0.hide()
        self.fertView2.show()
        self.fertView3.show()
        
        
    def disableNumFert(self):
        """"""    
        self.needFert = 0
        self.groupBoxNumApp.setEnabled(False)
        self.fertView0.show()
        
    def showFertApp(self):
        """"""
        if self.numFert == 0:
            self.fertView0.show()
        elif self.numFert == 1:
            self.fertView1.hide()
            self.fertView2.show()
            self.fertView3.show()
        elif self.numFert == 2:
            self.fertView1.hide()
            self.fertView2.hide()
            self.fertView3.show()
        elif self.numFert == 3:
            self.fertView1.hide()
            self.fertView2.hide()
            self.fertView3.hide()
            
    def enableNumFert1(self):
        """"""
        self.numFert   = 1
        self.showFertApp()
        
    def enableNumFert2(self):
        """"""
        self.numFert   = 2  
        self.showFertApp()  
        
    def enableNumFert3(self):
        """"""
        self.numFert   = 3 
        self.showFertApp()      
        
        
           
        
    def fertMatOptions(self):
        """"""
        combo = QComboBox()
        matList = list(self.dfDefaultSetup2['fertMat'])
        for item in matList:
            if isinstance(item, str):
                combo.addItem(item)
        
        return combo
    
    def fertMethOptions(self):
        """"""    
        combo = QComboBox()
        matList = list(self.dfDefaultSetup2['fertMeth'])
        for item in matList:
            if isinstance(item, str):
                combo.addItem(item)
            
        return combo
    
    def fertDigOptions(self):
        """""" 
        cmbList = []
        for i in range(3):
            cb = QSpinBox()
            cmbList.append(cb)
        return cmbList
    
    def irrigAutoCmb(self):
        """"""
        cmbList = []
        for i in range(3):
            cb = QDoubleSpinBox()
            cmbList.append(cb)
        return cmbList        
        
    def irrigPerioCmb(self):
        """"""
        cmbList = []
        firstIrrDate = QSpinBox()
        irrDepth = QDoubleSpinBox()
        interval = QSpinBox()
        endOfApp = QSpinBox()
        appEff = QDoubleSpinBox()
        
        cmbList.append(firstIrrDate)
        cmbList.append(irrDepth)
        cmbList.append(interval)
        cmbList.append(endOfApp)
        cmbList.append(appEff)
        
        return cmbList
    
    def whatIrrig(self):
        """"""
        meth = self.irrMethList.currentText()
        if meth == 'No':
            self.numIrrig = 0
        elif meth == 'Automatic':
            self.numIrrig = 1
            
        elif meth == 'Periodic':
            self.numIrrig = 2      
              
        # store these in df
        self.dfSavingStp2['Values'].iloc[14] = self.numIrrig # IRbutton
        

    def storeIrrigAuto(self):
        """"""
        deb = 0
        for i in range(15, 18) :
            item = self.irrAutoList[deb]
            value = item.value()
            self.dfSavingStp2['Values'].iloc[i] = value
            deb += 1
            
    def storeIrrigPeriodic(self):
        """"""   
        deb = 0
        for i in range(18, 23) :
            item = self.irrPerioList[deb]
            value = item.value()
            self.dfSavingStp2['Values'].iloc[i] = value
            deb += 1 
    
    
    def saveSetups12(self):
        self.saveSetup1()
        self.saveSetup2()
    
    def saveSetup1(self):
        """"""
        scfStates = self.getSCF()
        pltStates = self.getPlanting()
        
        self.dfSavingStp1['Values'].iloc[0] = self.WstationList.currentText()[:4].upper() 
        self.dfSavingStp1['Values'].iloc[1] = int( self.startYear.sectionText(QDateEdit.YearSection) )
        self.dfSavingStp1['Values'].iloc[2] = self.culTypeList.currentText()
        self.dfSavingStp1['Values'].iloc[3] = self.pltDensity.value()
        self.dfSavingStp1['Values'].iloc[4] = self.soilTypeList.currentText().split('/')[1]
        self.dfSavingStp1['Values'].iloc[5] = float(self.wetSoilList.currentText()[:3])
        self.dfSavingStp1['Values'].iloc[6] = self.NO3SoilList.currentText()[0]
        self.dfSavingStp1['Values'].iloc[7] = self.genMode
        
        self.dfSavingStp1['SCF1'] = scfStates[0]
        self.dfSavingStp1['SCF2'] = scfStates[1]
        self.dfSavingStp1['PLT1'] = pltStates[0]
        self.dfSavingStp1['PLT2'] = pltStates[1]        
        
        #print(self.dfSavingStp1['SCF1'] )
        
    def saveSetup2(self):
        """"""
        self.dfSavingStp2['Values'].iloc[0] = self.needFert # Fbutton1
        self.dfSavingStp2['Values'].iloc[1] = self.numFert # nfertilizer
        
        # store the values in the dataframe
        fertAppList = self.getFertApps()
        # Fertilizer
        for i in range(len(fertAppList)):
            if i == 0:
                start = 2
            elif i ==1:
                start = 3
            elif i == 2:
                start = 4
                
            stop = start + 9  
            deb = 0
            lsta = fertAppList[i]
            for i2 in range(start, stop+1, 3):
                self.dfSavingStp2['Values'].iloc[i2] = lsta[deb]
                deb += 1        
        
        # Irrigation
        if self.numIrrig == 1:
            self.storeIrrigAuto()
        elif self.numIrrig == 2:
            self.storeIrrigPeriodic()
            
            
        #print(self.dfSavingStp2['Values'])
        
     
    def setSetupFRE(self):
        """"""
        self.saveSetups12()
        setupFRE(self.dfSavingStp1)
        
        
        
    def viewOutputs(self, mode):
        """"""
        pass
    
    def viewHistorical(self):
        """"""
        pass
    
    def runDssatHist1(self):
        """ Run Hist for scenario 1
        """
        self.saveSetups12()
        self.dssatRun1 = 1
        dfList = self.getDfList()
        scfStates = self.getSCF()
        pltStates = self.getPlanting()
        states = [scfStates, pltStates]
        setupDSSAT(dfList,  states, self.dssatRun1, "H")
        self.saveImgHist(self.dssatRun1)
        
    
    def runDssatHist2(self):
        """ Run Hist for scenario 2
        """
        self.saveSetups12()
        self.dssatRun2 = 2
        dfList = self.getDfList()
        scfStates = self.getSCF()
        pltStates = self.getPlanting()
        states = [scfStates, pltStates]
        setupDSSAT(dfList,  states, self.dssatRun2 , "H")
        self.saveImgHist(self.dssatRun2 )
    
    def runDssatHist3(self):
        """ Run Hist for scenario 3
        """
        self.saveSetups12()
        self.dssatRun3 = 1
        dfList = self.getDfList()
        scfStates = self.getSCF()
        pltStates = self.getPlanting()
        states = [scfStates, pltStates]
        setupDSSAT(dfList,  states, self.dssatRun3 , "H")
        self.saveImgHist(self.dssatRun3)
        
    def runDssatSeas1(self):
        """ Run Seas for scenario 1
        """
        self.saveSetups12()
        self.dssatRun1 = 1
        dfList = self.getDfList()
        scfStates = self.getSCF()
        pltStates = self.getPlanting()
        states = [scfStates, pltStates]
        setupDSSAT(dfList,  states, self.dssatRun1, "S") 
        self.saveImgSeas(self.dssatRun1)
        
    def runDssatSeas2(self):
        """ Run Seas for scenario 2
        """
        self.saveSetups12()
        self.dssatRun2 = 2
        dfList = self.getDfList()
        scfStates = self.getSCF()
        pltStates = self.getPlanting()
        states = [scfStates, pltStates]
        setupDSSAT(dfList,  states, self.dssatRun2, "S") 
        self.saveImgSeas(self.dssatRun2)
        
    def runDssatSeas3(self):
        """ Run Seas for scenario 3
        """
        self.saveSetups12()
        self.dssatRun3 = 3
        dfList = self.getDfList()
        scfStates = self.getSCF()
        pltStates = self.getPlanting()
        states = [scfStates, pltStates]
        setupDSSAT(dfList,  states, self.dssatRun3, "S") 
        self.saveImgSeas(self.dssatRun3)
        
    def saveImgHist(self, scenario):
        """"""
        code = 'ValuesHistorical'
        if scenario == 1:
            picyieldBox_1 = QPixmap( self.dfSavingDisp[code].iloc[0] )
            picyieldExc_1 = QPixmap( self.dfSavingDisp[code].iloc[1] )
            picMDATBox_1 = QPixmap( self.dfSavingDisp[code].iloc[2] )
            picMDATExc_1 = QPixmap( self.dfSavingDisp[code].iloc[3] )
            picADATBox_1 = QPixmap( self.dfSavingDisp[code].iloc[4] )
            picADATExc_1 = QPixmap( self.dfSavingDisp[code].iloc[5] )
            picCumRain_1 = QPixmap( self.dfSavingDisp[code].iloc[6] )
            picNSTD_1 = QPixmap( self.dfSavingDisp[code].iloc[7] )
            picSRAD_1 = QPixmap( self.dfSavingDisp[code].iloc[8] )
            picTMAX_1 = QPixmap( self.dfSavingDisp[code].iloc[9] )
            picTMIN_1 = QPixmap( self.dfSavingDisp[code].iloc[10] )
            picWSGD_1 = QPixmap( self.dfSavingDisp[code].iloc[11] )
            
            self.picyieldBox_1.setPixmap(picyieldBox_1)
            self.picyieldExc_1.setPixmap(picyieldExc_1)
            self.picMDATBox_1.setPixmap(picMDATBox_1)
            self.picMDATExc_1.setPixmap(picMDATExc_1)
            self.picADATBox_1.setPixmap(picADATBox_1)
            self.picADATExc_1.setPixmap(picADATExc_1)
            self.picCumRain_1.setPixmap(picCumRain_1)
            self.picNSTD_1.setPixmap(picNSTD_1)
            self.picSRAD_1.setPixmap(picSRAD_1)
            self.picTMAX_1.setPixmap(picTMAX_1)
            self.picTMIN_1.setPixmap(picTMIN_1)
            self.picWSGD_1.setPixmap(picWSGD_1)
            
            
        elif scenario == 2:
            picyieldBox_2 = QPixmap( self.dfSavingDisp[code].iloc[12] )
            picyieldExc_2 = QPixmap( self.dfSavingDisp[code].iloc[13] )
            picMDATBox_2 = QPixmap( self.dfSavingDisp[code].iloc[14] )
            picMDATExc_2 = QPixmap( self.dfSavingDisp[code].iloc[15] )
            picADATBox_2 = QPixmap( self.dfSavingDisp[code].iloc[16] )
            picADATExc_2 = QPixmap( self.dfSavingDisp[code].iloc[17] )
            picCumRain_2 = QPixmap( self.dfSavingDisp[code].iloc[18] )
            picNSTD_2 = QPixmap( self.dfSavingDisp[code].iloc[19] )
            picSRAD_2 = QPixmap( self.dfSavingDisp[code].iloc[20] )
            picTMAX_2 = QPixmap( self.dfSavingDisp[code].iloc[21] )
            picTMIN_2 = QPixmap( self.dfSavingDisp[code].iloc[22] )
            picWSGD_2 = QPixmap( self.dfSavingDisp[code].iloc[23] )
            
            self.picyieldBox_2.setPixmap(picyieldBox_2)
            self.picyieldExc_2.setPixmap(picyieldExc_2)
            self.picMDATBox_2.setPixmap(picMDATBox_2)
            self.picMDATExc_2.setPixmap(picMDATExc_2)
            self.picADATBox_2.setPixmap(picADATBox_2)
            self.picADATExc_2.setPixmap(picADATExc_2)
            self.picCumRain_2.setPixmap(picCumRain_2)
            self.picNSTD_2.setPixmap(picNSTD_2)
            self.picSRAD_2.setPixmap(picSRAD_2)
            self.picTMAX_2.setPixmap(picTMAX_2)
            self.picTMIN_2.setPixmap(picTMIN_2)
            self.picWSGD_2.setPixmap(picWSGD_2)
            
            
        elif scenario == 3:
            picyieldBox_3 = QPixmap( self.dfSavingDisp[code].iloc[24] )
            picyieldExc_3 = QPixmap( self.dfSavingDisp[code].iloc[25] )
            picMDATBox_3 = QPixmap( self.dfSavingDisp[code].iloc[26] )
            picMDATExc_3 = QPixmap( self.dfSavingDisp[code].iloc[27] )
            picADATBox_3 = QPixmap( self.dfSavingDisp[code].iloc[28] )
            picADATExc_3 = QPixmap( self.dfSavingDisp[code].iloc[29] )
            picCumRain_3 = QPixmap( self.dfSavingDisp[code].iloc[30] )
            picNSTD_3 = QPixmap( self.dfSavingDisp[code].iloc[31] )
            picSRAD_3 = QPixmap( self.dfSavingDisp[code].iloc[32] )
            picTMAX_3 = QPixmap( self.dfSavingDisp[code].iloc[33] )
            picTMIN_3 = QPixmap( self.dfSavingDisp[code].iloc[34] )
            picWSGD_3 = QPixmap( self.dfSavingDisp[code].iloc[35] )
            
            self.picyieldBox_3.setPixmap(picyieldBox_3)
            self.picyieldExc_3.setPixmap(picyieldExc_3)
            self.picMDATBox_3.setPixmap(picMDATBox_3)
            self.picMDATExc_3.setPixmap(picMDATExc_3)
            self.picADATBox_3.setPixmap(picADATBox_3)
            self.picADATExc_3.setPixmap(picADATExc_3)
            self.picCumRain_3.setPixmap(picCumRain_3)
            self.picNSTD_3.setPixmap(picNSTD_3)
            self.picSRAD_3.setPixmap(picSRAD_3)
            self.picTMAX_3.setPixmap(picTMAX_3)
            self.picTMIN_3.setPixmap(picTMIN_3)
            self.picWSGD_3.setPixmap(picWSGD_3)
        
        
        else:
            print('Empty - Iames paths')
        
    def saveImgSeas(self, scenario):
        """"""
        code = 'ValuesSeasonal'
        if scenario == 1:
            picyieldBox_1 = QPixmap( self.dfSavingDisp[code].iloc[0] )
            picyieldExc_1 = QPixmap( self.dfSavingDisp[code].iloc[1] )
            picMDATBox_1 = QPixmap( self.dfSavingDisp[code].iloc[2] )
            picMDATExc_1 = QPixmap( self.dfSavingDisp[code].iloc[3] )
            picADATBox_1 = QPixmap( self.dfSavingDisp[code].iloc[4] )
            picADATExc_1 = QPixmap( self.dfSavingDisp[code].iloc[5] )
            picCumRain_1 = QPixmap( self.dfSavingDisp[code].iloc[6] )
            picNSTD_1 = QPixmap( self.dfSavingDisp[code].iloc[7] )
            picSRAD_1 = QPixmap( self.dfSavingDisp[code].iloc[8] )
            picTMAX_1 = QPixmap( self.dfSavingDisp[code].iloc[9] )
            picTMIN_1 = QPixmap( self.dfSavingDisp[code].iloc[10] )
            picWSGD_1 = QPixmap( self.dfSavingDisp[code].iloc[11] )
            
            self.picyieldBox_1.setPixmap(picyieldBox_1)
            self.picyieldExc_1.setPixmap(picyieldExc_1)
            self.picMDATBox_1.setPixmap(picMDATBox_1)
            self.picMDATExc_1.setPixmap(picMDATExc_1)
            self.picADATBox_1.setPixmap(picADATBox_1)
            self.picADATExc_1.setPixmap(picADATExc_1)
            self.picCumRain_1.setPixmap(picCumRain_1)
            self.picNSTD_1.setPixmap(picNSTD_1)
            self.picSRAD_1.setPixmap(picSRAD_1)
            self.picTMAX_1.setPixmap(picTMAX_1)
            self.picTMIN_1.setPixmap(picTMIN_1)
            self.picWSGD_1.setPixmap(picWSGD_1)
            
            
        elif scenario == 2:
            picyieldBox_2 = QPixmap( self.dfSavingDisp[code].iloc[12] )
            picyieldExc_2 = QPixmap( self.dfSavingDisp[code].iloc[13] )
            picMDATBox_2 = QPixmap( self.dfSavingDisp[code].iloc[14] )
            picMDATExc_2 = QPixmap( self.dfSavingDisp[code].iloc[15] )
            picADATBox_2 = QPixmap( self.dfSavingDisp[code].iloc[16] )
            picADATExc_2 = QPixmap( self.dfSavingDisp[code].iloc[17] )
            picCumRain_2 = QPixmap( self.dfSavingDisp[code].iloc[18] )
            picNSTD_2 = QPixmap( self.dfSavingDisp[code].iloc[19] )
            picSRAD_2 = QPixmap( self.dfSavingDisp[code].iloc[20] )
            picTMAX_2 = QPixmap( self.dfSavingDisp[code].iloc[21] )
            picTMIN_2 = QPixmap( self.dfSavingDisp[code].iloc[22] )
            picWSGD_2 = QPixmap( self.dfSavingDisp[code].iloc[23] )
            
            self.picyieldBox_2.setPixmap(picyieldBox_2)
            self.picyieldExc_2.setPixmap(picyieldExc_2)
            self.picMDATBox_2.setPixmap(picMDATBox_2)
            self.picMDATExc_2.setPixmap(picMDATExc_2)
            self.picADATBox_2.setPixmap(picADATBox_2)
            self.picADATExc_2.setPixmap(picADATExc_2)
            self.picCumRain_2.setPixmap(picCumRain_2)
            self.picNSTD_2.setPixmap(picNSTD_2)
            self.picSRAD_2.setPixmap(picSRAD_2)
            self.picTMAX_2.setPixmap(picTMAX_2)
            self.picTMIN_2.setPixmap(picTMIN_2)
            self.picWSGD_2.setPixmap(picWSGD_2)
            
            
        elif scenario == 3:
            picyieldBox_3 = QPixmap( self.dfSavingDisp[code].iloc[24] )
            picyieldExc_3 = QPixmap( self.dfSavingDisp[code].iloc[25] )
            picMDATBox_3 = QPixmap( self.dfSavingDisp[code].iloc[26] )
            picMDATExc_3 = QPixmap( self.dfSavingDisp[code].iloc[27] )
            picADATBox_3 = QPixmap( self.dfSavingDisp[code].iloc[28] )
            picADATExc_3 = QPixmap( self.dfSavingDisp[code].iloc[29] )
            picCumRain_3 = QPixmap( self.dfSavingDisp[code].iloc[30] )
            picNSTD_3 = QPixmap( self.dfSavingDisp[code].iloc[31] )
            picSRAD_3 = QPixmap( self.dfSavingDisp[code].iloc[32] )
            picTMAX_3 = QPixmap( self.dfSavingDisp[code].iloc[33] )
            picTMIN_3 = QPixmap( self.dfSavingDisp[code].iloc[34] )
            picWSGD_3 = QPixmap( self.dfSavingDisp[code].iloc[35] )
            
            self.picyieldBox_3.setPixmap(picyieldBox_3)
            self.picyieldExc_3.setPixmap(picyieldExc_3)
            self.picMDATBox_3.setPixmap(picMDATBox_3)
            self.picMDATExc_3.setPixmap(picMDATExc_3)
            self.picADATBox_3.setPixmap(picADATBox_3)
            self.picADATExc_3.setPixmap(picADATExc_3)
            self.picCumRain_3.setPixmap(picCumRain_3)
            self.picNSTD_3.setPixmap(picNSTD_3)
            self.picSRAD_3.setPixmap(picSRAD_3)
            self.picTMAX_3.setPixmap(picTMAX_3)
            self.picTMIN_3.setPixmap(picTMIN_3)
            self.picWSGD_3.setPixmap(picWSGD_3)
        
        
        else:
            print('Empty - Iames paths')
        
        
        
     
    def getDfList(self):
        la = []
        la.append(self.dfSavingStp1)
        la.append(self.dfSavingStp2)
        la.append(self.dfSavingScen)
        la.append(self.dfSavingDisp)
        
        return la
        
    def activateBtnSc1(self):
        """"""
        scName1 = self.nameScn1.text()
        if len(scName1) == 4:
            scStep1 = self.stepScn1.value()
            scCmt1 = self.cmtScn1.toPlainText()
            self.dfSavingScen['Values'].iloc[0] = scName1
            self.dfSavingScen['Values'].iloc[3] = scStep1
            self.dfSavingScen['Values'].iloc[6] = scCmt1
            
            self.stepScn1.setEnabled(True)
            self.cmtScn1.setEnabled(True)
            self.btnSeasScn1.setEnabled(True)
            self.btnHistScn1.setEnabled(True)
        else:
            self.stepScn1.setEnabled(False)
            self.cmtScn1.setEnabled(False)
            self.btnSeasScn1.setEnabled(False)
            self.btnHistScn1.setEnabled(False)         
        
    def activateBtnSc2(self):
        """"""
        scName2 = self.nameScn2.text()
        if len(scName2) == 4:
            scStep2 = self.stepScn2.value()
            scCmt2 = self.cmtScn2.toPlainText()
            self.dfSavingScen['Values'].iloc[1] = scName2
            self.dfSavingScen['Values'].iloc[4] = scStep2
            self.dfSavingScen['Values'].iloc[7] = scCmt2
            
            self.stepScn2.setEnabled(True)
            self.cmtScn2.setEnabled(True)
            self.btnSeasScn2.setEnabled(True)
            self.btnHistScn2.setEnabled(True)
        else:
            self.stepScn2.setEnabled(False)
            self.cmtScn2.setEnabled(False)
            self.btnSeasScn2.setEnabled(False)
            self.btnHistScn2.setEnabled(False)     
        
    def activateBtnSc3(self):
        """"""
        scName3 = self.nameScn3.text()
        if len(scName3) == 4:
            scStep3 = self.stepScn3.value()
            scCmt3 = self.cmtScn3.toPlainText()
            self.dfSavingScen['Values'].iloc[2] = scName3
            self.dfSavingScen['Values'].iloc[5] = scStep3
            self.dfSavingScen['Values'].iloc[8] = scCmt3
            
            self.stepScn3.setEnabled(True)
            self.cmtScn3.setEnabled(True)
            self.btnSeasScn3.setEnabled(True)
            self.btnHistScn3.setEnabled(True)
        else:
            self.stepScn3.setEnabled(False)
            self.cmtScn3.setEnabled(False)
            self.btnSeasScn3.setEnabled(False)
            self.btnHistScn3.setEnabled(False)  
    
    def viewSeasonal(self):
        """"""
        pass
    
    def viewSeasHist(self):
        """"""
        pass
    
    def activateView(self, code):
        """
        code: scenario mode: 1,2,3
        """
        
        # first check which scenario display tab we currently are in
        if code == 1:
            mode = self.dssatMode1
        elif code == 2:
            mode = self.dssatMode2
        elif code == 3:
            mode = self.dssatMode3
        
        # now based on the scenario, activate the corresponding view:    
        if mode == 0: # Seasonal 
            pass
        elif mode == 1: # Hist 
            pass
        elif mode == 2: # Seasonal vs. Hist
            pass
            
    def setScenTab(self):
        """"""
        tab = self.resultsTabs.currentIndex()
        self.scnMode = tab + 1
        #print("Scenario: ", self.scnMode)
        
    def getDssatMode1(self):
        """"""
        print("Scn1 Dssat mode is: ", self.dssatMode1)
        return self.dssatMode1
    
    def getDssatMode2(self):
        """"""
        print("Scn2 Dssat mode is: ", self.dssatMode2)
        return self.dssatMode2   
    
    def getDssatMode3(self):
        """"""
        print("Scn3 Dssat mode is: ", self.dssatMode3)
        return self.dssatMode3 
    
    def setDssatHist1(self):
        """"""
        self.dssatMode1 = 1
        self.activateViewButtons()
        
    def setDssatSeas1(self):
        """"""
        self.dssatMode1 = 0   
        self.activateViewButtons()
        
    def setDssatSeasHist1(self):
        """"""
        self.dssatMode1 = 2     
    
    def setDssatHist2(self):
        """"""
        self.dssatMode2 = 1
        self.activateViewButtons()
        
    def setDssatSeas2(self):
        """"""
        self.dssatMode2 = 0   
        self.activateViewButtons()
        
    def setDssatSeasHist2(self):
        """"""
        self.dssatMode2 = 2 
    
    def setDssatHist3(self):
        """"""
        self.dssatMode3 = 1
        self.activateViewButtons()
        
    def setDssatSeas3(self):
        """"""
        self.dssatMode3 = 0   
        self.activateViewButtons()
        
    def setDssatSeasHist3(self):
        """"""
        self.dssatMode3 = 2 
        
    
    def activateViewButtons(self):
        if self.scnMode == 1:
            #self.getDssatMode1()
            self.groupBoxDisplayBtns_1.setEnabled(True)
        elif self.scnMode == 2:
            #self.getDssatMode2()
            self.groupBoxDisplayBtns_2.setEnabled(True)
        elif self.scnMode == 3:
            #self.getDssatMode3()
            self.groupBoxDisplayBtns_3.setEnabled(True)
            
    def deactivateViewButtons(self):
        """
        When the "HIst. vs. Seas" button is clicked
        """
        self.groupBoxDisplayBtns_1.setEnabled(False)
        self.groupBoxDisplayBtns_2.setEnabled(False)
        self.groupBoxDisplayBtns_3.setEnabled(False)        

    # All display buttons
    def showYieldBox(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(1)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(1)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(1)
        else:
            print("Bad Error - Yieldbox plot")
    
    def showYieldExc(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(2)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(2)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(3)
        else:
            print("Bad Error - Yieldbox Exceedence")

    def showMDATBox(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(3)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(3)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(3)
        else:
            print("Bad Error - MDAT plot")
    
    def showMDATExc(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(4)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(4)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(4)
        else:
            print("Bad Error - MDAT Exceedence")

    def showADATBox(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(5)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(5)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(5)
        else:
            print("Bad Error - ADAT plot")
    
    def showADATExc(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(6)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(6)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(6)
        else:
            print("Bad Error - ADAT Exceedence")
            
    def showCumRain(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(7)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(7)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(7)
        else:
            print("Bad Error - Cumulative precipitation")
        
    def showNitroStress(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(8)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(8)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(8)
        else:
            print("Bad Error - Nitrogen Stress")
            
    def showSRAD(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(9)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(9)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(9)
        else:
            print("Bad Error - SRAD")
            
    def showTMAX(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(10)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(10)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(10)
        else:
            print("Bad Error - TMAX")
        
    def showTMIN(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(11)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(11)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(11)
        else:
            print("Bad Error - TMIN")
            
    def showWaterStress(self):
        """"""
        #print(self.scnMode)
        if self.scnMode == 1:
            self.stackedWidgetHistorical_1.setCurrentIndex(12)
        elif self.scnMode == 2:
            self.stackedWidgetHistorical_2.setCurrentIndex(12)
        elif self.scnMode == 3:
            self.stackedWidgetHistorical_3.setCurrentIndex(12)
        else:
            print("Bad Error - Water Stress")
        
    def showAnalysis1(self):
        """"""
        self.stackedWidgetRiskImg.setCurrentIndex(1)
        
    def showAnalysis2(self):
        """"""            
        self.stackedWidgetRiskImg.setCurrentIndex(2)
        
    def runAnalysis(self):
        """"""
        # Call setupWthParser from wthParser
        
        self.saveRiskImg()
        
        self.resML1.setEnabled(True)
        self.resML2.setEnabled(True)
        
    def saveRiskImg(self):
        """"""
        img1holder = QPixmap( self.dfSavingDisp['MLAnalysis'].iloc[0] )
        img2holder = QPixmap( self.dfSavingDisp['MLAnalysis'].iloc[1] )        
        
        self.img1holder.setPixmap(img1holder)
        self.img2holder.setPixmap(img2holder)        
    

    def makeWTDdir(self):
        """"""
        tmpDir = Wdir + "\\" + "tmp_wtd"
        if os.path.exists(tmpDir):
            shutil.rmtree(tmpDir)   
        os.makedirs(tmpDir)        
              

def  main():
    """"""
    
    app = QApplication(sys.argv)
    window = MainApp()
    window.show()
    app.exec_()
    
        
if __name__ == '__main__':
    main()
    
    