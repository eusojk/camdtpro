import os.path
import glob
import pandas as pd
import csv
import subprocess
import shutil

from wthParser import *

wdir   = os.path.dirname(os.path.abspath(__file__))
fredir = wdir + "\\freRes\\"
frenp = wdir + "\\frenp\\"
fnParPT = fredir + "paramTemp.txt"
fname    = fredir + "paramPT.txt"
tmpWTDDir = wdir + "\\" + "tmp_wtd"

dfParPT = pd.read_csv(fnParPT, delimiter=':')
dfSetup1 = pd.DataFrame()


def setupFRE(df):
    """"""
    global dfSetup1, fnParPT, fname, dfParPT
    
    dfSetup1 = df
    indexOfSCF = list(dfSetup1['SCF1'])
    indexPosList = [ i for i in range(len(indexOfSCF)) if indexOfSCF[i] == 1]
    
    # Getters
    station = dfSetup1['Values'].iloc[0] # 0
    startyear = ' 1988' #dfSetup1['Values'].iloc[1] #1
    endyear = '2019' #2
    currentyear = '2018' #5
    plantingMonth = list(dfSetup1['PLT1']).index(1) + 1 #6
    harvMonthPlus = plantingMonth + 5 #7
    startMonth = indexPosList[0] + 1 #11
    endMonth = indexPosList[-1] + 1#12
    bnRain = dfSetup1['BN1'].mean() #14
    nnRain = dfSetup1['NN1'].mean() #15
    anRain = dfSetup1['AN1'].mean() #16
    bnTemp = dfSetup1['TBN1'].mean() #19
    nnTemp = dfSetup1['TNN1'].mean() #20
    anTemp = dfSetup1['TAN1'].mean() # 21
    
    
    sp = "   "
    colname = dfParPT.columns.tolist()[1]
    col1 = dfParPT.columns.tolist()[0]
    colnwnm = sp  + colname
    # setters
    dfParPT[colname].iloc[0] = sp + str(station)
    dfParPT[colname].iloc[1] = sp + str(startyear)
    dfParPT[colname].iloc[2] = sp + str(endyear)
    dfParPT[colname].iloc[5] = sp + str(currentyear)
    dfParPT[colname].iloc[6] = sp + str(plantingMonth)
    dfParPT[colname].iloc[7] = sp + str(harvMonthPlus)
    dfParPT[colname].iloc[11] = sp + str(startMonth)
    dfParPT[colname].iloc[12] = sp + str(endMonth)
    dfParPT[colname].iloc[14] = sp + str(bnRain)
    dfParPT[colname].iloc[15] = sp + str(nnRain)
    dfParPT[colname].iloc[16] = sp + str(anRain)
    dfParPT[colname].iloc[19] = sp + str(bnTemp)
    dfParPT[colname].iloc[20] = sp + str(nnTemp)
    dfParPT[colname].iloc[21] = sp + str(anTemp)
    dfParPT=dfParPT.rename(columns = {colname:colnwnm})
    
    dfParPT.to_csv(fname, index=None, sep=':', mode='w')
    #print(dfParPT[colname])
    runOptimizer(fname)
    
def runOptimizer(file):
    """"""
    global fname
    
    if os.path.exists(fredir):
        #print('OY')
        os.chdir(fredir)
        
        args = "freo.exe paramPT.txt"
        subprocess.call(args)
        
        exportWTH()
        copyToWTDHdir()
    
def copyToWTDHdir():
    """"""
    global fredir, frenp
    
    if os.path.exists(frenp):
        shutil.rmtree(frenp)   #remove existing folder
    os.makedirs(frenp)    
    
    if os.path.exists(tmpWTDDir):
        targetFiles = fredir + "\\*.WTD"
        targetFiles = glob.glob(targetFiles)
        tgfiles = fredir + "\\*.WTH"
        tgfiles = glob.glob(tgfiles)
        
        for file in targetFiles:
            if len(file) == 44:
                shutil.copy(file, tmpWTDDir)  
            
        for f in tgfiles:
            shutil.copy(f, frenp)  
    print("finish copying")
            
def exportWTH():
    """"""
    global fredir
    
    os.chdir(fredir)
    args = "exp.exe ABCD.CLI "
    fi = glob.glob("*.WTD")
    for f in fi:
        if len(f) == 12:
            #print(f)
            subargs = args + f
            subprocess.call(subargs)
    print("finish exporting")