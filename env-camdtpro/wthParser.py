# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 11:39:58 2019

@author: kpodojos
"""

import glob
import pandas as pd
import os.path

wdir   = os.path.dirname(os.path.abspath(__file__))
tmp_wtd = wdir + "\\" + "tmp_wtd"

# First day of June and last day of Septemeber as DOY in year 2017
# Retrieve data based on time frame
doyJune17 = 2018152
doySept17 = 2018273

# http://localhost:8888/notebooks/Documents/GitHub/camdt_ml/current/01_ml_features_selection_bgd.ipynb
# name of column:[standard dev, mean]
lstMuStdRAIN = {'RAIN-Jun-W1':[0.051696, 0.063824] , 'RAIN-Jun-W2':[0.072433, 0.080271] , 'RAIN-Jun-W3':[0.190737, 0.161857] , 'RAIN-Jun-W4':[0.100157, 0.179166] ,
                'RAIN-Jul-W1':[0.058813, 0.039023] , 'RAIN-Jul-W2':[0.114489, 0.081197] , 'RAIN-Jul-W3':[0.054662, 0.049185] , 'RAIN-Jul-W4':[0.090220, 0.115639] , 
                'RAIN-Aug-W1':[0.048873, 0.054120] , 'RAIN-Aug-W2':[0.130044, 0.166639] , 'RAIN-Aug-W3':[0.045154, 0.028075] , 'RAIN-Aug-W4':[0.116699, 0.140068] , 
                'RAIN-Sep-W1':[0.115046, 0.154617] , 'RAIN-Sep-W2':[0.026710, 0.028391] , 'RAIN-Sep-W3':[0.038338, 0.020405] , 'RAIN-Sep-W4':[0.072125, 0.060342] }

lstMuStdRH   = {'RH-Jun-W1':[5.559415, 66.202632] , 'RH-Jun-W2':[8.172493, 66.273722] , 'RH-Jun-W3':[5.461039, 70.788112] , 'RH-Jun-W4':[5.846639, 72.768864] , 
                'RH-Jul-W1':[4.646379, 70.460067] , 'RH-Jul-W2':[7.376740, 65.833676] , 'RH-Jul-W3':[4.688452, 70.354953] , 'RH-Jul-W4':[4.007738, 74.242825] , 
                'RH-Aug-W1':[4.433728, 69.489141] , 'RH-Aug-W2':[3.981468, 72.302374] , 'RH-Aug-W3':[4.107893, 71.090649] , 'RH-Aug-W4':[3.933751, 74.706188] , 
                'RH-Sep-W1':[4.724067, 72.808452] , 'RH-Sep-W2':[3.555347, 72.377221] , 'RH-Sep-W3':[3.295768, 73.179701] , 'RH-Sep-W4':[5.463900, 70.591986] }

lstMuStdTEMP = {'TEMP-Jun-W1':[2.242915, 64.030789] , 'TEMP-Jun-W2':[3.452473, 69.584023] , 'TEMP-Jun-W3':[2.637773, 70.470487] , 'TEMP-Jun-W4':[1.858251, 64.902248] ,
                'TEMP-Jul-W1':[3.971964, 74.654925] , 'TEMP-Jul-W2':[1.224209, 71.110940] , 'TEMP-Jul-W3':[3.008184, 71.220365] , 'TEMP-Jul-W4':[3.700690, 70.260471] ,  
                'TEMP-Aug-W1':[3.470026, 67.947463] , 'TEMP-Aug-W2':[5.547217, 70.032914] , 'TEMP-Aug-W3':[2.513589, 70.813496] , 'TEMP-Aug-W4':[1.515928, 70.132406] , 
                'TEMP-Sep-W1':[6.834224, 67.813571] , 'TEMP-Sep-W2':[2.824146, 59.818642] , 'TEMP-Sep-W3':[2.697725, 69.100903] , 'TEMP-Sep-W4':[6.397915, 67.071818] }

def iniVarDf(N, var):
    """
    akes in the type of var (R/T/P) and
    creates a 200 x 4 dataframe | meaning (WTH1...WTH200) X (VarW1...VarW4)
    """
    
    # Make rows names, 200 by default
    rw = N*['WTH']
    rwr=[]
    for i,j in enumerate(rw):
        stri = j + str(i+1)
        rwr.append(stri)
    
    # Make columns name
    colJu = 4*['-Jun-W']
    colJl = 4*['-Jul-W']
    colAu = 4*['-Aug-W']
    colSe = 4*['-Sep-W']
    col = [colJu, colJl, colAu, colSe ]
    colr = []
    for z in range(len(col)):
        coli = col[z]
        for i,j in enumerate(coli):
            stri = str(var) + j + str(i+1)
            colr.append(stri)
        
    return pd.DataFrame(columns=colr, index=rwr)
    
def getAvg(df):
    return df.mean(axis=1)
    
def makeWeekly(df):
    
    # Get all the indexes
    allIndexes = df.index.tolist()
    
    # needed for rolling weekly:
    wkMean = df.rolling(7).mean()
    lstMeanVal = []
    
    for i in allIndexes:
        if i%7 == 6:
            val = wkMean.iloc[i]
            lstMeanVal.append(val)
    
    return lstMeanVal   
    

    
def convRain(rain):
    return rain/25.4
    
def convTAVToFar(temp):
    return (temp * 1.8) + 32
    
def applyStSca(z, mu, s):
    return (z - mu) / s

def getMuStd(df, code):
    global lstMuStdRAIN, lstMuStdRH, lstMuStdTEMP
    
    dfSTd = df
    
    if code == 'RAIN':
        dicti = lstMuStdRAIN
    elif code == 'RH':
        dicti = lstMuStdRH
    elif code == 'TEMP':
        dicti = lstMuStdTEMP
    else:
        return
     
    colLst = df.columns.values.tolist()
    for i in range(len(colLst)):
        col = colLst[i]
        muSt= dicti[col]
        std = muSt[0]
        mu  = muSt[1]
        
        dfSTd[col] = dfSTd[col].apply( ( lambda z: applyStSca(z, mu, std) ) )
    
    return dfSTd


def parseWTD(wtdDir):
    
    global doyJune17, doySept17
    
    numWTH = len(wtdDir)
    
    # Initialize our main dataframes
    dfRH = iniVarDf(numWTH, 'RH')
    dfRAIN = iniVarDf(numWTH, 'RAIN')
    dfTEMP = iniVarDf(numWTH, 'TEMP')
    
    lstDF = []
    
    for fnum in range(numWTH):
        
        # needs to be iterate over later
        tmpFile = wtdDir[fnum]
        
        columns = ['DATE', 'SRAD', 'TMAX', 'TMIN', 'RAIN', 'RH']
        dfTmp  = pd.read_csv(tmpFile, delim_whitespace=True, skiprows=1, names=columns)
        
        
        
        dfSel = dfTmp[(dfTmp['DATE'] >= doyJune17) & (dfTmp['DATE'] <= doySept17 )].reset_index(drop=True)
        
        dfTempAvg = dfSel[['TMAX', 'TMIN']]
        dfTempAvg = getAvg(dfTempAvg)
        
        dfTempAvgConv = dfTempAvg.apply(convTAVToFar)
        dfRainConv = dfSel['RAIN'].apply(convRain)
        dfRHNoConv = dfSel['RH']
        
        meanListRAIN = makeWeekly(dfRainConv)
        meanListRH = makeWeekly(dfRHNoConv)
        meanListTEMP = makeWeekly(dfTempAvgConv)
        
        # make sure we have 16 entries
        meanListRAIN.pop() 
        meanListRH.pop()
        meanListTEMP.pop()
        
        dfRH.iloc[fnum] = meanListRH
        dfRAIN.iloc[fnum] = meanListRAIN
        dfTEMP.iloc[fnum] = meanListTEMP
    
    lstDF.append(dfRH)
    lstDF.append(dfRAIN)
    lstDF.append(dfTEMP)
    
    return lstDF


#def main():
def setupWthParser(reginp=0, doninp='MR'):
    """
    int_ reginp: 0 (Rain), 1(T), 2(R-T), 3(R-T-RH)
    str_ doninp: MR (Multiple regression), LA (lasso), RF (Random Forest)
    """
    global tmp_wtd
    file_path = glob.glob(tmp_wtd)
    numWTH = len(file_path)
        
    if numWTH != 200:
        print('Missing WTD files, only got {} but should be 200'.format(numWTH))
    
    else:
        lstDF = parseWTD(file_path)
        
        # Non standardized values
        dfRH = lstDF[0]
        dfRAIN = lstDF[1]
        dfTEMP = lstDF[2]
            
        # standardized values
        dfRHSTD   = getMuStd(dfRH, 'RH')
        dfRAINSTD = getMuStd(dfRAIN, 'RAIN')
        dfTEMPSTD = getMuStd(dfTEMP, 'TEMP')
        
        # working DF needed 
        if reginp == 0:
            wkDF = dfRHSTD
        elif reginp == 1:
            wkDF = dfTEMPSTD
        elif reginp == 2:
            wkDF = makeDFRT(dfRHSTD, dfTEMPSTD)
        elif reginp == 3:
            wkDF = makeDFRTH(dfRHSTD, dfTEMPSTD, dfRHSTD)
            

def makeDFRT(dfR, dfT):
    pass



def makeDFRTH(dfR, dfT, dfRH):
    pass
    
    
def runMLModel(wkDF, code):
    """"""
    if code == 'MR':
        runMR(wkDF)
    elif code == 'LA':
        runLA(wkDF)
    elif code == 'RF':
        runRF(wkDF)
    else:
        print('Error running ML model: ', code)

def runMR(wkDF):
    """"""
    pass

def runLA(wkDF):
    """"""
    pass

def runRF(wkDF):
    """"""
    pass
    