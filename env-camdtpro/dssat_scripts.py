import time
from time import sleep
import cv2

import datetime
import math
import subprocess  #to run executable
from pathlib import Path
import shutil   #to remove a foler which is not empty
import os   #operating system
import sys
import numpy as np
import matplotlib.pyplot as plt  #to create plots
import fnmatch   # Unix filename pattern matching => to remove PILI0*.WTD
import os.path
import glob
from scipy.stats import rankdata #to make a rank to create yield exceedance curve
import pandas as pd
import csv
import calendar
import string
from Create_WTH import create_WTH_main

Wdir = os.path.dirname(os.path.abspath(__file__))
Wdir_path = Wdir + "\\inputs"
Wdir_path_temp = Wdir + "\\inputstemp\\"
sname = ''
step = 1
WSTA = ''
WSTA_ID = ''
WTD_fname = ''
WTD_last_date = ''
INGENO = ''
ID_SOIL = ''
cul_type = ''
plt_density = 0
soil_type = ''
wet_soil = ''
i_NO3 = ''
SNX_list_Seas = []
SNX_list_Hist = []
start_year_Seas = 0
start_year_Hist = 0
start_year = 0
sim_years = 0
obs_flag = 0
obs_flag_Hist = 0
obs_flag_Seas= 0
genMode = 1 # Generator used: 1 (WGEN) and 0 (FRESAMPLER)

FDATE1  = ''
FDATE2  = ''
FDATE3  = ''
FAMN1  = ''
FAMN2  = ''
FAMN3  = ''
FACD1  = ''
FACD2  = ''
FACD3  = ''
FMCD1  = ''
FMCD2  = ''
FMCD3  = ''
irrMthVal = ''
fertMthVal = ''
numFert   = ''

IC_w_ratio = ''
IMETH = ''
IMDEP = ''
ITHRL = ''
IRAMT = ''

startDate = ''
IRVAL = ''
interval = ''
endDate = ''
EFIR    = '' 
IROP = ''

lst = [] #Year 1 for planting
lst2 = []  #Year 2 for planting
lst_frst = [] #Year 1 for SCF
lst_frst2 = [] #Year 2 for SCF

dfSavingStp1 = pd.DataFrame()
dfSavingStp2 = pd.DataFrame()
dfSavingScen = pd.DataFrame()
dfSavingDisp = pd.DataFrame()

scenario = 1
mode = ''
modeDisp = ''

list_AN = []
list_BN = []
list_NN = []


def setupDSSAT(dfList, states, scenarioNum, modeChar):
    """
    df: dataframe to retrieve parameters from
    states: list of 2 lists. the first contains SCF and the second Planting
    scenario: 1,2,3
    mode: H (historical) or S (seasonal)
    """
    global lst_frst, lst_frst2, lst, lst2, WSTA, WSTA_ID, WTD_fname, WTD_last_date, start_year, cul_type, plt_density, soil_type, wet_soil, \
           i_NO3, genMode, INGENO, ID_SOIL, IC_w_ratio, IMETH, IROP, fertMthVal, numFert, FDATE1, FDATE2, FDATE3, \
           FAMN1, FAMN2, FAMN3, FMCD1, FMCD2, FMCD3,  FACD1, FACD2, FACD3, irrMthVal, IMDEP, ITHRL, IRAMT, startDate, \
           IRVAL, interval, endDate, EFIR, sname, step, mode, modeDisp, scenario, list_AN, list_BN, list_NN, dfSavingDisp
    
    
    #print(glob.glob(Wdir_path))
    scenario = scenarioNum
    mode = modeChar
    
    if mode == 'H':
        modeDisp = 'ValuesHistorical'
    elif mode == 'S':
        modeDisp = 'ValuesSeasonal'
    else:
        modeDisp = 'ValueComparison'
        
    
    dfSavingStp1 = dfList[0]
    dfSavingStp2 = dfList[1]
    dfSavingScen = dfList[2]
    dfSavingDisp = dfList[3]    
    
    lst_frst = states[0][0]
    lst_frst2 = states[0][1]
    lst = states[1][0]
    lst2 = states[1][1]    
    
    setup1List = list(dfSavingStp1['Values'])
    setup2List = list(dfSavingStp2['Values'])
    #print(dfSavingStp2['Values'])
    
    list_AN = list(dfSavingStp1['AN1']) + list(dfSavingStp1['AN2']) 
    list_BN = list(dfSavingStp1['BN1']) + list(dfSavingStp1['BN2']) 
    list_NN = list(dfSavingStp1['NN1']) + list(dfSavingStp1['NN2'])    
    
    
    WSTA = setup1List[0]
    WSTA_ID =  WSTA + '1701'
    WTD_fname = Wdir_path + "\\"+ WSTA + ".WTD"
    WTD_last_date = find_obs_date(WTD_fname)
    start_year = setup1List[1]
    cul_type = setup1List[2]
    plt_density = setup1List[3]
    soil_type = setup1List[4]
    wet_soil = setup1List[5]
    i_NO3 = setup1List[6]  
    genMode = setup1List[7]
    INGENO = cul_type
    ID_SOIL = soil_type
    IC_w_ratio = wet_soil
    IMETH = 'IR001'
    IROP =  IMETH
    
    
    fertMthVal = setup2List[0]  
    numFert   = setup2List[1] 
    FDATE1  = setup2List[2]
    FDATE2  = setup2List[3]
    FDATE3  = setup2List[4]
    FAMN1  = setup2List[5]
    FAMN2  = setup2List[6]
    FAMN3  = setup2List[7]
    FMCD1  = setup2List[8]
    FMCD2  = setup2List[9]
    FMCD3  = setup2List[10]    
    FACD1  = setup2List[11]
    FACD2  = setup2List[12]
    FACD3  = setup2List[13]
    irrMthVal = setup2List[14]
    IMDEP = setup2List[15]
    ITHRL = setup2List[16]
    IRAMT = setup2List[17]   
    startDate = int(setup2List[18]) 
    IRVAL = setup2List[19] 
    interval = int(setup2List[20] )
    endDate = int(setup2List[21] )
    EFIR    = setup2List[22] 
    
     
    
    if (FACD1 == 'None'):
        FACD1  = '-99'
    if (FACD2 == 'None'):
        FACD2  = '-99'    
    if (FACD3 == 'None'):
        FACD3  = '-99'     
       
    
    if scenario == 1:
        sname = dfSavingScen['Values'].iloc[0]
        step = dfSavingScen['Values'].iloc[3]
    elif scenario == 2:
        sname = dfSavingScen['Values'].iloc[1]
        step = dfSavingScen['Values'].iloc[4]
    elif scenario == 3:
        sname = dfSavingScen['Values'].iloc[2]
        step = dfSavingScen['Values'].iloc[5]
        
    if (mode == 'H'):
        runHistorical()
    elif (mode == 'S'):
        runSeasonal()
    elif (mode == 'C'):
        runComparison()
    else:
        print("Error")
        




def updateImgPaths():
    """"""
    global dfSavingDisp
    
    return dfSavingDisp

def runHistorical():
    """
    This function runs the historical version of DSSAT
    """
    global lst_frst, lst_frst2, lst, lst2, WSTA, WSTA_ID, WTD_fname, WTD_last_date, start_year, cul_type, plt_density, soil_type, wet_soil, \
           i_NO3, genMode, INGENO, ID_SOIL, IC_w_ratio, IMETH, IROP, fertMthVal, numFert, FDATE1, FDATE2, FDATE3, \
           FAMN1, FAMN2, FAMN3, FMCD1, FMCD2, FMCD3,  FACD1, FACD2, FACD3, irrMthVal, IMDEP, ITHRL, IRAMT, startDate, \
           IRVAL, interval, endDate, EFIR, sname, step
    
    #print("name: ", sname)
    
    obs_flag = 0
    #print(lst_frst, lst_frst2)
    pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)
    
    #find simulation start date
    temp=int(pdate_1%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)
    if temp <= 0 :   #EJ(3/13/2019)
        if calendar.isleap(int(start_year)-1):  
            temp = 366 + temp
        else:
            temp = 365 + temp
        IC_date = (pdate_1//1000 -1)*1000+temp
    else:
        IC_date = (pdate_1//1000)*1000+temp    
    
    #Determines how many crop growing seasons are available from the historical observed.
    sim_years, obs_year1 = find_obs_years(WTD_fname, IC_date, hv_date)  
    
    if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
        obs_flag = 1
        
    SNX_list = []
    if (pdate_2%1000 > pdate_1%1000):
        for iday in range(pdate_1,pdate_2+1,step):   
            SNX_fname = Wdir_path+ "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
            writeSNX_main_hist(SNX_fname, sim_years, obs_year1, iday)  
            SNX_list.append(SNX_fname)    
            
    else: #if planting window covers two consecutive years
        #go through Year 1
        end_day = int(start_year)*1000 + 365
        if calendar.isleap(int(start_year)):  
            end_day = int(start_year)*1000 + 366
            
        for iday in range(pdate_1, end_day + 1, step):   
            SNX_fname = Wdir_path.replace("/","\\") + "\\H" + repr(iday%1000).zfill(3) + sname+".SNX"
            writeSNX_main_hist(SNX_fname,sim_years,obs_year1,iday)    
            SNX_list.append(SNX_fname) 
            temp_date = iday
            
        #next, go throught Year 2
        start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
        if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
        for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
            SNX_fname = Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
            writeSNX_main_hist(SNX_fname,sim_years,obs_year1,iday)     
            SNX_list.append(SNX_fname)    
    

    #=============================================
    # Create DSSBatch.v47 file and run DSSAT executable 
    #=============================================    
    
    entries = ("PlantGro.OUT","Evaluate.OUT", "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
    
    ##write *.V47 batch file
    mode = 'H'    
    writeV47(mode, SNX_list, obs_flag)
    
    #==RUN DSSAT with ARGUMENT 
    start_time = time.perf_counter() # => not compatible with Python 2.7
    os.chdir( Wdir_path)  #change directory
    
    args = "DSCSM046.EXE N DSSBatch.v46 "
    
    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
    end_time0 = time.perf_counter() # => not compatible with Python 2.7
    #print('It took {0:5.1f} sec to finish n years of obs weather x n planting dates'.format(end_time0-start_time))
    start_time1 = time.perf_counter()  # => not compatible with Python 2.7    
    
    #create a new folder to save outputs of the target scenario
    new_folder = sname + "_output_hist"
    #remove existing folder
    if os.path.exists(new_folder):
        shutil.rmtree(new_folder)   
    os.makedirs(new_folder)
    #copy outputs to the new folder
    dest_dir = Wdir_path + "\\" + new_folder
    # print 'dest_dir is', dest_dir    
    
    for entry in entries:
        source_file = Wdir_path + "\\" + entry
        if os.path.isfile(source_file):
            shutil.move(source_file, dest_dir)
        else:
            print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')
    
    
    #move *.SNX file to the new folder with output files
    for i in range(len(SNX_list)):
        shutil.move(SNX_list[i], dest_dir)    
    
    Yield_boxplot_hist(SNX_list, sim_years, obs_flag)
    WSGD_plot_hist(SNX_list, sim_years, obs_flag)
    weather_plot_hist(SNX_list, sim_years, obs_flag)
    plot_cum_rain_hist(SNX_list, sim_years, obs_flag)
    print('========================================================================')
    print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
    print ("** Yeah!! DSSAT Simulation has been finished successfully!")
    print ("** Please check all output and figues in your working directory.")
    print('========================================================================')
######################################################################    
   
    
    
def runSeasonal():
    """
    This function runs the seasonal version of DSSAT
    
    """
    global lst_frst, lst_frst2, lst, lst2, WSTA, WSTA_ID, WTD_fname, WTD_last_date, start_year, cul_type, plt_density, soil_type, wet_soil, \
           i_NO3, genMode, INGENO, ID_SOIL, IC_w_ratio, IMETH, IROP, fertMthVal, numFert, FDATE1, FDATE2, FDATE3, \
           FAMN1, FAMN2, FAMN3, FMCD1, FMCD2, FMCD3,  FACD1, FACD2, FACD3, irrMthVal, IMDEP, ITHRL, IRAMT, startDate, \
           IRVAL, interval, endDate, EFIR, sname, step, mode, modeDisp, scenario, list_AN, list_BN, list_NN
    
    obs_flag = 0
    #print(lst_frst, lst_frst2)
    pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)
    
    if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
        obs_flag = 1
    
    cr_type='MZ'
    SNX_list = []
    if pdate_2%1000 > pdate_1%1000:
        for iday in range(pdate_1,pdate_2+1,step):   
            SNX_fname=Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
            writeSNX_main(SNX_fname,cr_type,iday,obs_flag)  
            SNX_list.append(SNX_fname)
    
    else: #if planting window covers two consecutive years
            #go through Year 1
        end_day = int(start_year)*1000 + 365
        if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
        for iday in range(pdate_1,end_day + 1,step):    
            SNX_fname=Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
            writeSNX_main(SNX_fname,cr_type,iday,obs_flag)    
            SNX_list.append(SNX_fname) 
            temp_date = iday
        #next, go throught Year 2
        start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
        if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
        for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
            SNX_fname=Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
            writeSNX_main(SNX_fname,cr_type,iday,obs_flag)     
            SNX_list.append(SNX_fname)       
    
    #=============================================    
    # Write scenario summary into a text file 
    # => this can be also used for input for Create_WTH subroutine
    #=============================================
    LAT, LONG, ELEV, TAV, AMP = find_station_info(WSTA)  #Call a function to find more info about station to write header in *.WTH
    summary_fname=Wdir_path+ "\\" + sname+"_summary.txt"
    f = open(summary_fname,"w") 
    working_dir=Wdir_path.replace("/","\\")
    f.write("Directory:  " + working_dir + "\n")
    f.write("Station_name: " + WSTA + " \n")
    f.write("LAT: " + repr(LAT) + " \n")
    f.write("LONG: " + repr(LONG) + " \n")
    f.write("ELEV: " + repr(ELEV) + " \n")
    f.write("TAV: " + repr(TAV) + " \n")
    f.write("AMP: " + repr(AMP) + " \n")
    f.write("First_Plt_Date: " + repr(pdate_1) + " \n")
    f.write("Last_Plt_Date: " + repr(pdate_2) + " \n")
    f.write("Harvest_Date: " + repr(hv_date) + " \n")
    f.write("frst_start_Date: " + repr(frst_date1) + " \n")
    f.write("Planting_window: " + repr(pwindow_m) + " \n")
    f.write("SCF_window: " + repr(fwindow_m) + " \n")
    f.write("Cultivar: " + str(cul_type) + " \n")  #INGENO=self.cul_type.getvalue()[0][0:18] #cultivar type
    f.write("Planting_density: " + str(plt_density) + " \n")  # PPOP=self.plt_density.getvalue()[0:3] #planting density
    f.write("Soil_type: " + str(soil_type)  + " \n")  #ID_SOIL=self.soil_type.getvalue()[0][0:10]
    f.write("Irrigation_A(1)_N(0): " + repr(irrMthVal) + " \n")    #automatic or no-irrigation

    if int(irrMthVal) == 1:
        IMETH = 'IR001' #irrigation method
        f.write("IRR_method: " + IMETH + " \n")
    f.write("Fertilizer_Y(1)_N(0): " + repr(fertMthVal) + " \n")    
    
    if int(fertMthVal)== 1:
        f.write("FDATE1: " + str(FDATE1) + " \n")  
        f.write("FDATE2: " + str(FDATE2) + " \n")
        f.write("FDATE3: " + str(FDATE3) + " \n")
        f.write("F_amount1: " + str(FAMN1)  + " \n")
        f.write("F_amount2: " + str(FAMN2) + " \n")
        f.write("F_amount3: " + str(FAMN3) + " \n")
        f.write("F_method1: " + str(FMCD1) + " \n")
        f.write("F_method2: " + str(FMCD2) + " \n")
        f.write("F_method3: " + str(FMCD3) + " \n")
    
    f.close()    
    
    temp_csv=Wdir_path+ "\\SCF_input_monthly1.csv"
    with open(temp_csv, 'w', newline='') as csvfile:
        scfwriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
        scfwriter.writerow(['BN'] + list_BN[:12])
        scfwriter.writerow(['NN'] + list_NN[:12])
        scfwriter.writerow(['AN'] + list_AN[:12])

    temp_csv=Wdir_path+ "\\SCF_input_monthly2.csv" 
    with open(temp_csv, 'w', newline='') as csvfile:
        scfwriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
        scfwriter.writerow(['BN'] + list_BN[12:])
        scfwriter.writerow(['NN'] + list_NN[12:])
        scfwriter.writerow(['AN'] + list_AN[12:])

    temp_snx=Wdir_path + "\\button_write.csv" 
    with open(temp_snx, 'w', newline='') as csvfile:
        scfwriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        scfwriter.writerow(['Month','J1','F1', 'M1', 'A1','M1','J1','J1','A1','S1','O1',
                'N1','D1', 'J2','F2', 'M2', 'A2','M2','J2','J2','A2','S2','O2','N2','D2'])
        scfwriter.writerow(['SCF'] + lst_frst+ lst_frst2)
        scfwriter.writerow(['plt'] + lst + lst2)
        # scfwriter.writerow(self.scf_button1.state())
    #=============================================
    # Generate 100 weather realizations
    #=============================================    
    
    # Get a list of all files in directory
    for rootDir, subdirs, filenames in os.walk(Wdir_path):
        # Find the files that matches the given patterm
        for filename in fnmatch.filter(filenames, '0*.WTH'):
            try:
                os.remove(os.path.join(rootDir, filename))
            except OSError:
                print("Error while deleting file")

    create_WTH_main(Wdir_path,WTD_fname, WSTA, LAT, LONG, ELEV, TAV, AMP, pdate_1, pdate_2, step, hv_date, frst_date1) 
    
    #=============================================
    # Create DSSBatch.v47 file and run DSSAT executable 
    #=============================================    
    
    entries = ("PlantGro.OUT","Evaluate.OUT", "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
    
    ##write *.V47 batch file
    mode = 'S'    
    writeV47(mode, SNX_list, obs_flag)    

    os.chdir( Wdir_path)  #change directory    
    args = "DSCSM046.EXE N DSSBatch.v46 "   
    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)    
    
    #create a new folder to save outputs of the target scenario
    new_folder= sname+"_output"
    if os.path.exists(new_folder):
        shutil.rmtree(new_folder)   #remove existing folder
    os.makedirs(new_folder)
    #copy outputs to the new folder
    dest_dir= Wdir_path + "\\"+new_folder    
    
    for entry in entries:
        source_file= Wdir_path + "\\" + entry
        if os.path.isfile(source_file):
            shutil.move(source_file, dest_dir)
        else:
            print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')
    
    #move *.SNX file to the new folder with output files
    for i in range(len(SNX_list)):
        snx = SNX_list[i]
        if snx.exists():
            os.remove(snx)
        shutil.move(snx, dest_dir)
    shutil.move(summary_fname, dest_dir)     
    
    shutil.move("WGEN_input_param_month_" + WSTA + ".txt", dest_dir)
    shutil.move("WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt", dest_dir)    
    
    Yield_boxplot(SNX_list, sim_years, obs_flag)
    WSGD_plot(SNX_list, sim_years, obs_flag)
    weather_plot(SNX_list, sim_years, obs_flag)
    plot_cum_rain(SNX_list, sim_years, obs_flag)
    
    print ("** Yeah!! DSSAT Simulation has been finished successfully!")
    print ("** Please check all output and figues in your working directory.")    
    
    
##########################################    
def runComparison():
    """"""
    
def writeV47(mode, snx_list, obs_flag):
    """
    mode: H or S
    """
    #global obs_flag
    temp_dv4   =  Wdir_path+ "\\DSSBatch_TEMP_MZ.v47"
    dv4_fname =  Wdir_path + "\\DSSBatch.v46"
    fr   = open(temp_dv4,"r") 
    fw = open(dv4_fname,"w")     
    
    for line in range(0,10):
        temp_str=fr.readline()
        fw.write(temp_str)
                
    temp_str=fr.readline()       
    
    if mode == 'H':
        #write SNX file with each different planting date
        for i in range(len(snx_list)):   #EJ(3/12/2019) for finding optimal planting dates
            new_str2='{:<95}'.format(snx_list[i])+ temp_str[95:]
           # new_str2='{0:<95}{1:4s}'.format(snx_list[i], repr(j+1).rjust(4))+ temp_str[99:]
            fw.write(new_str2)        
            
    elif mode == 'S':#'S'
        for i in range(len(snx_list)):   #EJ(3/12/2019) for finding optimal planting dates
            #new_str=Wdir_path.replace("/","\\")+ "\\" + snx_list[i]
            #new_str2='{:<95}'.format(new_str)+ temp_str[95:]
            for j in range(100):
                #new_str2='{:<95}'.format(snx_list[i])+ temp_str[95:]
                new_str2='{0:<95}{1:4s}'.format(snx_list[i], repr(j+1).rjust(4))+ temp_str[99:]
                fw.write(new_str2)
            if obs_flag ==1:
                new_str2='{0:<95}{1:4s}'.format(snx_list[i], repr(101).rjust(4))+ temp_str[99:]
                fw.write(new_str2)    
              
    fr.close()
    fw.close()    
    
    
##############################################
    
def writeSNX_main_hist(SNX_fname, sim_years, obs_year1, plt_date):
    """"""    
    global lst_frst, lst_frst2, lst, lst2, WSTA, WSTA_ID, WTD_fname, WTD_last_date, start_year, cul_type, plt_density, soil_type, wet_soil, \
           i_NO3, genMode, INGENO, ID_SOIL, IC_w_ratio, IMETH, IROP, fertMthVal, numFert, FDATE1, FDATE2, FDATE3, \
           FAMN1, FAMN2, FAMN3, FMCD1, FMCD2, FMCD3,  FACD1, FACD2, FACD3, irrMthVal, IMDEP, ITHRL, IRAMT, startDate, \
           IRVAL, interval, endDate, EFIR
    
    temp_snx=Wdir_path + "\\PHMZTEMP.SNX" 
    fr = open(temp_snx,"r") #opens temp SNX file to read
    fw = open(SNX_fname,"w") #opens SNX file to write    
    
    #Setting up parameters
    MI = '0'
    MF = str(fertMthVal)
    
    #print("fertMthVal: ", fertMthVal)
    NYERS = sim_years
    
    #initial condition: 30 day before planting  #EJ(3/13/2019)
    temp=int(plt_date%1000)-30  
    if temp <= 0 :   #EJ(3/13/2019)
        if calendar.isleap(int(start_year)-1):  
            temp = 366 + temp
        else:
            temp = 365 + temp
        ICDAT = repr((plt_date//1000 -1)*1000+temp)
    else:
        ICDAT = repr((plt_date//1000)*1000+temp)    
    
    #ICDAT => str in YYYYDOY, obs_year is integer in YYYY    
    ICDAT = repr(obs_year1) + ICDAT[4:] 
    SNH4 = 1.5    
    PDATE = repr(obs_year1)[2:] + repr(plt_date)[4:]     #YYDOY format   
    
    if int(ICDAT) > (obs_year1*1000 + plt_date%1000):  #if ICDAT > PDATE
        PDATE = repr(obs_year1+1)[2:] + repr(plt_date)[4:]    
        
    SDATE = ICDAT
    
    if irrMthVal == 1:
        IRRIG='A'  #automatic, or 'N' (no irrigation)
        MI= '0'
        # MI = str(self._setup2.getApplyFert())
    elif irrMthVal == 2: # periodic irr
        IRRIG='D' # what value?  --> needs to make a def that appends the periodic irr info at the end of the active snx file
        MI =  '1'
        MI = str(fertMthVal)
    elif irrMthVal == 0: # No irr  
        IRRIG='N'  #automatic, or 'N' (no irrigation)    
    
    
    if fertMthVal == 1:
        FERTI='D' # 'D'= Days after planting, 'R'=on report date, or 'N' (no fertilizer)
    else:
        FERTI='N'    
    
    for line in range(0,14):
        temp_str=fr.readline()
        fw.write(temp_str)    
    
    #write *TREATMENTS 
    CU='1'
    SA='0'
    IC='1'
    MP='1'
    MR='0'
    MC='0'
    MT='0'
    ME='0'
    MH='0'
    SM='1'
    temp_str=fr.readline()    
    FL = '1'
    # *TREATMENTS 
    fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(FL.rjust(3),'1 0 0 ERiMA DCC1                 1',
                        FL.rjust(3),SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3), 
                        MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))     
    fw.write(" \n")   
    
    #read lines from temp file
    for line in range(0,3):
        temp_str=fr.readline()
        #print temp_str
        fw.write(temp_str)
        
    #write *CULTIVARS
    temp_str=fr.readline()
    new_str=temp_str[0:6] + INGENO + temp_str[26:]
    fw.write(new_str)
    fw.write(" \n")   

    #read lines from temp file
    for line in range(0,3):
        temp_str=fr.readline()
        fw.write(temp_str)
    
    soil_depth, wp, fc, nlayer, SLTX = get_soil_IC(ID_SOIL) 
    #print("soil_depth: {}, wp: {}, fc:{}, nlayer:{}, SLTX: {}".format(soil_depth, wp, fc, nlayer, SLTX))
    temp_str=fr.readline()
    SLDP = repr(soil_depth[-1])    
    FL = str(1)
    ID_FIELD = WSTA + str(1).zfill(4)
    fw.write('{0:3s}{1:8s} {2:8s}{3:37s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID, '   -99   -99   -99   -99   -99   -99 ',
                                                SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,' -99')) 
    fw.write(" \n")  
    temp_str=fr.readline()  #@L ...........XCRD ...........YCRD .....ELEV
    fw.write(temp_str)
    temp_str=fr.readline()  # 1             -99             -99       -99   ==> skip    
    
    #================write *FIELDS - second section
    FL = str(1)
    fw.write('{0:3s}{1:89s}'.format(FL.rjust(3), '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
    fw.write(" \n")    
    
    #read lines from temp file
    for line in range(0,3):
        temp_str=fr.readline()
        fw.write(temp_str)    
    
    #write *INITIAL CONDITIONS   
    temp_str=fr.readline()
    new_str=temp_str[0:9] + ICDAT[2:] + temp_str[14:]
    fw.write(new_str)
    temp_str=fr.readline() #@C  ICBL  SH2O  SNH4  SNO3 
    fw.write(temp_str)    
    
    temp_str=fr.readline()
    for nline in range(0,nlayer):
        if nline == 0:  #first layer
            temp_SH2O = IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            
            if i_NO3 == 'H':
                SNO3='14'  # arbitrary... following to Uruguay DSS
            elif i_NO3 == 'L':
                SNO3='5'  # arbitrary... following to Uruguay DSS
            else:
                # self.ini_NO3_err.activate() 
                print("Error of NO3 value") 
                
        elif nline == 1:  #second layer
            temp_SH2O =IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            if i_NO3 == 'H':
                SNO3='14'  # arbitrary... following to Uruguay DSS
            elif i_NO3 == 'L':
                SNO3='5'  # arbitrary... following to Uruguay DSS
            else:
                # self.ini_NO3_err.activate() 
                print("Error of NO3 value")
                
        elif nline == 2:  #third layer
            temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
        elif nline == 3:  #forth layer
            temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
        elif nline == 4:  #fifth layer
            temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
        elif nline == 5:  #sixth layer
            temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L        
        else:
            temp_SH2O=fc[nline] #float
            SNO3='0'  #**EJ(5/27/2015)
        
        SH2O=repr(temp_SH2O)[0:5]  #convert float to string
        new_str=temp_str[0:5] + repr(soil_depth[nline]).rjust(3) + ' ' + SH2O.rjust(5) + temp_str[14:22]+ SNO3.rjust(4)+ "\n"
        fw.write(new_str)
        
    fw.write("  \n")
    
    for nline in range(0,10):
        temp_str=fr.readline()
        #print temp_str
        if temp_str[0:9] == '*PLANTING':
            break
        
    fw.write(temp_str) #*PLANTING DETAILS  
    temp_str = fr.readline()  #@P PDATE EDATE
    fw.write(temp_str)    
    
    #write *PLANTING DETAILS
    temp_str=fr.readline()
    PPOP    = str(plt_density)#planting density
    PPOE    = str(plt_density)#planting density        
    new_str=temp_str[0:3] + PDATE + temp_str[8:14] + PPOP.rjust(6) + PPOE.rjust(6) + temp_str[26:]
    fw.write(new_str)    
    
    #read lines from temp file
    for line in range(0,3):
        temp_str=fr.readline()
        fw.write(temp_str)    
    
    #write *FERTILIZERS (INORGANIC)
    if (MF == '1'):
        temp_str=fr.readline()
        new_str=temp_str[0:5] + str(FDATE1).rjust(3) + ' '+ str(FMCD1).rjust(5)+' '+str(FACD1).rjust(5)+temp_str[20:30]+ str(FAMN1).rjust(3) + temp_str[32:]
        fw.write(new_str)
        temp_str=fr.readline()
        if numFert == 2:
            new_str=temp_str[0:5] + str(FDATE2).rjust(3) + ' '+ str(FMCD2).rjust(5)+' '+ str(FACD2).rjust(5)+temp_str[20:30]+ str(FAMN2).rjust(3) + temp_str[32:]
            fw.write(new_str)
        if numFert== 3:
            new_str=temp_str[0:5] + str(FDATE2).rjust(3) + ' '+ str(FMCD2).rjust(5)+' '+ str(FACD2).rjust(5)+temp_str[20:30]+ str(FAMN2).rjust(3) + temp_str[32:]
            fw.write(new_str)
            new_str=temp_str[0:5] + str(FDATE3).rjust(3) + ' '+ str(FMCD3).rjust(5)+' '+ str(FACD3).rjust(5)+temp_str[20:30]+ str(FAMN3).rjust(3) + temp_str[32:]
            fw.write(new_str)
        
        if numFert == 0:
            print("Error: Number of fertlization app is 0.")
            
    else:  #
        temp_str=fr.readline()
        fw.write(temp_str)
        temp_str=fr.readline()
        fw.write(temp_str)    
    
    
    #read lines from temp file
    for line in range(0,3):
        temp_str=fr.readline()
        fw.write(temp_str)
        
    #write *SIMULATION CONTROLS
    temp_str=fr.readline()
    #new_str=temp_str[0:33]+ SDATE[2:] + temp_str[38:]
    new_str=temp_str[0:18] + repr(NYERS).rjust(2) + temp_str[20:33]+ SDATE[2:] + temp_str[38:]
    fw.write(new_str)
    temp_str=fr.readline() #@N OPTIONS
    fw.write(temp_str)
    temp_str=fr.readline()  # 1 OP     
    fw.write(temp_str)
    temp_str=fr.readline()  #@N METHODS 
    fw.write(temp_str)
    temp_str=fr.readline()  # 1 ME      
    fw.write(temp_str)
    temp_str=fr.readline()  #@N MANAGEMENT 
    fw.write(temp_str)
    temp_str=fr.readline()  # 1 MA   
    new_str=temp_str[0:25] + IRRIG + temp_str[26:31]+ FERTI + temp_str[32:]
    fw.write(new_str)
    temp_str=fr.readline()  #@N OUTPUTS
    fw.write(temp_str)
    temp_str=fr.readline()  # 1 OU   
    fw.write(temp_str)    
    
    #read lines from temp file
    for line in range(0,5):
        temp_str=fr.readline()
        fw.write(temp_str)    
    
    #irrigation method
    temp_str=fr.readline()  #  1 IR 
    new_str = temp_str
    if irrMthVal == 0:
        new_str=temp_str[0:18] + str(IMDEP) + temp_str[20:24] + str(ITHRL) + temp_str[26:39] + str(IMETH) + temp_str[44:48] + str(IRAMT) + temp_str[50:] 

    fw.write(new_str)    
    
    #read lines from temp file
    for line in range(0,7):
        temp_str=fr.readline()
        fw.write(temp_str)    
    
    #*IRRIGATION AND WATER MANAGEMENT
    if irrMthVal == 2: #periodic
        #
        for i in range(4):
            temp_str=fr.readline()  # * IRR & Water Management 
            if i == 2:
                new_str = temp_str[:5] + str(EFIR) + temp_str[8:]
    
            else:
                # new_str = temp_str[:5] + str(EFIR) + temp_str[8:]
                new_str = temp_str
    
            fw.write(new_str)
    
        IDATE_val = 0 
        val = 0
        for i in range(startDate, endDate+interval+1, interval):
            if i == startDate:
                val = 1
            else:
                startDate += interval
                val = startDate
            iDateStr = str(val).rjust(5)
            temp_str=fr.readline()
            # new_str = temp_str[:3] + iDateStr + temp_str[8:18] + str(IRVAL) + temp_str[20:]
            new_str = ' 1 ' + iDateStr + ' IR001    ' + str(IRVAL) + '\n'
    
            fw.write(new_str)    
    
    
    fw.write('\n')
    fr.close()
    fw.close()    
    #====End of WRITE *.SNX
    
    
def writeSNX_main(SNX_fname ,crop, plt_date, obs_flag):
    """"""
    global lst_frst, lst_frst2, lst, lst2, WSTA, WSTA_ID, WTD_fname, WTD_last_date, start_year, cul_type, plt_density, soil_type, wet_soil, \
           i_NO3, genMode, INGENO, ID_SOIL, IC_w_ratio, IMETH, IROP, fertMthVal, numFert, FDATE1, FDATE2, FDATE3, \
           FAMN1, FAMN2, FAMN3, FMCD1, FMCD2, FMCD3,  FACD1, FACD2, FACD3, irrMthVal, IMDEP, ITHRL, IRAMT, startDate, \
           IRVAL, interval, endDate, EFIR, sname, step
    
    
    temp_snx=Wdir_path + "\\PHMZTEMP.SNX" 
    fr = open(temp_snx,"r") #opens temp SNX file to read
    fw = open(SNX_fname,"w") #opens SNX file to write    
    
    #Setting up parameters
    MI = '0'
    MF = str(fertMthVal)    
    
    NYERS= 1  #repr(int(end_year)-int(start_year)+1) #'12'
    temp=int(plt_date%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)     
    if temp <= 0 :   #EJ(3/13/2019)
        if calendar.isleap(int(start_year)-1):  
            temp = 366 + temp
        else:
            temp = 365 + temp
        ICDAT = repr((plt_date//1000 -1)*1000+temp)
    else:
        ICDAT = repr((plt_date//1000)*1000+temp)    
    
    SNH4 = 1.5  #**EJ(5/27/2015) followed by Walter's ETMZDSS6.SNX
    PDATE=repr(plt_date)[2:]      #YYDOY format    
    
    SDATE = ICDAT
    
    if irrMthVal == 1: #Auto
        IRRIG='A'  #automatic, or 'N' (no irrigation)
        MI= '0'
        # MI = str(self._setup2.getApplyFert())
    elif irrMthVal == 2: # periodic irr
        IRRIG='D' # what value?  --> needs to make a def that appends the periodic irr info at the end of the active snx file
        MI =  '1'
        MI = str(fertMthVal)
    elif irrMthVal == 0: # No irr  
        IRRIG='N'  #automatic, or 'N' (no irrigation)       
    
    if fertMthVal == 1: #Auto
        FERTI='D' # 'D'= Days after planting, 'R'=on report date, or 'N' (no fertilizer)
    else:
        FERTI='N'      
    
    for line in range(0,14):
        temp_str=fr.readline()
        fw.write(temp_str)    
    
    #write *TREATMENTS 
    CU='1'
    SA='0'
    IC='1'
    MP='1'
    MR='0'
    MC='0'
    MT='0'
    ME='0'
    MH='0'
    SM='1'
    temp_str=fr.readline()   
    
    for i in range(0,100):
        FL = str(i+1)
        fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(FL.rjust(3),'1 0 0 ERiMA DCC1                 1',
                           FL.rjust(3),SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3),  MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))  
        fw.write(" \n")  
    if obs_flag == 1:
        FL = str(101)  #if observed weather is available, run #101 treatment with observed weather
        fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(FL.rjust(3),'1 0 0 ERiMA DCC1                 1',
                       FL.rjust(3),SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3),  MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))       
        fw.write(" \n")    

    #read lines from temp file
    for line in range(0,3):
        temp_str=fr.readline()
        fw.write(temp_str)

    #write *CULTIVARS
    temp_str=fr.readline()
    new_str=temp_str[0:6] + INGENO + temp_str[26:]
    fw.write(new_str)
    fw.write(" \n")

    #read lines from temp file
    for line in range(0,3):
        temp_str=fr.readline()
        fw.write(temp_str)
    
    #================write *FIELDS   
    #Get soil info from *.SOL
    #soil_depth, wp, fc, nlayer = get_soil_IC(ID_SOIL)  
    soil_depth, wp, fc, nlayer, SLTX = get_soil_IC(ID_SOIL)  # <=========== ***********************
    temp_str=fr.readline()
    SLDP = repr(soil_depth[-1])     
    
    for i in range(0,100):
        FL = str(i+1)
        ID_FIELD = WSTA + str(i+1).zfill(4)  #WSTA = 'SANJ'
        WSTA_ID = str(i+1).zfill(4)
        fw.write('{0:3s}{1:8s}{2:5s}{3:3s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID.rjust(5), '       -99   -99   -99   -99   -99   -99 ',
                                                                                      SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,' -99'))  
        fw.write(" \n")  
    if obs_flag == 1:
        FL = str(101)  #if observed weather is available, run #101 treatment with observed weather
        ID_FIELD = WSTA + str(101).zfill(4)  #WSTA = 'SANJ'
        WSTA_ID = WSTA
        fw.write('{0:3s}{1:8s}{2:5s}{3:3s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID.rjust(5), '       -99   -99   -99   -99   -99   -99 ',
                                                                                  SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,' -99'))  
        fw.write(" \n")  
    
    temp_str=fr.readline()  #@L ...........XCRD ...........YCRD .....ELEV
    fw.write(temp_str)
    temp_str=fr.readline()  # 1             -99             -99       -99   ==> skip
    #================write *FIELDS - second section
    for i in range(0,100):
        FL = str(i+1)
        fw.write('{0:3s}{1:89s}'.format(FL.rjust(3), '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
        fw.write(" \n")  
    if obs_flag == 1:
        FL = str(101)  #if observed weather is available, run #101 treatment with observed weather
        fw.write('{0:3s}{1:89s}'.format(FL.rjust(3), '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
        fw.write(" \n")      
        
    
    #read lines from temp file
    for line in range(0,3):
        temp_str=fr.readline()
        fw.write(temp_str)
    #write *INITIAL CONDITIONS   
    temp_str=fr.readline()
    new_str=temp_str[0:9] + ICDAT[2:] + temp_str[14:]
    fw.write(new_str)
    temp_str=fr.readline() #@C  ICBL  SH2O  SNH4  SNO3 
    fw.write(temp_str)   
    
    #Get soil info from *.SOL
    #soil_depth, wp, fc, nlayer = get_soil_IC(ID_SOIL)  
    temp_str=fr.readline()
    for nline in range(0,nlayer):
        if nline == 0:  #first layer
            temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            # SH2O=0.7*(float(fc[nline])- float(wp[nline]))+ float(wp[nline])#EJ(6/25/2015): initial AWC=70% of maximum AWC
            if i_NO3 == 'H':
                SNO3='14'  # arbitrary... following to Uruguay DSS
            elif i_NO3 == 'L':
                SNO3='5'  # arbitrary... following to Uruguay DSS
            else:
                # self.ini_NO3_err.activate() 
                print("Error of NO3 value")                  
        elif nline == 1:  #second layer
            temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            if i_NO3 == 'H':
                SNO3='14'  # arbitrary... following to Uruguay DSS
            elif i_NO3 == 'L':
                SNO3='5'  # arbitrary... following to Uruguay DSS
            else:
                # self.ini_NO3_err.activate() 
                print("Error of NO3 value") 
        elif nline == 2:  #third layer
            temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
        elif nline == 3:  #forth layer
            temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
        elif nline == 4:  #fifth layer
            temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
        elif nline == 5:  #sixth layer
            temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
            SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
        else:
            temp_SH2O=fc[nline] #float
            SNO3='0'  #**EJ(5/27/2015)

        SH2O=repr(temp_SH2O)[0:5]  #convert float to string
        new_str=temp_str[0:5] + repr(soil_depth[nline]).rjust(3) + ' ' + SH2O.rjust(5) + temp_str[14:22]+ SNO3.rjust(4)+ "\n"
        fw.write(new_str)
    fw.write("  \n")    
    
    for nline in range(0,10):
        temp_str=fr.readline()
        #print temp_str
        if temp_str[0:9] == '*PLANTING':
            break
    fw.write(temp_str) #*PLANTING DETAILS  
    temp_str=fr.readline() #@P PDATE EDATE
    fw.write(temp_str)


    #write *PLANTING DETAILS
    temp_str=fr.readline()
    PPOP    = str(plt_density)#planting density
    PPOE    = str(plt_density)#planting density        
    
    new_str=temp_str[0:3] + PDATE + temp_str[8:14] + PPOP.rjust(6) + PPOE.rjust(6) + temp_str[26:]
    fw.write(new_str)
    
    #read lines from temp file
    for line in range(0,3):
        temp_str=fr.readline()
        fw.write(temp_str)    
    
    #write *FERTILIZERS (INORGANIC)
    if MF == '1':
        temp_str=fr.readline()
        new_str=temp_str[0:5] + str(FDATE1).rjust(3) + ' '+ str(FMCD1).rjust(5)+' '+str(FACD1).rjust(5)+temp_str[20:30]+ str(FAMN1).rjust(3) + temp_str[32:]
        fw.write(new_str)
        temp_str=fr.readline()
        
        if numFert == 2:
            new_str=temp_str[0:5] + str(FDATE2).rjust(3) + ' '+ str(FMCD2).rjust(5)+' '+ str(FACD2).rjust(5)+temp_str[20:30]+ str(FAMN2).rjust(3) + temp_str[32:]
            fw.write(new_str)
        if numFert== 3:
            new_str=temp_str[0:5] + str(FDATE2).rjust(3) + ' '+ str(FMCD2).rjust(5)+' '+ str(FACD2).rjust(5)+temp_str[20:30]+ str(FAMN2).rjust(3) + temp_str[32:]
            fw.write(new_str)
            new_str=temp_str[0:5] + str(FDATE3).rjust(3) + ' '+ str(FMCD3).rjust(5)+' '+ str(FACD3).rjust(5)+temp_str[20:30]+ str(FAMN3).rjust(3) + temp_str[32:]
            fw.write(new_str)
        
        if numFert == 0:
            print("Error: Number of fertlization app is 0.")

    else:  #
        temp_str=fr.readline()
        fw.write(temp_str)
        temp_str=fr.readline()
        fw.write(temp_str)

    #read lines from temp file
    for line in range(0,3):
        temp_str=fr.readline()
        fw.write(temp_str)       
    
    #write *SIMULATION CONTROLS
    temp_str=fr.readline()
    new_str=temp_str[0:18] + repr(NYERS).rjust(2) + temp_str[20:33]+ SDATE[2:] + temp_str[38:]
    # new_str=temp_str[0:33]+ SDATE[2:] + temp_str[38:]
    fw.write(new_str)
    temp_str=fr.readline() #@N OPTIONS
    fw.write(temp_str)
    temp_str=fr.readline()  # 1 OP     
    fw.write(temp_str)
    temp_str=fr.readline()  #@N METHODS 
    fw.write(temp_str)
    temp_str=fr.readline()  # 1 ME      
    fw.write(temp_str)
    temp_str=fr.readline()  #@N MANAGEMENT 
    fw.write(temp_str)
    temp_str=fr.readline()  # 1 MA   
    new_str=temp_str[0:25] + IRRIG + temp_str[26:31]+ FERTI + temp_str[32:]
    fw.write(new_str)
    temp_str=fr.readline()  #@N OUTPUTS
    fw.write(temp_str)
    temp_str=fr.readline()  # 1 OU   
    fw.write(temp_str)    
    
    #read lines from temp file
    for line in range(0,5):
        temp_str=fr.readline()
        fw.write(temp_str)    
        
    ###
    #irrigation method
    temp_str=fr.readline()  #  1 IR 
    new_str=temp_str[0:18] + str(IMDEP) + temp_str[20:24] + str(ITHRL) + temp_str[26:39] + str(IMETH) + temp_str[44:48] + str(IRAMT) + temp_str[50:] 
    fw.write(new_str)    
    
    #read lines from temp file
    for line in range(0,7):
        temp_str=fr.readline()
        fw.write(temp_str)    
    ###
    
    #*IRRIGATION AND WATER MANAGEMENT
    if irrMthVal == 2: #periodic
        #
        for i in range(4):
            temp_str=fr.readline()  # * IRR & Water Management 
            if i == 2:
                new_str = temp_str[:5] + str(EFIR) + temp_str[8:]

            else:
                # new_str = temp_str[:5] + str(EFIR) + temp_str[8:]
                new_str = temp_str

            fw.write(new_str)

        IDATE_val = 0 
        val = 0
        for i in range(startDate, endDate+interval+1, interval):
            if i == startDate:
                val = 1
            else:
                startDate += interval
                val = startDate
            iDateStr = str(val).rjust(5)
            temp_str=fr.readline()
            # new_str = temp_str[:3] + iDateStr + temp_str[8:18] + str(IRVAL) + temp_str[20:]
            new_str = ' 1 ' + iDateStr + ' IR001    ' + str(IRVAL) + '\n'

            fw.write(new_str)    


    fw.write('\n')
    fr.close()
    fw.close()    
    #====End of WRITE *.SNX    
    
    
        
    #############################################   
           
    
    
def Yield_boxplot(SNX_list, sim_years, obs_flag):
    """
    """
    global sname, start_year, scenario, mode, modeDisp, dfSavingDisp
    
    fname1 = Wdir_path + "\\"+ sname+"_output\\SUMMARY.OUT"
    pdate_list = []
    year_list = []
    
    for i in range(len(SNX_list)):
        pdate_list.append(int(SNX_list[i][-11:-8]))
        if i == 0:
            year_list.append(int(start_year))
        else:
            if pdate_list[i] > pdate_list[i-1]:
                year_list.append(int(start_year))
            else:
                year_list.append(int(start_year) + 1)
                
    df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
    # PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
    PDAT = df_OUT.ix[:,13].values  #read 14th column only => Planting date # This is needed for DSSAT 46
    HWAM = df_OUT.ix[:,20].values  #read 21th column only
    ADAT = df_OUT.ix[:,15].values  #read 21th column only
    MDAT = df_OUT.ix[:,16].values  #read 21th column only

    DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
    ADAT_array = np.zeros(len(ADAT)) #np.empty((ndays,1))*np.nan #initialize
    MDAT_array = np.zeros(len(MDAT)) #np.empty((ndays,1))*np.nan #initialize                
               
    for i in range(len(HWAM)):
        # year_array[i] = int(repr(PDAT[i])[:4])
        DOY_array[i] = int(repr(PDAT[i])[4:])
        #MDAT_array[i] = int(repr(MDAT[i])[4:])
        if ADAT[i] == 0:  #in case emergence does not happen
            ADAT_array[i] = np.nan
            MDAT_array[i] = np.nan
        elif ADAT[i] == -99:  #in case emergence does not happen
            ADAT_array[i] = np.nan
            MDAT_array[i] = np.nan
        elif MDAT[i] == -99: 
            MDAT_array[i] = np.nan
        else:
            ADAT_array[i] = int(repr(ADAT[i])[4:])
            MDAT_array[i] = int(repr(MDAT[i])[4:])
            
    #make three arrays into a dataframe
    df = pd.DataFrame({'DOY': DOY_array, 'ADAT':ADAT_array, 'MDAT':MDAT_array, 'HWAM':HWAM}, columns=['DOY','ADAT','MDAT','HWAM'])

    # #make an empty array [nyears * n_plt_dates]
    # n_year = 3000
    n_year = 100
    # For Boxplot
    yield_n = np.empty([n_year,len(pdate_list)])*np.nan
    ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan
    MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
    # For exceedance curve
    sorted_yield_n = np.empty([n_year,len(pdate_list)])*np.nan
    Fx_scf = np.empty([n_year,len(pdate_list)])*np.nan       
    sorted_ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan   
    Fx_scf_ADAT = np.empty([n_year,len(pdate_list)])*np.nan
    sorted_MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
    # Fx_scf_MDAT = np.empty([n_year,len(pdate_list)])*np.nan    

    if obs_flag == 0:  #if there is no observed weather available 
        for count in range(len(pdate_list)):
            Pdate = pdate_list[count]
            yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values  
            ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values 
            MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values  
            sorted_yield_n[:,count] = np.sort(df.HWAM[(df["DOY"] == Pdate)].values)
            fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
            #Fx_scf = np.cumsum(fx_scf)
            Fx_scf[:,count] = 1.0-np.cumsum(fx_scf)  #for exceedance curve
            sorted_ADAT_n[:,count] = np.sort(df.ADAT[(df["DOY"] == Pdate)].values)
            sorted_MDAT_n[:,count] = np.sort(df.MDAT[(df["DOY"] == Pdate)].values)
            temp = sorted_ADAT_n[:,count]
            temp2 = temp[~np.isnan(temp)]
            fx_scf_ADAT = [1.0/len(temp2)] * len(temp2) #pdf
            Fx_scf_ADAT[:len(fx_scf_ADAT),count] = 1.0-np.cumsum(fx_scf_ADAT)  #for exceedance curve         
    else:   
        obs_yield = []
        obs_ADAT = []
        obs_MDAT = []
        for count in range(len(pdate_list)):
            Pdate = pdate_list[count]
            yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values[:-1]  
            ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values[:-1] 
            MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values[:-1] 
            sorted_yield_n[:,count] = np.sort(df.HWAM[(df["DOY"] == Pdate)].values[:-1])
            fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
            #Fx_scf = np.cumsum(fx_scf)
            Fx_scf[:,count] = 1.0-np.cumsum(fx_scf)  #for exceedance curve
            sorted_ADAT_n[:,count] = np.sort(df.ADAT[(df["DOY"] == Pdate)].values[:-1])
            sorted_MDAT_n[:,count] = np.sort(df.MDAT[(df["DOY"] == Pdate)].values[:-1])
            temp = sorted_ADAT_n[:,count]
            temp2 = temp[~np.isnan(temp)]
            fx_scf_ADAT = [1.0/len(temp2)] * len(temp2) #pdf
            Fx_scf_ADAT[:len(fx_scf_ADAT),count] = 1.0-np.cumsum(fx_scf_ADAT)  #for exceedance curve    
            #collect simulated results from observed weather
            obs_yield.append(df.HWAM[(df["DOY"] == Pdate)].values[-1])      
            obs_ADAT.append(df.ADAT[(df["DOY"] == Pdate)].values[-1])  
            obs_MDAT.append(df.MDAT[(df["DOY"] == Pdate)].values[-1])                 

    
    #To remove 'nan' from data array
    #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
    mask = ~np.isnan(yield_n)
    filtered_yield = [d[m] for d, m in zip(yield_n.T, mask.T)]
    mask2 = ~np.isnan(ADAT_n)
    filtered_data2 = [d[m] for d, m in zip(ADAT_n.T, mask2.T)]
    mask3 = ~np.isnan(MDAT_n)
    filtered_data3 = [d[m] for d, m in zip(MDAT_n.T, mask3.T)]        
    
    if obs_flag == 1: 
        #replace ADAT = 0 with nan
        obs_ADAT = [np.nan if x==1 else x for x in obs_ADAT]
        obs_MDAT = [np.nan if x==1 else x for x in obs_MDAT]
        
    myXList=[i+1 for i in range(len(pdate_list))]
    scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
    for count in range(len(pdate_list)):
        temp_str = repr(year_list[count]) + " " + repr(pdate_list[count]) 
        temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
        scename.append(temp_name)
    # 1) Plotting Yield 
    fig = plt.figure()
    fig.suptitle('Estimated Yields with different planting dates', fontsize=14, fontweight='bold')
    ax = fig.add_subplot(111)
    ax.set_xlabel('Planting Date[MM/DD]',fontsize=14)
    ax.set_ylabel('Yield [kg/ha]',fontsize=14)
    if obs_flag == 1:
        # Plot a line between yields with observed weather
        plt.plot(myXList, obs_yield, 'go-')
        pass
    ax.boxplot(filtered_yield,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
    fig_name_YB = Wdir_path + "\\"+ sname +"_output\\"+ sname +"_Yield_boxplot.png"
    plt.savefig(fig_name_YB)         

    # 2) Plotting ADAT
    fig2 = plt.figure()
    fig2.suptitle('Estimated Anthesis Date with different planting dates', fontsize=14, fontweight='bold')
    ax2 = fig2.add_subplot(111)
    ax2.set_xlabel('Planting Date[DOY]',fontsize=14)
    ax2.set_ylabel('Anthesis Date [DOY]',fontsize=14)
    if obs_flag == 1:
        # Plot a line between yields with observed weather
        plt.plot(myXList, obs_ADAT, 'go-')
    ax2.boxplot(filtered_data2,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
    fig_name_AB = Wdir_path + "\\"+ sname +"_output\\"+ sname +"_ADAT_boxplot.png"
    plt.savefig(fig_name_AB)         

    # 3) Plotting MDAT
    fig3 = plt.figure()
    fig3.suptitle('Estimated Maturity Date with different planting dates', fontsize=14, fontweight='bold')
    ax3 = fig3.add_subplot(111)
    ax3.set_xlabel('Planting Date[DOY]',fontsize=14)
    ax3.set_ylabel('Maturity Date [DOY]',fontsize=14)
    if obs_flag == 1:
        # Plot a line between yields with observed weather
        plt.plot(myXList, obs_MDAT, 'go-')
    ax3.boxplot(filtered_data3,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
    fig_name_MB = Wdir_path + "\\"+ sname+"_output\\"+sname+"_MDAT_boxplot.png"
    plt.savefig(fig_name_MB)        
    
    #4) Plotting yield exceedance curve
    fig4 = plt.figure()
    fig4.suptitle('Yield Exceedance Curve', fontsize=14, fontweight='bold')
    ax4 = fig4.add_subplot(111)
    ax4.set_xlabel('Yield [kg/ha]',fontsize=14)
    ax4.set_ylabel('Probability of Exceedance [-]',fontsize=14)
    plt.ylim(0, 1) 
    for x in range(len(pdate_list)):
        ax4.plot(sorted_yield_n[:,x],Fx_scf[:,x],'o-', label=scename[x])
        #vertical line for the yield with observed weather
        if obs_flag == 1:
            x_data=[obs_yield[x],obs_yield[x]] #only two points to draw a line
            y_data=[0,1]
            temp='w/ obs wth (' + scename[x] + ')'
            ax4.plot(x_data,y_data,'-.', label=temp)
            plt.ylim(0, 1)
    box = ax4.get_position()  # Shrink current axis by 15%
    ax4.set_position([box.x0, box.y0, box.width * 0.70, box.height])
    plt.grid(True)
    ax4.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
    fig_name_YE = Wdir_path + "\\"+ sname +"_output\\" + sname+"_Yield_exceedance.png"
    plt.savefig(fig_name_YE)     
    
    #5) Plotting ADAT exceedance curve
    fig5 = plt.figure()
    fig5.suptitle('Anthesis Date Exceedance Curve', fontsize=14, fontweight='bold')
    ax5 = fig5.add_subplot(111)
    ax5.set_xlabel('Anthesis Date [DOY]',fontsize=14)
    ax5.set_ylabel('Probability of Exceedance [-]',fontsize=14)
    plt.ylim(0, 1) 
    for x in range(len(pdate_list)):
        ax5.plot(sorted_ADAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
        #vertical line for the yield with observed weather
        if obs_flag == 1:
            x_data=[obs_ADAT[x],obs_ADAT[x]] #only two points to draw a line
            y_data=[0,1]
            temp='w/ obs wth (' + scename[x] + ')'
            ax5.plot(x_data,y_data,'-.', label=temp)
            plt.ylim(0, 1)
    box = ax5.get_position()
    ax5.set_position([box.x0, box.y0, box.width * 0.70, box.height])
    plt.grid(True)
    ax5.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
    fig_name_AE = Wdir_path + "\\"+ sname +"_output\\"+ sname +"_ADAT_exceedance.png"
    plt.savefig(fig_name_AE)    
    
    #6) Plotting MDAT exceedance curve
    fig6 = plt.figure()
    fig6.suptitle('Maturity Date Exceedance Curve', fontsize=14, fontweight='bold')
    ax6 = fig6.add_subplot(111)
    ax6.set_xlabel('Maturity Date [DOY]',fontsize=14)
    ax6.set_ylabel('Probability of Exceedance [-]',fontsize=14)
    plt.ylim(0, 1) 
    for x in range(len(pdate_list)):
        ax6.plot(sorted_MDAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
        #vertical line for the yield with observed weather
        if obs_flag == 1:
            x_data=[obs_MDAT[x],obs_MDAT[x]] #only two points to draw a line
            y_data=[0,1]
            temp='w/ obs wth (' + scename[x] + ')'
            ax6.plot(x_data,y_data,'-.', label=temp)
            plt.ylim(0, 1)
    box = ax6.get_position()
    ax6.set_position([box.x0, box.y0, box.width * 0.70, box.height])
    plt.grid(True)
    ax6.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
    fig_name_ME = Wdir_path + "\\"+ sname +"_output\\"+ sname +"_MDAT_exceedance.png"
    plt.savefig(fig_name_ME)    
    
    modeDisp = 'ValuesSeasonal'
    ### Store in df now:
    if scenario == 1:
        dfSavingDisp[modeDisp].iloc[0]   = fig_name_YB
        dfSavingDisp[modeDisp].iloc[1]   = fig_name_YE
        dfSavingDisp[modeDisp].iloc[2]   = fig_name_MB
        dfSavingDisp[modeDisp].iloc[3]   = fig_name_ME
        dfSavingDisp[modeDisp].iloc[4]   = fig_name_AB
        dfSavingDisp[modeDisp].iloc[5]   =fig_name_AE
    elif scenario == 2:     
        dfSavingDisp[modeDisp].iloc[12]   = fig_name_YB
        dfSavingDisp[modeDisp].iloc[13]   = fig_name_YE
        dfSavingDisp[modeDisp].iloc[14]   = fig_name_MB
        dfSavingDisp[modeDisp].iloc[15]   = fig_name_ME
        dfSavingDisp[modeDisp].iloc[16]   = fig_name_AB
        dfSavingDisp[modeDisp].iloc[17]   =fig_name_AE
    elif scenario == 3:     
        dfSavingDisp[modeDisp].iloc[24]   = fig_name_YB
        dfSavingDisp[modeDisp].iloc[25]   = fig_name_YE
        dfSavingDisp[modeDisp].iloc[26]   = fig_name_MB
        dfSavingDisp[modeDisp].iloc[27]   = fig_name_ME
        dfSavingDisp[modeDisp].iloc[28]   = fig_name_AB
        dfSavingDisp[modeDisp].iloc[29]   =fig_name_AE    
    
    
    
    
    
def Yield_boxplot_hist(SNX_list, sim_years, obs_flag):
    """"""
    global sname, start_year, scenario, mode, modeDisp, dfSavingDisp
    
    fname1 = Wdir_path + "\\"+ sname+"_output_hist\\SUMMARY.OUT"
    pdate_list = []
    year_list = []
    for i in range(len(SNX_list)):
        pdate_list.append(int(SNX_list[i][-11:-8]))
        if i == 0:
            year_list.append(int(start_year))
        else:
            if pdate_list[i] > pdate_list[i-1]:
                year_list.append(int(start_year))
            else:
                year_list.append(int(start_year) + 1)
                
    df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
    
    PDAT = df_OUT.ix[:,13].values  #read 14th column only => Planting date 
    HWAM = df_OUT.ix[:,20].values  #read 21th column only
    ADAT = df_OUT.ix[:,15].values  #read 21th column only
    MDAT = df_OUT.ix[:,16].values  #read 21th column only

    year_array =  np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
    DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
    ADAT_array = np.zeros(len(ADAT)) #np.empty((ndays,1))*np.nan #initialize
    MDAT_array = np.zeros(len(MDAT)) #np.empty((ndays,1))*np.nan #initialize     
 
    for i in range(len(HWAM)):
        year_array[i] = int(repr(PDAT[i])[:4])
        DOY_array[i] = int(repr(PDAT[i])[4:])
        #MDAT_array[i] = int(repr(MDAT[i])[4:])
        if ADAT[i] == 0:  #in case emergence does not happen
            ADAT_array[i] = np.nan
            MDAT_array[i] = np.nan
        elif ADAT[i] == -99:  #in case emergence does not happen
            ADAT_array[i] = np.nan
            MDAT_array[i] = np.nan
        elif MDAT[i] == -99: 
            MDAT_array[i] = np.nan
        else:
            ADAT_array[i] = int(repr(ADAT[i])[4:])
            MDAT_array[i] = int(repr(MDAT[i])[4:])
            
    #make three arrays into a dataframe
    df = pd.DataFrame({'Year': year_array,'DOY': DOY_array, 'ADAT':ADAT_array, 'MDAT':MDAT_array, 'HWAM':HWAM}, columns=['Year','DOY','ADAT','MDAT','HWAM'])

    # #make an empty array [nyears * n_plt_dates]
    n_year = sim_years
    
    # For Boxplot
    yield_n = np.empty([n_year,len(pdate_list)])*np.nan
    ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan
    MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
    
    # For exceedance curve
    sorted_yield_n = np.empty([n_year,len(pdate_list)])*np.nan
    Fx_scf = np.empty([n_year,len(pdate_list)])*np.nan       
    sorted_ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan   
    Fx_scf_ADAT = np.empty([n_year,len(pdate_list)])*np.nan
    sorted_MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
    
    # Fx_scf_MDAT = np.empty([n_year,len(pdate_list)])*np.nan    
    for count in range(len(pdate_list)):
        Pdate = pdate_list[count]
        yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values  
        ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values 
        MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values  
        sorted_yield_n[:,count] = np.sort(df.HWAM[(df["DOY"] == Pdate)].values)
        fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
        #Fx_scf = np.cumsum(fx_scf)
        Fx_scf[:,count] = 1.0-np.cumsum(fx_scf)  #for exceedance curve
        sorted_ADAT_n[:,count] = np.sort(df.ADAT[(df["DOY"] == Pdate)].values)
        sorted_MDAT_n[:,count] = np.sort(df.MDAT[(df["DOY"] == Pdate)].values)
        temp = sorted_ADAT_n[:,count]
        temp2 = temp[~np.isnan(temp)]
        fx_scf_ADAT = [1.0/len(temp2)] * len(temp2) #pdf
        Fx_scf_ADAT[:len(fx_scf_ADAT),count] = 1.0-np.cumsum(fx_scf_ADAT)  #for exceedance curve      
    
    
    if obs_flag == 1:
        obs_yield = []
        obs_ADAT = []
        obs_MDAT = []
        for count in range(len(pdate_list)):
            Pdate = pdate_list[count]
            if count > 0 and pdate_list[count] < pdate_list[count-1]:  #when two consecutive years
                start_year = repr(int(start_year)+1)
            #collect simulated results from observed weather
            obs_yield.append(df.HWAM[(df["DOY"] == Pdate) & (df["Year"] == int(start_year))].values[0])      
            obs_ADAT.append(df.ADAT[(df["DOY"] == Pdate) & (df["Year"] == int(start_year))].values[0])
            obs_MDAT.append(df.MDAT[(df["DOY"] == Pdate) & (df["Year"] == int(start_year))].values[0])
    
    # Fx_scf_ADAT = Fx_scf
    #To remove 'nan' from data array
    #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
    mask = ~np.isnan(yield_n)
    filtered_yield =  [d[m] for d, m in zip(yield_n.T, mask.T)]
    mask2 = ~np.isnan(ADAT_n)
    filtered_data2 = [d[m] for d, m in zip(ADAT_n.T, mask2.T)]
    mask3 = ~np.isnan(MDAT_n)
    filtered_data3 = [d[m] for d, m in zip(MDAT_n.T, mask3.T)]   
        
    if obs_flag == 1: 
        #replace ADAT = 0 with nan
        obs_ADAT = [np.nan if x==1 else x for x in obs_ADAT]
        obs_MDAT = [np.nan if x==1 else x for x in obs_MDAT]
        
    
    #X data for plot
    myXList = [i+1 for i in range(len(pdate_list))]
    scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
    for count in range(len(pdate_list)):
        temp_str = repr(year_list[count]) + " " + repr(pdate_list[count]) 
        temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
        scename.append(temp_name)    
        
    # 1) Plotting Yield 
    fig = plt.figure()
    fig.suptitle('Estimated Yields using historical weather', fontsize=12, fontweight='bold')
    ax = fig.add_subplot(111)
    ax.set_xlabel('Planting Date[MM/DD]',fontsize=14)
    ax.set_ylabel('Yield [kg/ha]',fontsize=14)    
    
    if obs_flag == 1:
        # Plot a line between yields with observed weather
        plt.plot(myXList, obs_yield, 'go-')
        #pass
    
    ax.boxplot(filtered_yield,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
    fig_name_YB = Wdir_path + "\\"+sname+"_output_hist\\"+sname+"_Yield_boxplot.png"
    plt.savefig(fig_name_YB)    
        
    # 2) Plotting ADAT
    fig2 = plt.figure()
    fig2.suptitle('Estimated Anthesis Dates using historical weather', fontsize=12, fontweight='bold')
    ax2 = fig2.add_subplot(111)
    ax2.set_xlabel('Planting Date[DOY]',fontsize=14)
    ax2.set_ylabel('Anthesis Date [DOY]',fontsize=14)
    if obs_flag == 1:
        # Plot a line between yields with observed weather
        plt.plot(myXList, obs_ADAT, 'go-')
    ax2.boxplot(filtered_data2,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
    fig_name_AB = Wdir_path + "\\"+ sname+"_output_hist\\"+sname+"_ADAT_boxplot.png"
    plt.savefig(fig_name_AB)  
    
    # 3) Plotting MDAT
    fig3 = plt.figure()
    fig3.suptitle('Estimated Maturity Dates using historical weather', fontsize=12, fontweight='bold')
    ax3 = fig3.add_subplot(111)
    ax3.set_xlabel('Planting Date[DOY]',fontsize=14)
    ax3.set_ylabel('Maturity Date [DOY]',fontsize=14)
    if obs_flag == 1:
        # Plot a line between yields with observed weather
        plt.plot(myXList, obs_MDAT, 'go-')
    ax3.boxplot(filtered_data3,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
    fig_name_MB = Wdir_path + "\\"+ sname+"_output_hist\\"+sname+"_MDAT_boxplot.png"
    plt.savefig(fig_name_MB)    
        
    #4) Plotting yield exceedance curve
    fig4 = plt.figure()
    fig4.suptitle('Yield Exceedance Curve using historical weather', fontsize=12, fontweight='bold')
    ax4 = fig4.add_subplot(111)
    ax4.set_xlabel('Yield [kg/ha]',fontsize=14)
    ax4.set_ylabel('Probability of Exceedance [-]',fontsize=14)
    plt.ylim(0, 1) 
    for x in range(len(pdate_list)):
        ax4.plot(sorted_yield_n[:,x],Fx_scf[:,x],'o-', label=scename[x])
        #vertical line for the yield with observed weather
        if obs_flag == 1:
            x_data=[obs_yield[x],obs_yield[x]] #only two points to draw a line
            y_data=[0,1]
            temp='w/ obs wth (' + scename[x] + ')'
            ax4.plot(x_data,y_data,'-.', label=temp)
            plt.ylim(0, 1)
    box = ax4.get_position()  # Shrink current axis by 15%
    ax4.set_position([box.x0, box.y0, box.width * 0.70, box.height])
    plt.grid(True)
    ax4.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
    fig4.set_size_inches(10, 8)
    fig_name_YE = Wdir_path + "\\" + sname + "_output_hist\\" + sname +"_Yield_exceedance.png"
    plt.savefig(fig_name_YE)        
    
    #5) Plotting ADAT exceedance curve
    fig5 = plt.figure()
    fig5.suptitle('Anthesis Date Exceedance Curve using historical weather', fontsize=12, fontweight='bold')
    ax5 = fig5.add_subplot(111)
    ax5.set_xlabel('Anthesis Date [DOY]',fontsize=14)
    ax5.set_ylabel('Probability of Exceedance [-]',fontsize=14)
    plt.ylim(0, 1) 
    for x in range(len(pdate_list)):
        ax5.plot(sorted_ADAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
        #vertical line for the yield with observed weather
        if obs_flag == 1:
            x_data=[obs_ADAT[x],obs_ADAT[x]] #only two points to draw a line
            y_data=[0,1]
            temp='w/ obs wth (' + scename[x] + ')'
            ax5.plot(x_data,y_data,'-.', label=temp)
            plt.ylim(0, 1)
    box = ax5.get_position()
    ax5.set_position([box.x0, box.y0, box.width * 0.70, box.height])
    plt.grid(True)
    ax5.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
    fig5.set_size_inches(10, 8)
    fig_name_AE = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_ADAT_exceedance.png"
    plt.savefig(fig_name_AE)      
        
        
    #6) Plotting MDAT exceedance curve
    fig6 = plt.figure()
    fig6.suptitle('Maturity Date Exceedance Curve using historical weather', fontsize=12, fontweight='bold')
    ax6 = fig6.add_subplot(111)
    ax6.set_xlabel('Maturity Date [DOY]',fontsize=14)
    ax6.set_ylabel('Probability of Exceedance [-]',fontsize=14)
    plt.ylim(0, 1) 
    for x in range(len(pdate_list)):
        ax6.plot(sorted_MDAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
        #vertical line for the yield with observed weather
        if obs_flag == 1:
            x_data=[obs_MDAT[x],obs_MDAT[x]] #only two points to draw a line
            y_data=[0,1]
            temp='w/ obs wth (' + scename[x] + ')'
            ax6.plot(x_data,y_data,'-.', label=temp)
            plt.ylim(0, 1)
    box = ax6.get_position()
    ax6.set_position([box.x0, box.y0, box.width * 0.70, box.height])
    plt.grid(True)
    ax6.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
    fig6.set_size_inches(10, 8)
    fig_name_ME = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_MDAT_exceedance.png"
    plt.savefig(fig_name_ME)         
    
    modeDisp = 'ValuesHistorical'
    
    ### Store in df now:
    if scenario == 1:
        dfSavingDisp[modeDisp].iloc[0]   = fig_name_YB
        dfSavingDisp[modeDisp].iloc[1]   = fig_name_YE
        dfSavingDisp[modeDisp].iloc[2]   = fig_name_MB
        dfSavingDisp[modeDisp].iloc[3]   = fig_name_ME
        dfSavingDisp[modeDisp].iloc[4]   = fig_name_AB
        dfSavingDisp[modeDisp].iloc[5]   =fig_name_AE
    elif scenario == 2:     
        dfSavingDisp[modeDisp].iloc[12]   = fig_name_YB
        dfSavingDisp[modeDisp].iloc[13]   = fig_name_YE
        dfSavingDisp[modeDisp].iloc[14]   = fig_name_MB
        dfSavingDisp[modeDisp].iloc[15]   = fig_name_ME
        dfSavingDisp[modeDisp].iloc[16]   = fig_name_AB
        dfSavingDisp[modeDisp].iloc[17]   =fig_name_AE
    elif scenario == 3:     
        dfSavingDisp[modeDisp].iloc[24]   = fig_name_YB
        dfSavingDisp[modeDisp].iloc[25]   = fig_name_YE
        dfSavingDisp[modeDisp].iloc[26]   = fig_name_MB
        dfSavingDisp[modeDisp].iloc[27]   = fig_name_ME
        dfSavingDisp[modeDisp].iloc[28]   = fig_name_AB
        dfSavingDisp[modeDisp].iloc[29]   =fig_name_AE
    

    
def WSGD_plot(SNX_list, sim_years, obs_flag):
    """"""
    global sname, start_year, scenario, mode, modeDisp, dfSavingDisp
    
    fname1 = Wdir_path + "\\"+ sname+"_output\\PlantGro.OUT"
    pdate_list = []
    # year_list = []
    for i in range(len(SNX_list)):
        pdate_list.append(SNX_list[i][-11:-8])     
    if obs_flag == 1:
        nrealiz = 101
    else:
        nrealiz = 100
    n_days = 500 #approximate growing days
    WSGD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
    NSTD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN 
    read_flag = 0  #off
    n_count = 0  #count of n weahter realizations
    d_count = 0  #day counter during a growing season
    p_index_old = 0
    p_count = 0  #count for different planting date scenario    
    
    with open(fname1) as fp:
        for line in fp:
            if line[18:21] == '  0':   #line[18:21] => DAP
                p_index = pdate_list.index(line[6:9])   #line[6:9] => DOY
                read_flag = 1  #on
                if p_index > p_index_old:  #move to a new planting date scenario
                    p_count = p_count + 1
                    n_count = 0
            #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
            if read_flag ==1 and line == '\n':  #after reading last growing day
                read_flag = 0 #off
                p_index_old = p_index
                d_count = 0
                n_count = n_count + 1
            if read_flag == 1:
                WSGD_3D[d_count, n_count, p_count] = float(line[127:133])
                NSTD_3D[d_count, n_count, p_count] = float(line[134:140])
                d_count = d_count + 1    
    
    # WSGD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
    # NSTD_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
    WSGD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
    WSGD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    WSGD_75= np.empty((n_days,len(pdate_list),))*np.NAN 
    NSTD_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
    NSTD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    NSTD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    if obs_flag == 1:
        WSGD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
        NSTD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 

    #plot
    scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
    for count in range(len(pdate_list)):
        if count > 0:
            if int(pdate_list[count]) < int(pdate_list[count-1]):
                start_year = repr(int(start_year) + 1)
        temp_str = str(start_year) + " " + pdate_list[count]
        temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
        scename.append(temp_name)    
        
    #==================================
    for i in range(len(pdate_list)):
        # WSGD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
        # NSTD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
        WSGD_50[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 50, axis = 1)
        WSGD_25[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 25, axis = 1)
        WSGD_75[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 75, axis = 1)
        NSTD_50[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 50, axis = 1)
        NSTD_25[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 25, axis = 1)
        NSTD_75[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 75, axis = 1)
        if obs_flag == 1:
            WSGD_obs[:, i] = WSGD_3D[:, -1, i]
            NSTD_obs[:, i] = NSTD_3D[:, -1, i]   
   #==================================
   
   #1) Plot WSGD Water stress
    nrow = math.ceil(len(pdate_list)/2.0)
    ncol = 2
    #fig, axs = plt.subplots(nrow, ncol)
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('WSGD (Water Stress Index)')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                xdata = range(len(WSGD_50[:,0]))
                axs[j, i].plot(xdata, WSGD_50[:, p_count-1], 'w')
            else:
                xdata = range(len(WSGD_50[:,p_count]))
                yerr_low = WSGD_50[:, p_count] -WSGD_25[:, p_count]
                yerr_up = WSGD_75[:, p_count] - WSGD_50[:, p_count]
                axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(WSGD_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After Planting [DAP]', ylabel='Water Stress Index [-]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(13, 8)
    fig_nameWST = Wdir_path + "\\"+ sname +"_output\\" + sname +"_WStress.png"
    #plt.show()
    plt.savefig(fig_nameWST,dpi=100)   
   
    #2) Plot NSTD Nitrogen stress
    nrow = math.ceil(len(pdate_list)/2.0)
    ncol = 2
    #fig, axs = plt.subplots(nrow, ncol)
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('NSTD (Nitrogen Stress Index)')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                xdata = range(len(NSTD_50[:,0]))
                axs[j, i].plot(xdata, NSTD_50[:, p_count-1], 'w')
            else:
                xdata = range(len(NSTD_50[:,p_count]))
                yerr_low = NSTD_50[:, p_count] -NSTD_25[:, p_count]
                yerr_up = NSTD_75[:, p_count] - NSTD_50[:, p_count]
                axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(NSTD_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After Planting [DAP]', ylabel='Nitrogen Stress Index [-]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(12, 8)
    fig_nameNST = Wdir_path + "\\"+ sname +"_output\\" + sname +"_NStress.png"
    #plt.show()
    plt.savefig(fig_nameNST,dpi=100)   
    
    modeDisp = 'ValuesSeasonal'
    
    ### Store in df now:
    if scenario == 1:
        dfSavingDisp[modeDisp].iloc[7]   = fig_nameNST
        dfSavingDisp[modeDisp].iloc[11]   = fig_nameWST
    elif scenario == 2:     
        dfSavingDisp[modeDisp].iloc[19]   = fig_nameNST
        dfSavingDisp[modeDisp].iloc[23]   = fig_nameWST
    elif scenario == 3:     
        dfSavingDisp[modeDisp].iloc[31]   = fig_nameNST
        dfSavingDisp[modeDisp].iloc[35]   = fig_nameWST  
    
    
    
def WSGD_plot_hist(SNX_list, sim_years, obs_flag):
    """"""
    global sname, start_year, scenario, mode, modeDisp, dfSavingDisp
    
    fname1 = Wdir_path + "\\"+ sname+"_output_hist\\PlantGro.OUT"
    pdate_list = []
    # year_list = []
    for i in range(len(SNX_list)):
        pdate_list.append(SNX_list[i][-11:-8])
                
    n_days = 300 #approximate growing days
    WSGD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
    NSTD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN 
    if obs_flag == 1:
        WSGD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
        NSTD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
    read_flag = 0  #off
    n_count = 0  #count of n weahter realizations
    d_count = 0  #day counter during a growing season
    p_index_old = 0
    p_count = 0  #count for different planting date scenario
    obs_read_flag = 0   
    
    with open(fname1) as fp:
        for line in fp:
            if line[18:21] == '  0':   #line[18:21] => DAP
                p_index = pdate_list.index(line[6:9])   #line[6:9] => DOY
                read_flag = 1  #on
                if line[1:5] == start_year:  #if this is the observed year
                    obs_read_flag = 1  #on
                if p_index > p_index_old:  #move to a new planting date scenario
                    p_count = p_count + 1
                    n_count = 0
            #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
            if read_flag ==1 and line == '\n':  #after reading last growing day
                read_flag = 0 #off
                obs_read_flag = 0
                p_index_old = p_index
                d_count = 0
                n_count = n_count + 1
            if read_flag == 1:
                WSGD_3D[d_count, n_count, p_count] = float(line[127:133])
                NSTD_3D[d_count, n_count, p_count] = float(line[134:140])
                if obs_flag == 1 and obs_read_flag == 1:
                    WSGD_obs[d_count, p_count] = float(line[127:133])
                    NSTD_obs[d_count, p_count] = float(line[134:140])
                d_count = d_count + 1     
    ###########################
    # WSGD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
    # NSTD_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
    WSGD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
    WSGD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    WSGD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    NSTD_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
    NSTD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    NSTD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 

    #plot
    scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
    for count in range(len(pdate_list)):
        if count > 0:
            if int(pdate_list[count]) < int(pdate_list[count-1]):
                start_year = repr(int(start_year) + 1)
        temp_str = str(start_year) + " " + pdate_list[count]
        temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
        scename.append(temp_name)    
    ###############
    
    for i in range(len(pdate_list)):
        # WSGD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
        # NSTD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
        WSGD_50[:, i] = np.nanpercentile(WSGD_3D[:, 0:sim_years, i], 50, axis = 1)
        WSGD_25[:, i] = np.nanpercentile(WSGD_3D[:, 0:sim_years, i], 25, axis = 1)
        WSGD_75[:, i] = np.nanpercentile(WSGD_3D[:, 0:sim_years, i], 75, axis = 1)
        NSTD_50[:, i] = np.nanpercentile(NSTD_3D[:, 0:sim_years, i], 50, axis = 1)
        NSTD_25[:, i] = np.nanpercentile(NSTD_3D[:, 0:sim_years, i], 25, axis = 1)
        NSTD_75[:, i] = np.nanpercentile(NSTD_3D[:, 0:sim_years, i], 75, axis = 1)
    #==================================     
    
    #1) Plot WSGD Water stress
    nrow = math.ceil(len(pdate_list)/2.0)
    ncol = 2
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('WSGD (Water Stress Index)')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                xdata = range(len(WSGD_50[:,0]))
                axs[j, i].plot(xdata, WSGD_50[:, p_count-1], 'w')
            else:
                xdata = range(len(WSGD_50[:,p_count]))
                yerr_low = WSGD_50[:, p_count] -WSGD_25[:, p_count]
                yerr_up = WSGD_75[:, p_count] - WSGD_50[:, p_count]
                axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(WSGD_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After Planting [DAP]', ylabel='Water Stress Index [-]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(13, 8)
    fig_nameWST = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_WStress.png"
    #plt.show()
    plt.savefig(fig_nameWST,dpi=100)    
    
    #2) Plot NSTD Nitrogen stress
    nrow = math.ceil(len(pdate_list)/2.0)
    ncol = 2
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('NSTD (Nitrogen Stress Index)')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                xdata = range(len(NSTD_50[:,0]))
                axs[j, i].plot(xdata, NSTD_50[:, p_count-1], 'w')
            else:
                xdata = range(len(NSTD_50[:,p_count]))
                yerr_low = NSTD_50[:, p_count] -NSTD_25[:, p_count]
                yerr_up = NSTD_75[:, p_count] - NSTD_50[:, p_count]
                axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(NSTD_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After Planting [DAP]', ylabel='Nitrogen Stress Index [-]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(12, 8)
    fig_nameNST = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_NStress.png"
    #plt.show()
    plt.savefig(fig_nameNST,dpi=100)    
    
    modeDisp = 'ValuesHistorical'
    
    ### Store in df now:
    if scenario == 1:
        dfSavingDisp[modeDisp].iloc[7]   = fig_nameNST
        dfSavingDisp[modeDisp].iloc[11]   = fig_nameWST
    elif scenario == 2:     
        dfSavingDisp[modeDisp].iloc[19]   = fig_nameNST
        dfSavingDisp[modeDisp].iloc[23]   = fig_nameWST
    elif scenario == 3:     
        dfSavingDisp[modeDisp].iloc[31]   = fig_nameNST
        dfSavingDisp[modeDisp].iloc[35]   = fig_nameWST     
 
    
def weather_plot(SNX_list, sim_years, obs_flag):
    """"""
    global sname, start_year, scenario, mode, modeDisp, dfSavingDisp
    
    # TMND   MIN TEMP deg C     Minimum daily temperature (degC)                           .
    # TMXD   MAX TEMP deg C     Maximum daily temperature (deg C) 
    # PRED   PRECIP mm/d     Precipitation depth (mm/d) ==>> ??????                        .
    # SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d))                              .
    # TAVD   AVG TEMP deg C     Average daily temperature (deg C)
    fname1 = Wdir_path + "\\"+ sname+"_output\\Weather.OUT"
    pdate_list = []
    for i in range(len(SNX_list)):
        pdate_list.append(SNX_list[i][-11:-8])

    if obs_flag == 1:
        nrealiz = 101
    else:
        nrealiz = 100
    n_days = 500 #approximate growing days
    TMXD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
    TMND_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN 
    SRAD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN 
    read_flag = 0  #off
    n_count = 0  #count of n weahter realizations
    d_count = 0  #day counter during a growing season
    p_count = 0  #count for different planting date scenario    
    
    with open(fname1) as fp:
        for line in fp:
            if line[6:9] == pdate_list[p_count]:   #line[12:15] => DAS
                read_flag = 1  #on
            if read_flag ==1 and line == '\n':  #after reading last growing day
                read_flag = 0 #off
                d_count = 0
                n_count = n_count + 1
                if n_count == nrealiz:  #move to next planting date scenario
                    p_count = p_count + 1
                    n_count = 0
            if read_flag == 1:
                TMXD_3D[d_count, n_count, p_count] = float(line[60:64])
                TMND_3D[d_count, n_count, p_count] = float(line[67:71])
                SRAD_3D[d_count, n_count, p_count] = float(line[39:43])
                d_count = d_count + 1
    
    # TMXD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
    # TMND_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
    TMXD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
    TMXD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    TMXD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    TMND_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
    TMND_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    TMND_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    SRAD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
    SRAD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    SRAD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    if obs_flag == 1:
        TMXD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMND_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 

    #plot
    scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
    for count in range(len(pdate_list)):
        if count > 0:
            if int(pdate_list[count]) < int(pdate_list[count-1]):
                start_year = repr(int(start_year) + 1)
        temp_str = str(start_year) + " " + pdate_list[count]
        temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
        scename.append(temp_name)

    for i in range(len(pdate_list)):
        TMXD_50[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 50, axis = 1)
        TMXD_25[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 25, axis = 1)
        TMXD_75[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 75, axis = 1)
        TMND_50[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 50, axis = 1)
        TMND_25[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 25, axis = 1)
        TMND_75[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 75, axis = 1)
        SRAD_50[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 50, axis = 1)
        SRAD_25[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 25, axis = 1)
        SRAD_75[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 75, axis = 1)
        if obs_flag == 1:
            TMXD_obs[:, i] = TMXD_3D[:, -1, i]
            TMND_obs[:, i] = TMND_3D[:, -1, i]
            SRAD_obs[:, i] = SRAD_3D[:, -1, i]    
    
    #1) Plot TMXD
    nrow = math.ceil(len(pdate_list)/2.0)
    ncol = 2
    # fig, axs = plt.subplots(nrow, ncol)
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('TMXD (Maximum daily temperature)')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                xdata = range(len(TMXD_50[:,0]))
                axs[j, i].plot(xdata, TMXD_50[:, p_count-1], 'w')
            else:
                xdata = range(len(TMXD_50[:,p_count]))
                yerr_low = TMXD_50[:, p_count] -TMXD_25[:, p_count]
                yerr_up = TMXD_75[:, p_count] - TMXD_50[:, p_count]
                axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(TMXD_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(12, 8)
    fig_nameTX = Wdir_path + "\\"+ sname +"_output\\" + sname +"_Tmax.png"
    #plt.show()
    plt.savefig(fig_nameTX,dpi=100)    
    
    
    #2) Plot TMND
    # fig, axs = plt.subplots(nrow, ncol)
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('TMND (Minimum daily temperature)')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                axs[j, i].plot(xdata, TMND_50[:, p_count-1], 'w')
            else:
                xdata = range(len(TMND_50[:,p_count]))
                yerr_low = TMND_50[:, p_count] -TMND_25[:, p_count]
                yerr_up = TMND_75[:, p_count] - TMND_50[:, p_count]
                axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(TMND_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(12, 8)
    fig_nameTN = Wdir_path + "\\"+ sname +"_output\\" + sname +"_Tmin.png"
    #plt.show()
    plt.savefig(fig_nameTN,dpi=100)    
    
    #3) Plot SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d)) 
    # fig, axs = plt.subplots(nrow, ncol)
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('SARD (Daily solar radiation (MJ/(m2.d))')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                axs[j, i].plot(xdata, SRAD_50[:, p_count-1], 'w')
            else:
                xdata = range(len(SRAD_50[:,p_count]))
                yerr_low = SRAD_50[:, p_count] -SRAD_25[:, p_count]
                yerr_up = SRAD_75[:, p_count] - SRAD_50[:, p_count]
                axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(SRAD_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After Planting [DAP]', ylabel='SRAD[MJ/(m2.d)]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(12, 8)
    fig_nameSRAD = Wdir_path + "\\"+ sname +"_output\\" + sname +"_SRAD.png"
    #plt.show()
    plt.savefig(fig_nameSRAD, dpi=100)    
    
    
    
    
    modeDisp = 'ValuesSeasonal'
    
    ### Store in df now:
    if scenario == 1:
        dfSavingDisp[modeDisp].iloc[8]   = fig_nameSRAD
        dfSavingDisp[modeDisp].iloc[9]   = fig_nameTX
        dfSavingDisp[modeDisp].iloc[10]   = fig_nameTN
    elif scenario == 2:     
        dfSavingDisp[modeDisp].iloc[20]   = fig_nameSRAD
        dfSavingDisp[modeDisp].iloc[21]   = fig_nameTX
        dfSavingDisp[modeDisp].iloc[22]   = fig_nameTN
    elif scenario == 3:     
        dfSavingDisp[modeDisp].iloc[32]   = fig_nameSRAD
        dfSavingDisp[modeDisp].iloc[33]   = fig_nameTX
        dfSavingDisp[modeDisp].iloc[34]   = fig_nameTN  
    
    
    
    
    
    
def weather_plot_hist(SNX_list,  sim_years, obs_flag):
    """"""
    global sname, start_year, scenario, mode, modeDisp, dfSavingDisp
    
    fname1 = Wdir_path + "\\"+ sname+"_output_hist\\Weather.OUT"
    # TMND   MIN TEMP �C     Minimum daily temperature (�C)                           .
    # TMXD   MAX TEMP �C     Maximum daily temperature (�C) 
    # PRED   PRECIP mm/d     Precipitation depth (mm/d) ==>> ??????                        .
    # SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d))                              .
    # TAVD   AVG TEMP �C     Average daily temperature (�C)
    pdate_list = []
    for i in range(len(SNX_list)):
        pdate_list.append(SNX_list[i][-11:-8])

    n_days = 500 #approximate growing days
    TMXD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
    TMND_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN 
    SRAD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN 
    if obs_flag == 1:
        TMXD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMND_obs = np.empty((n_days,len(pdate_list),))*np.NAN
        SRAD_obs = np.empty((n_days,len(pdate_list),))*np.NAN

    read_flag = 0  #off
    n_count = 0  #count of n weahter realizations
    d_count = 0  #day counter during a growing season
    p_count = 0  #count for different planting date scenario
    obs_read_flag = 0    
    
    with open(fname1) as fp:
        for line in fp:
            if line[6:9] == pdate_list[p_count]:   #line[12:15] => DAS
                read_flag = 1  #on
                if line[1:5] == start_year:  #if this is the observed year
                    obs_read_flag = 1  #on
            if read_flag ==1 and line == '\n':  #after reading last growing day
                read_flag = 0 #off
                obs_read_flag = 0  #off
                d_count = 0
                n_count = n_count + 1
                if n_count == sim_years:  #move to next planting date scenario
                    p_count = p_count + 1
                    n_count = 0
            if read_flag == 1:
                TMXD_3D[d_count, n_count, p_count] = float(line[60:64])
                TMND_3D[d_count, n_count, p_count] = float(line[67:71])
                SRAD_3D[d_count, n_count, p_count] = float(line[39:43])
                if obs_flag == 1 and obs_read_flag == 1:
                    TMXD_obs[d_count, p_count] = float(line[60:64])
                    TMND_obs[d_count, p_count] = float(line[67:71])
                    SRAD_obs[d_count, p_count] = float(line[39:43])
                d_count = d_count + 1
    
    # TMXD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
    # TMND_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
    TMXD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
    TMXD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    TMXD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    TMND_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
    TMND_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    TMND_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    SRAD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
    SRAD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    SRAD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 

    #plot
    scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
    for count in range(len(pdate_list)):
        if count > 0:
            if int(pdate_list[count]) < int(pdate_list[count-1]):
                start_year = repr(int(start_year) + 1)
        temp_str = str(start_year) + " " + pdate_list[count]
        temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
        scename.append(temp_name)

    for i in range(len(pdate_list)):
        TMXD_50[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 50, axis = 1)
        TMXD_25[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 25, axis = 1)
        TMXD_75[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 75, axis = 1)
        TMND_50[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 50, axis = 1)
        TMND_25[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 25, axis = 1)
        TMND_75[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 75, axis = 1)
        SRAD_50[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 50, axis = 1)
        SRAD_25[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 25, axis = 1)
        SRAD_75[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 75, axis = 1)    
    #================================== 
    
    #1) Plot TMXD
    nrow = math.ceil(len(pdate_list)/2.0)
    ncol = 2
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('TMXD (Maximum daily temperature)')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                xdata = range(len(TMXD_50[:,0]))
                axs[j, i].plot(xdata, TMXD_50[:, p_count-1], 'w')
            else:
                xdata = range(len(TMXD_50[:,p_count]))
                yerr_low = TMXD_50[:, p_count] -TMXD_25[:, p_count]
                yerr_up = TMXD_75[:, p_count] - TMXD_50[:, p_count]
                axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(TMXD_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(12, 8)
    fig_nameTX = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_Tmax.png"
    #plt.show()
    plt.savefig(fig_nameTX,dpi=100)    
    
    #==================================
    #2) Plot TMND
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('TMND (Minimum daily temperature)')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                axs[j, i].plot(xdata, TMND_50[:, p_count-1], 'w')
            else:
                xdata = range(len(TMND_50[:,p_count]))
                yerr_low = TMND_50[:, p_count] -TMND_25[:, p_count]
                yerr_up = TMND_75[:, p_count] - TMND_50[:, p_count]
                axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(TMND_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(12, 8)
    fig_nameTN = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_Tmin.png"
    #plt.show()
    plt.savefig(fig_nameTN,dpi=100)    
    
    #==================================
    #3) Plot SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d)) 
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('SARD (Daily solar radiation (MJ/(m2.d))')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                axs[j, i].plot(xdata, SRAD_50[:, p_count-1], 'w')
            else:
                xdata = range(len(SRAD_50[:,p_count]))
                yerr_low = SRAD_50[:, p_count] -SRAD_25[:, p_count]
                yerr_up = SRAD_75[:, p_count] - SRAD_50[:, p_count]
                axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(SRAD_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After Planting [DAP]', ylabel='SRAD[MJ/(m2.d)]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(12, 8)
    fig_nameSRAD = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_SRAD.png"
    #plt.show()
    plt.savefig(fig_nameSRAD,dpi=100)    
    
    
    modeDisp = 'ValuesHistorical'
    
    ### Store in df now:
    if scenario == 1:
        dfSavingDisp[modeDisp].iloc[8]   = fig_nameSRAD
        dfSavingDisp[modeDisp].iloc[9]   = fig_nameTX
        dfSavingDisp[modeDisp].iloc[10]   = fig_nameTN
    elif scenario == 2:     
        dfSavingDisp[modeDisp].iloc[20]   = fig_nameSRAD
        dfSavingDisp[modeDisp].iloc[21]   = fig_nameTX
        dfSavingDisp[modeDisp].iloc[22]   = fig_nameTN
    elif scenario == 3:     
        dfSavingDisp[modeDisp].iloc[32]   = fig_nameSRAD
        dfSavingDisp[modeDisp].iloc[33]   = fig_nameTX
        dfSavingDisp[modeDisp].iloc[34]   = fig_nameTN      
    
    
    
    
def plot_cum_rain(SNX_list, sim_years, obs_flag):
    """"""
    global sname, start_year, scenario, mode, modeDisp, dfSavingDisp
    
    fname1 = Wdir_path + "\\"+ sname+"_output\\SoilWat.OUT"
    pdate_list = []
    for i in range(len(SNX_list)):
        pdate_list.append(SNX_list[i][-11:-8])

    if obs_flag == 1:
        nrealiz = 101
    else:
        nrealiz = 100
    n_days = 500 #approximate growing days + 30 days from IC to plt date
    PREC_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]

    read_flag = 0  #off
    n_count = 0  #count of n weahter realizations
    d_count = 0  #day counter during a growing season
    p_count = 0  #count for different planting date scenario
    with open(fname1) as fp:
        for line in fp:
            if line[12:15] == '  0':   #line[12:15] => DAS
                read_flag = 1  #on
            if read_flag ==1 and line == '\n':  #after reading last growing day
                read_flag = 0 #off
                d_count = 0
                n_count = n_count + 1
                if n_count == nrealiz:  #move to next planting date scenario
                    p_count = p_count + 1
                    n_count = 0
            if read_flag == 1:
                PREC_3D[d_count, n_count, p_count] = float(line[44:48])
                d_count = d_count + 1

    # PREC_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
    PREC_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
    PREC_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    PREC_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    if obs_flag == 1:
        PREC_obs = np.empty((n_days,len(pdate_list),))*np.NAN 

    #plot
    scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
    for count in range(len(pdate_list)):
        if count > 0:
            if int(pdate_list[count]) < int(pdate_list[count-1]):
                start_year = repr(int(start_year) + 1)
        temp_str = str(start_year) + " " + pdate_list[count]
        temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
        scename.append(temp_name)

    for i in range(len(pdate_list)):
        PREC_50[:, i] = np.nanpercentile(PREC_3D[:, 0:100, i], 50, axis = 1)
        PREC_25[:, i] = np.nanpercentile(PREC_3D[:, 0:100, i], 25, axis = 1)
        PREC_75[:, i] = np.nanpercentile(PREC_3D[:, 0:100, i], 75, axis = 1)
        if obs_flag == 1:
            PREC_obs[:, i] = PREC_3D[:, -1, i]
    #================================== 
    #1) Plot PREC
    nrow = math.ceil(len(pdate_list)/2.0)
    ncol = 2
    # fig, axs = plt.subplots(nrow, ncol)
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('Cumulative Precipitation')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                xdata = range(len(PREC_50[:,0]))
                axs[j, i].plot(xdata, PREC_50[:, p_count-1], 'w')
            else:
                xdata = range(len(PREC_50[:,p_count]))
                yerr_low = PREC_50[:, p_count] -PREC_25[:, p_count]
                yerr_up = PREC_75[:, p_count] - PREC_50[:, p_count]
                axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(PREC_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After SIMULATION [DAS]', ylabel='Cum. Rainfall[mm]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(12, 8)
    fig_nameCR = Wdir_path + "\\"+ sname +"_output\\" + sname +"_CumRain.png"
    #plt.show()
    plt.savefig(fig_nameCR,dpi=100)    
    
    modeDisp = 'ValuesSeasonal'
    
    ### Store in df now:
    if scenario == 1:
        dfSavingDisp[modeDisp].iloc[6]   = fig_nameCR
    elif scenario == 2:     
        dfSavingDisp[modeDisp].iloc[18]   = fig_nameCR
    elif scenario == 3:     
        dfSavingDisp[modeDisp].iloc[30]   = fig_nameCR  
    ############################    
        
def plot_cum_rain_hist(SNX_list, sim_years, obs_flag):
    """"""
    global sname, start_year, scenario, mode, modeDisp, dfSavingDisp
    
    fname1 = Wdir_path + "\\"+ sname+"_output_hist\\SoilWat.OUT"
    pdate_list = []
    for i in range(len(SNX_list)):
        pdate_list.append(SNX_list[i][-11:-8])

    n_days =  500 #180 + 30 # 230 approximate growing days + 30 days from IC to plt date
    PREC_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
    if obs_flag == 1:
        PREC_obs= np.empty((n_days,len(pdate_list),))*np.NAN 

    read_flag = 0  #off
    n_count = 0  #count of n weahter realizations
    d_count = 0  #day counter during a growing season
    p_count = 0  #count for different planting date scenario
    obs_read_flag = 0
    with open(fname1) as fp:
        for line in fp:
            if line[12:15] == '  0':   #line[12:15] => DAS
                read_flag = 1  #on
                if line[1:5] == start_year:  #if this is the observed year
                    obs_read_flag = 1  #on
            if read_flag ==1 and line == '\n':  #after reading last growing day
                read_flag = 0 #off
                obs_read_flag = 0
                d_count = 0
                n_count = n_count + 1
                if n_count == sim_years:  #move to next planting date scenario
                    p_count = p_count + 1
                    n_count = 0
            if read_flag == 1:
                PREC_3D[d_count, n_count, p_count] = float(line[44:48])
                if obs_flag == 1 and obs_read_flag == 1:
                    PREC_obs[d_count, p_count] = float(line[44:48])
                d_count = d_count + 1

    # PREC_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
    PREC_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
    PREC_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
    PREC_75 = np.empty((n_days,len(pdate_list),))*np.NAN 

    #plot
    scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
    for count in range(len(pdate_list)):
        if count > 0:
            if int(pdate_list[count]) < int(pdate_list[count-1]):
                start_year = repr(int(start_year) + 1)
        temp_str = str(start_year) + " " + pdate_list[count]
        temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
        scename.append(temp_name)

    for i in range(len(pdate_list)):
        PREC_50[:, i] = np.nanpercentile(PREC_3D[:, 0:sim_years, i], 50, axis = 1)
        PREC_25[:, i] = np.nanpercentile(PREC_3D[:, 0:sim_years, i], 25, axis = 1)
        PREC_75[:, i] = np.nanpercentile(PREC_3D[:, 0:sim_years, i], 75, axis = 1)

    #================================== 
    #1) Plot PREC
    nrow = math.ceil(len(pdate_list)/2.0)
    ncol = 2
    fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
    fig.suptitle('Cumulative Precipitation')
    p_count = 0
    for i in range(ncol):
        for j in range(nrow):
            if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                xdata = range(len(PREC_50[:,0]))
                axs[j, i].plot(xdata, PREC_50[:, p_count-1], 'w')
            else:
                xdata = range(len(PREC_50[:,p_count]))
                yerr_low = PREC_50[:, p_count] -PREC_25[:, p_count]
                yerr_up = PREC_75[:, p_count] - PREC_50[:, p_count]
                axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_low, uplims=True)
                axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_up, lolims=True)
                if obs_flag == 1:
                    axs[j, i].plot(PREC_obs[:, p_count],'x-.', label='w/ obs. weather')
                axs[j, i].set_title(scename[p_count])
            p_count = p_count + 1
    for ax in axs.flat:
        ax.set(xlabel='Days After SIMULATION [DAS]', ylabel='Cum. Rainfall[mm]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()
    fig.set_size_inches(12, 8)
    fig_nameCR = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_CumRain.png"
    #plt.show()
    plt.savefig(fig_nameCR,dpi=100)    
      
    
    modeDisp = 'ValuesHistorical'
    
    ### Store in df now:
    if scenario == 1:
        dfSavingDisp[modeDisp].iloc[6]   = fig_nameCR
    elif scenario == 2:     
        dfSavingDisp[modeDisp].iloc[18]   = fig_nameCR
    elif scenario == 3:     
        dfSavingDisp[modeDisp].iloc[30]   = fig_nameCR   
    #######################################
    
def get_soil_IC( ID_SOIL):
    # SOL_file=self.Wdir_path.replace("/","\\") + "\\PH.SOL"
    SOL_file=Wdir_path + "\\SOIL.SOL" 
    #initialize
    depth_layer=[]
    ll_layer=[]
    ul_layer=[]
    n_layer=0
    soil_flag=0
    count=0
    fname = open(SOL_file,"r") #opens *.SOL
    for line in fname:
        if ID_SOIL in line:
            #soil_depth=line[33:36]
            # soil_depth=line[34:38]  #PH.SOL -> original
            soil_depth=line[33:36]  #SOIL.SOL
            soil_depth.replace(' ', '')
            sltx=line[25:29]  #PH.SOL
            soil_flag=1
        if soil_flag == 1:
            count=count+1
            if count >= 7:
                depth_layer.append(int(line[0:6]))
                ll_layer.append(float(line[13:18]))
                ul_layer.append(float(line[19:24]))
                n_layer=n_layer+1
                if int(line[3:6]) == int(soil_depth):
                    yield depth_layer  #list 
                    yield ll_layer
                    yield ul_layer
                    yield n_layer
                    yield sltx
                    fname.close()
                    break
                
def find_obs_date(WTD_fname):
    """check last observed data from *.WTD"""
    data1 = np.loadtxt(WTD_fname,skiprows=1)
    #convert numpy array to dataframe
    WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
                               'DOY':data1[:,0].astype(int)%1000,
            'SRAD':data1[:,1],
            'TMAX':data1[:,2],
            'TMIN':data1[:,3],
            'RAIN':data1[:,4]})
    WTD_last_date = int(repr(WTD_df.YEAR.values[-1]) + repr(WTD_df.DOY.values[-1]).zfill(3))
    del WTD_df
    return WTD_last_date

def find_station_info(station_name):
    if station_name == 'ALLE':
        LAT = 42.567    
        LONG = -85.633
        ELEV = 232
        TAV = 8.7
        AMP = 26.0
    elif station_name == 'BRAN':
        LAT = 41.967    
        LONG = -85.083
        ELEV = 248
        TAV = 9.3
        AMP = 27.0
    elif station_name == 'CASS':
        LAT = 41.867    
        LONG = -85.883
        ELEV = 250
        TAV = 9.4
        AMP = 26.9
    elif station_name == 'HURO':
        LAT = 42.150    
        LONG = -83.567
        ELEV = 247
        TAV = 9.0 
        AMP = 27.3
    elif station_name == 'WASH':
        LAT = 42.150    
        LONG = -83.567
        ELEV = 247
        TAV = 9.0
        AMP = 27.3      
    elif station_name == 'INGH':
        LAT = 42.700   
        LONG = -84.467
        ELEV = 259
        TAV = 8.5
        AMP = 27.2
    elif station_name == 'MONT':
        LAT = 43.217    
        LONG = -85.200
        ELEV = 257
        TAV = 8.0
        AMP = 26.9      
    elif station_name == 'SAGI':
        LAT = 43.133   
        LONG = -83.967
        ELEV = 220
        TAV = 8.2
        AMP = 26.6
    else:
        LAT = -99    
        LONG = -99
        ELEV = -99
        TAV = -99
        AMP = -99
    return LAT, LONG, ELEV, TAV, AMP 


def find_obs_years(  WTD_fname, IC_date, hv_date): #note: IC_date and hv_date is integer in [YYYYDOY] 
    """check last observed data from *.WTD"""
    data1 = np.loadtxt(WTD_fname,skiprows=1)
    WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
                               'DOY':data1[:,0].astype(int)%1000,
            'SRAD':data1[:,1],
            'TMAX':data1[:,2],
            'TMIN':data1[:,3],
            'RAIN':data1[:,4]})
    year1 = WTD_df.YEAR[(WTD_df["DOY"] == IC_date%1000)].values[0]
    year2 = WTD_df.YEAR[(WTD_df["DOY"] == hv_date%1000)].values[-1]
    if IC_date%1000 < hv_date%1000:  #when IC_date and hv_date are in a same year
        sim_years = year2 - year1 + 1
    else:
        sim_years = year2 - year1  #when a growing period go beyond next year
    del WTD_df
    return sim_years, year1


def find_plt_date(  start_year, lst, lst2, lst_frst, lst_frst2):
    #=============================================
    m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
    m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365] #ending date of each month for regular years
    m_doys_list2 = [1,32,61,92,122,153,183,214,245,275,306,336]  #starting date of each month for leap years
    m_doye_list2 = [31,60,91,121,152,182,213,244,274,305,335,366] #ending date of each month for leap years
    numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
    #======================================================================
    #==> (1)find the first planting month
    #check year 1 first
    p_index=[i for i in range(len(lst)) if lst[i]== 1]  #range (python 3):   #xrange (python 2):
    s_index=[i for i in range(len(lst_frst)) if lst_frst[i]== 1] 
    p_index2=[i for i in range(len(lst2)) if lst2[i]== 1]
    s_index2=[i for i in range(len(lst_frst2)) if lst_frst2[i]== 1] 
    # plt1_month = p_index[0] + 1
    if len(p_index) > 0:
        pdate_1 = int(start_year)*1000 + m_doys_list[p_index[0]]
        if calendar.isleap(int(start_year)):  pdate_1 = int(start_year)*1000 + m_doys_list2[p_index[0]]
        if len(s_index) > 0:
            frst_date1 = int(start_year)*1000 + m_doys_list[s_index[0]]
            if calendar.isleap(int(start_year)):  frst_date1 = int(start_year)*1000 + m_doys_list2[s_index[0]]
        else: #check year 2
            s_index2=[i for i in range(len(lst_frst2)) if lst_frst2[i]== 1]  #range (python 3):
            frst_date1 = (int(start_year)+1)*1000 + m_doys_list[s_index2[0]]
            if calendar.isleap(int(start_year)+1):  frst_date1 = (int(start_year)+1)*1000 + m_doys_list2[s_index2[0]]
    else: # len(p_index) == 0:  #If cannot find planting month in the year 1
        #plt1_month = p_index2[0] + 1
        pdate_1 = int(start_year)*1000 + m_doys_list[p_index2[0]]
        if calendar.isleap(int(start_year)):  pdate_1 = int(start_year)*1000 + m_doys_list2[p_index2[0]]
        if len(s_index) > 0:  #check year 1 first  => case 2
            frst_date1 = (int(start_year)-1)*1000 + m_doys_list[s_index[0]]
            if calendar.isleap(int(start_year)-1):  frst_date1 = (int(start_year)-1)*1000 + m_doys_list2[s_index[0]]
        else: #check year 2
            frst_date1 = int(start_year)*1000 + m_doys_list[s_index2[0]]
            if calendar.isleap(int(start_year)):  frst_date1 = int(start_year)*1000 + m_doys_list2[s_index2[0]]

    #==> (2)find the last planting month 
    p2_index=[i for i in range(len(lst2)) if lst2[i]== 1]   #check year 2 first
    # plt2_month = p2_index[-1]  #last element
    if len(p2_index) == 0:  #If cannot find planting month in the year 2, search year 1
        p2_index2=[i for i in range(len(lst)) if lst[i]== 1]
        #plt2_month = p2_index2[-1]
        pdate_2 = int(start_year)*1000 + m_doye_list[p2_index2[-1]]
        if calendar.isleap(int(start_year)):  pdate_2 = int(start_year)*1000 + m_doye_list2[p2_index2[-1]]
        end_date = 365  #int(start_year)*1000 + 365
        if calendar.isleap(int(start_year)):  end_date = 366 #int(start_year)*1000 + 366
        if (pdate_2%1000 + 180) > end_date:   #180 is arbitrary number to fully cover weather data until late maturity
            hv_date = (pdate_2//1000 + 1)*1000 + (pdate_2%1000 + 180) - end_date  
        else:
            hv_date = pdate_2 + 180
    else: #If can find planting month in the year 2
        if len(p_index) == 0:  #If cannot find planting month in the year 1, the first planting date is in year 2
            #case 2
            pdate_2 = int(start_year)*1000 + m_doye_list[p2_index[-1]]
            if calendar.isleap(int(start_year)):  pdate_2 = int(start_year)*1000 + m_doye_list2[p2_index[-1]]
            hv_date = pdate_2 + 180
        else: #case 3 => plating window covers both year 1 and year 2
            pdate_2 = (int(start_year) + 1)*1000 + m_doye_list[p2_index[-1]]
            if calendar.isleap(int(start_year) + 1):  pdate_2 = (int(start_year) + 1)*1000 + m_doye_list2[p2_index[-1]]
            hv_date = pdate_2 + 180
    #make a list of planting window months
    pwindow_m= []
    if len(p_index) > 0:
        for i in range(len(p_index)):
            pwindow_m.append(p_index[i] + 1)
        if len(p_index2) > 0:
            for i in range(len(p_index2)):
                pwindow_m.append(p_index2[i] + 1)
    else:
        if len(p_index2) > 0:
            for i in range(len(p_index2)):
                pwindow_m.append(p_index2[i] + 1)
        else:
            print ("error! - no planting window is found!")
            os.system('pause')
    #make a list of SCF window months
    fwindow_m= []
    if len(s_index) > 0:
        for i in range(len(s_index)):
            fwindow_m.append(s_index[i] + 1)
        if len(s_index2) > 0:
            for i in range(len(s_index2)):
                fwindow_m.append(s_index2[i] + 1)
    else:
        if len(s_index2) > 0:
            for i in range(len(s_index2)):
                fwindow_m.append(s_index2[i]+ 1)
        else:
            print ("error! - no SCF window is found!")
            os.system('pause')
    return pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m


def Yield_boxplot_compare(  SNX_list, sim_years, obs_flag):

    global sname, start_year, scenario, mode, modeDisp, dfSavingDisp    

    fname1 = Wdir_path + "\\"+ sname+"_output\\SUMMARY.OUT"
    if obs_flag == 1:
        nrealiz = 101
    else:
        nrealiz = 100
    pdate_list = []
    year_list = []
    for i in range(len(SNX_list)):
        pdate_list.append(int(SNX_list[i][-11:-8]))
        if i == 0:
            year_list.append(int(start_year))
        else:
            if pdate_list[i] > pdate_list[i-1]:
                year_list.append(int(start_year))
            else:
                year_list.append(int(start_year) + 1)
    #check if output  SUMMARY.OUt exists
    if not os.path.isfile(fname1):
        print( '**Error!!- SUMMARY.OUT file does not exist under ####_output folder')
        os.system('pause')

    df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
    PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
    HWAM = df_OUT.ix[:,20].values  #read 21th column only

    DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
    for i in range(len(HWAM)):
        # year_array[i] = int(repr(PDAT[i])[:4])
        DOY_array[i] = int(repr(PDAT[i])[4:])

    #make three arrays into a dataframe
    df = pd.DataFrame({'DOY': DOY_array, 'HWAM':HWAM}, columns=['DOY','HWAM'])
    #df = pd.DataFrame({'DOY': DOY_array, 'ADAT':ADAT_array, 'MDAT':MDAT_array, 'HWAM':HWAM}, columns=['DOY','ADAT','MDAT','HWAM'])

    # #make an empty array [nyears * n_plt_dates]
    n_year = 100
    # For Boxplot
    yield_n = np.empty([n_year,len(pdate_list)])*np.nan

    if obs_flag == 0:  #if there is no observed weather available 
        for count in range(len(pdate_list)):
            Pdate = pdate_list[count]
            yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values           
    else:   
        obs_yield = []
        # obs_ADAT = []
        # obs_MDAT = []
        for count in range(len(pdate_list)):
            Pdate = pdate_list[count]
            yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values[:-1]  
            obs_yield.append(df.HWAM[(df["DOY"] == Pdate)].values[-1])      

    #To remove 'nan' from data array
    #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
    mask = ~np.isnan(yield_n)
    filtered_yield = [d[m] for d, m in zip(yield_n.T, mask.T)]
    ##====================================================
    ##READ Yields from historical weather observations
    fname1 = Wdir_path + "\\"+ sname+"_output_hist\\SUMMARY.OUT"

    #check if output  SUMMARY.OUt exists
    if not os.path.isfile(fname1):
        print( '**Error!!- SUMMARY.OUT file does not exist under ####_output_hist folder')
        os.system('pause')

    df_OUT2=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
    PDAT2 = df_OUT2.ix[:,14].values  #read 14th column only => Planting date 
    HWAM2 = df_OUT2.ix[:,21].values  #read 21th column only

    year_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize
    DOY_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize

    for i in range(len(HWAM2)):
        year_array2[i] = int(repr(PDAT2[i])[:4])
        DOY_array2[i] = int(repr(PDAT2[i])[4:])

    #make three arrays into a dataframe
    df2 = pd.DataFrame({'Year': year_array2,'DOY': DOY_array2, 'HWAM':HWAM2}, columns=['Year','DOY','HWAM'])
    #df2 = pd.DataFrame({'Year': year_array2,'DOY': DOY_array2, 'ADAT':ADAT2_array2, 'MDAT':MDAT2_array2, 'HWAM':HWAM2}, columns=['Year','DOY','ADAT','MDAT','HWAM'])

    # #make an empty array [nyears * n_plt_dates]
    n_year = sim_years
    # For Boxplot
    yield2_n = np.empty([n_year,len(pdate_list)])*np.nan

    for count in range(len(pdate_list)):
        Pdate = pdate_list[count]
        yield2_n[:,count] = df2.HWAM[(df2["DOY"] == Pdate)].values  

    if obs_flag == 1:
        obs_yield = []
        for count in range(len(pdate_list)):
            Pdate = pdate_list[count]
            if count > 0 and pdate_list[count] < pdate_list[count-1]:  #when two consecutive years
                start_year = repr(int(start_year)+1)
            #collect simulated results from observed weather
            obs_yield.append(df2.HWAM[(df2["DOY"] == Pdate) & (df2["Year"] == int(start_year))].values[0])      
    #To remove 'nan' from data array
    #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
    mask = ~np.isnan(yield2_n)
    filtered_yield2 = [d[m] for d, m in zip(yield2_n.T, mask.T)]
    ##End of reading Yields from historical weather observations
    #plot============================================
    #regroup matrix for different planting date
    i_pos = 1
    fig = plt.figure()
    fig.suptitle('Estimated Yields with differnt planting dates', fontsize=14, fontweight='bold')
    ax = plt.axes()
    #plt.hold(True)

    for count in range(len(pdate_list)):
        a = filtered_yield2[count].tolist() #.reshape((1,obs_years,1))
        b = filtered_yield[count].tolist() #.reshape((nrealiz,1))
        new_list = []
        new_list.append(a)
        new_list.append(b) #=> nested list
        # first boxplot pair
        bp = plt.boxplot(new_list, positions = [i_pos, i_pos+1], widths = 0.6)
        setBoxColors(bp)
        i_pos = i_pos + 3

    #X data for plot
    #myXList=[i+3 for i in range(len(pdate_list))]
    myXList=[i+0.5 for i in range(1, 3*len(pdate_list), 3)]
    if obs_flag == 1:
        # Plot a line between yields with observed weather
        plt.plot(myXList, obs_yield, 'go-')
    scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
    for count in range(len(pdate_list)):
        temp_str = repr(year_list[count]) + " " + repr(pdate_list[count]) 
        temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
        scename.append(temp_name)

    # set axes limits and labels
    plt.xlim(0,3*len(pdate_list))
    # ax.set_xticks([1.5, 4.5, 7.5, 10.5, 13.5, 16.5])
    ax.set_xticklabels(scename)
    ax.set_xticks(myXList)
    ax.set_xlabel('Planting Date',fontsize=14)
    ax.set_ylabel('Yield [kg/ha]',fontsize=14)
    # draw temporary red and blue lines and use them to create a legend
    hB, = plt.plot([1,1],'b-')
    hR, = plt.plot([1,1],'r-')
    plt.legend((hB, hR),('observed WTH', 'SCF'), loc='best')
    hB.set_visible(False)
    hR.set_visible(False)
    fig.set_size_inches(12, 8)
    fig_name = Wdir_path + "\\"+sname+"_output\\"+sname+"_Yield_boxplot2.png"
    plt.savefig(fig_name,dpi=100)
    modeDisp = 'ValueComparison'
    
    if scenario == 1:
        idx = 0
    elif scenario == 2:    
        idx = 1
    elif scenario == 3:    
        idx = 3
    dfSavingDisp[modeDisp].iloc[idx]   = fig_name
     
def Yield_tercile_forecast( SNX_list, sim_years, obs_flag):
        

    
        fname1 = Wdir_path + "\\"+ sname+"_output\\SUMMARY.OUT"
        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        pdate_list = []
        year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(int(SNX_list[i][-11:-8]))
            if i == 0:
                year_list.append(int(start_year))
            else:
                if pdate_list[i] > pdate_list[i-1]:
                    year_list.append(int(start_year))
                else:
                    year_list.append(int(start_year) + 1)
        #check if output  SUMMARY.OUt exists
        if not os.path.isfile(fname1):
            print( '**Error!!- SUMMARY.OUT file does not exist under ####_output folder')
            os.system('pause')

        df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
        HWAM = df_OUT.ix[:,20].values  #read 21th column only

        DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize

        for i in range(len(HWAM)):
            # year_array[i] = int(repr(PDAT[i])[:4])
            DOY_array[i] = int(repr(PDAT[i])[4:])
                
        #make three arrays into a dataframe
        df = pd.DataFrame({'DOY': DOY_array, 'HWAM':HWAM}, columns=['DOY','HWAM'])
        ##====================================================
        ##READ Yields from historical weather observations

        fname1 = Wdir_path + "\\"+ sname+"_output_hist\\SUMMARY.OUT"

        #check if output  SUMMARY.OUt exists
        if not os.path.isfile(fname1):
            print( '**Error!!- SUMMARY.OUT file does not exist under ####_output_hist folder')
            os.system('pause')

        df_OUT2=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT2 = df_OUT2.ix[:,14].values  #read 14th column only => Planting date 
        HWAM2 = df_OUT2.ix[:,21].values  #read 21th column only
        year_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize
        DOY_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize

        BN_list = []
        NN_list = []
        AN_list = []
        for i in range(len(HWAM2)):
            year_array2[i] = int(repr(PDAT2[i])[:4])
            DOY_array2[i] = int(repr(PDAT2[i])[4:])
        #make three arrays into a dataframe
        df2 = pd.DataFrame({'Year': year_array2,'DOY': DOY_array2, 'HWAM':HWAM2}, columns=['Year','DOY','HWAM'])
        #===========================
        # extract target values corresponding to 33% and 67%
        for Pdate in pdate_list:
            yield_frst= df.HWAM[(df["DOY"] == Pdate)].values 
            yield_hist= df2.HWAM[(df2["DOY"] == Pdate)].values

            #a) compute CDF curve from the simulated yields-frst
            sorted_yield_frst = np.sort(yield_frst) #sort monthly rain from smallest to largest
            index_yield_frst = np.argsort(yield_frst) #** argsort - returns the original indexes of the sorted array
            temp = np.zeros(len(yield_frst))+1
            pdf = np.divide(temp,len(yield_frst))  #1/100years 
            cdf = np.cumsum(pdf)  #compute CDF

            #b) compute CDF curve from the simulated yields-historical
            sorted_yield_hist = np.sort(yield_hist) #sort monthly rain from smallest to largest
            index_yield_hist = np.argsort(yield_hist) #** argsort - returns the original indexes of the sorted array
            temp = np.zeros(len(yield_hist))+1
            pdf2 = np.divide(temp,len(yield_hist))  #1/30 years 
            cdf2 = np.cumsum(pdf2)  #compute CDF

            #C) compute threshold yield vaues (33% & 67%) from the yield-historical distribution
            #numpy.interp(x, xp, fp, left=None, right=None, period=None)
            yield_thres = np.interp([0.33,0.67],cdf2,sorted_yield_hist,left=sorted_yield_hist[0])# 100 WGEN outputs to theoretical CDF SCF curve

            #d) compute threshold yield vaues (33% & 67%) from the yield-historical distribution
            tercile2 = np.interp(yield_thres, sorted_yield_frst, cdf, left=0)      

            #e) add to the list
            BN_list.append(tercile2[0]*100)  #0.1167
            AN_list.append(100 - tercile2[1]*100)  # 1- 0.4297
            NN_list.append((tercile2[1]- tercile2[0])*100)  #NN= 100-BN - AN, e.g.,( 0.4297-0.1167) *100
            
        #f) Write the extract BN, NN and AN into a csv file
        temp_csv=Wdir_path.replace("/","\\") + "\\"+ sname+"_output\\yield_tercile_forecast.csv" 
        with open(temp_csv, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Pdate_DOY','BN','NN', 'AN'])
            for i in range(len(pdate_list)):
                scfwriter.writerow([repr(pdate_list[i])] + [repr(BN_list[i])]+ [repr(NN_list[i])] + [repr(AN_list[i])])



def setBoxColors(bp):
    """function for setting the colors of the box plots pairs"""
    plt.setp(bp['boxes'][0], color='blue')
    plt.setp(bp['caps'][0], color='blue')
    plt.setp(bp['caps'][1], color='blue')
    plt.setp(bp['whiskers'][0], color='blue')
    plt.setp(bp['whiskers'][1], color='blue')
    plt.setp(bp['fliers'][0], color='blue')
    plt.setp(bp['fliers'][1], color='blue')
    plt.setp(bp['medians'][0], color='blue')

    plt.setp(bp['boxes'][1], color='red')
    plt.setp(bp['caps'][2], color='red')
    plt.setp(bp['caps'][3], color='red')
    plt.setp(bp['whiskers'][2], color='red')
    plt.setp(bp['whiskers'][3], color='red')
    plt.setp(bp['fliers'][1], color='red')
    plt.setp(bp['fliers'][1], color='red')
    plt.setp(bp['medians'][1], color='red')



