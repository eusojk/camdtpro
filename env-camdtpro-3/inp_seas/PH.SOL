*SOILS: General DSSAT Soil Input File
! DSSAT v4.7; 09/01/2017
!
! Standard Soil Profiles
!
! The following generic information was developed by A.J. Gijsman:
!
! - BD was estimated as BD = 100 / (SOM% / 0.224 + (100 - SOM%) / mineral BD)  
!   (Adams, 1973; Rawls and Brakensiek, 1985).
! - LL and DUL are according to Saxton et al., 1986.
! - SAT was taken as a fraction of porosity (Dalgliesh and Foale, 1998):
!   0.93 for soil classes S, SL and LS; 0.95 for soil classes L, SIL, SI,
!   SCL and SC; and 0.97 for soil classes C, CL, SIC and SICL.
!   For this, porosity was estimated as: POR = 1 - BD / APD (in which APD
!   is the adjusted particle density, i.e. corrected for SOM; Baumer and Rice, 1988).
! - The ranges of LL and DUL values were calculated by stepping through the
!   complete texture triangle in steps of 1% sand, 1% silt and 1% clay (>5000 
!   combinations), but with the texture limitations that Saxton set for his method
!   taken into consideration. For SAT, these limitations do not hold, as this was
!   based on POR and not on Saxton. See Gijsman et al., 2002.
! - The root growth distribution function SRGF was was calculated as:
!   SRGF = 1 * EXP(-0.02 * LAYER_CENTER); SRGF was set 1 for LAYER_BOTTOM <= 15.
!
! SOIL CLASS       BD                LL               DUL               SAT
! ========== =============     =============     =============     =============
!   C        1.129 - 1.512     0.220 - 0.346     0.330 - 0.467     0.413 - 0.488
!   CL       1.243 - 1.502     0.156 - 0.218     0.282 - 0.374     0.417 - 0.512
!   L        1.245 - 1.483     0.083 - 0.156     0.222 - 0.312     0.415 - 0.501
!   LS       1.353 - 1.629     0.059 - 0.110     0.137 - 0.185     0.355 - 0.416
!   S        1.446 - 1.574     0.055 - 0.085     0.123 - 0.158     0.374 - 0.400
!   SC       1.501 - 1.593     0.195 - 0.294     0.276 - 0.389     0.376 - 0.409
!   SCL      1.475 - 1.636     0.132 - 0.191     0.213 - 0.304     0.360 - 0.418
!   SI       0.978 - 1.464     0.096 - 0.099     0.299 - 0.307     0.442 - 0.488
!   SIC      1.307 - 1.446     0.224 - 0.326     0.379 - 0.456     0.455 - 0.489
!   SICL     1.248 - 1.464     0.155 - 0.219     0.324 - 0.392     0.448 - 0.511
!   SIL      0.968 - 1.464     0.082 - 0.152     0.240 - 0.333     0.439 - 0.547
!   SL       1.142 - 1.647     0.066 - 0.133     0.164 - 0.243     0.348 - 0.499
!
!======================================================================================================
! Start of Generic soil profiles
!======================================================================================================
!
! The 12 Generic soils for SOIL.SOL, as estimated by Arjan Gijsman:
!
! - LL, DUL are according to the Nearest Neighbor method (Jagtap et al, 2004)
! - Ksat at -99 
! - BD according to Gijsman et al (2002)
! - SAT based on the APSRU manual (Dalgliesh and Foale, 1998); i.e. 93-97% of porosity
!   depending on the soil type) in which porosity is according to Baumer and Rice (1988).
!
! References
! Adams W.A. 1973. The effect of organic matter on the bulk and true densities of some
!   uncultivated podzolic soils. J. Soil Science 24, 10-17.
! Baumer O.W. and Rice J.W. 1988. Methods to predict soil input data for DRAINMOD. 
!   Am. Soc. Agr. Eng. Paper 88-2564
! Dalgliesh, N.P., and M.A. Foale. 1998. Soil Matters � monitoring soil water and nitrogen
!   in dryland farming. CSIRO, Agricultural Production Systems Research Unit, 
!   Toowoomba, Queensland, Australia. 122 pp.
! Gijsman A.J., Jagtap S.S., Jones J.W. 2002. Wading through a swamp of complete confusion: 
!   how to choose a method for estimating soil water retention parameters for crop models. 
!   European Journal of Agronomy, 18: 75-105.
! Jagtap S.S., Lal U., Jones J.W., Gijsman A.J., Ritchie J.T. 2004. A dynamic nearest-neighbor
!   method for estimating soil water parameters. Transactions of ASAE 47: 1437-1444
! Rawls W.J. and Brakensiek D.L. 1985. Prediction of soil water properties for hydrologic
!   modeling. In: Jones, E.B. and Ward, T.J. (Eds.), Proc. Symp. Watershed Management
!   in the Eighties. April 30-May 1, 1985, Denver, CO. Am. Soc. Civil Eng., 
!   New York, NY. pp.293-299.
! Saxton K.E., Rawls W.J., Romberger J.S., Papendick R.I. 1986. Estimating generalized soil-water
!   characteristics from texture. Soil Sci. Soc. Am. J. 50, 1031-1036
!
!======================================================================================================

*PH00000001  BSWM        SICL     150  QUINGUA DEEP
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 ISABELA     PHILIPPINE     17.126 121.888 SIL
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6    .6    73     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    20    Ap   .14  .344  .424     1   .15  1.28  1.82  27.6    61  11.4   -99     5   -99  27.2   -99
    48   -99  .136  .381  .422  .507   .15  1.35  1.35  27.6    65   7.4   -99   5.6   -99  29.4   -99
    84   -99  .134  .346  .455  .267   .15  1.24  1.33  27.6    64   8.4   -99   6.3   -99  31.6   -99
   107   -99  .154   .37  .449  .148   .15  1.28   2.3  31.6    63   5.4   -99   6.5   -99  33.6   -99
   137   -99  .202  .388  .442  .087   .15  1.28  1.88  35.6    57   7.4   -99   6.5   -99  37.7   -99
   150   -99  .213  .421  .463  .057   .09  1.21  2.36  41.6    51   7.4   -99   6.5   -99    37   -99
@  SLB  SLPX  SLPT  SLPO CACO3  SLAL  SLFE  SLMN  SLBS  SLPA  SLPB  SLKE  SLMG  SLNA  SLSU  SLEC  SLCA
    20   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .6   -99   -99   -99   -99  12.1
    48   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  12.7
    84   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  15.8
   107   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  16.1
   137   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  19.2
   150   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99    20
   
*PHS0000001  BSWM        SICL     150  QUINGUA  SHALLOW
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 ISABELA     PHILIPPINE     17.126 121.888 SIL
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6    .6    73     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    20    Ap   .14  .344  .424     1   .15  1.28  1.82  27.6    61  11.4   -99     5   -99  27.2   -99
    48   -99  .136  .381  .422  .507   .15  1.35  1.35  27.6    65   7.4   -99   5.6   -99  29.4   -99
    84   -99  .134  .346  .455    .4   .15  1.24  1.33  27.6    64   8.4   -99   6.3   -99  31.6   -99
   107   -99  .154   .37  .449    .0   .15  1.28   2.3  31.6    63   5.4   -99   6.5   -99  33.6   -99
   137   -99  .202  .388  .442    .0   .15  1.28  1.88  35.6    57   7.4   -99   6.5   -99  37.7   -99
   150   -99  .213  .421  .463    .0   .09  1.21  2.36  41.6    51   7.4   -99   6.5   -99    37   -99
@  SLB  SLPX  SLPT  SLPO CACO3  SLAL  SLFE  SLMN  SLBS  SLPA  SLPB  SLKE  SLMG  SLNA  SLSU  SLEC  SLCA
    20   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .6   -99   -99   -99   -99  12.1
    48   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  12.7
    84   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  15.8
   107   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  16.1
   137   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  19.2
   150   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99    20

*PH00000002  BSWM        C        300  CANDABA DEEP
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 PAMPANGA    PHILIPPINE     15.088 120.819 C
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
     G   .13     6   .01    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    29   Apg .2789  .478  .502     1   .06  1.27   .82  77.6  20.2   2.2   -99   6.3   -99   -99   -99
    55   A1g .2739  .469  .483  .432   .06  1.27   .41  81.6  16.6   1.8   -99   6.7   -99   -99   -99
    75   A3g .2978  .512  .594  .273   .06  1.17    .5  89.6   7.4     3   -99   6.9   -99   -99   -99
   140   B2g .2434  .461  .503  .116   .06  1.33   .26  77.6  20.2   2.2   -99   7.5   -99   -99   -99
   220   C1g  .447  .492  .539  .027   .06  1.17   .26  89.6   3.4     7   -99   7.3   -99   -99   -99
   300   C2g  .399  .444  .493  .006   .06   1.3   .04  81.6  10.2   8.2   -99     7   -99   -99   -99
@  SLB  SLPX  SLPT  SLPO CACO3  SLAL  SLFE  SLMN  SLBS  SLPA  SLPB  SLKE  SLMG  SLNA  SLSU  SLEC  SLCA
    29   -99   -99   -99   2.2   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99    24
    55   -99   -99   -99   2.2   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99  30.5
    75   -99   -99   -99   3.3   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99  27.2
   140   -99   -99   -99   1.3   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99    25
   220   -99   -99   -99   2.3   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99  42.8
   300   -99   -99   -99   2.3   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99  27.9

*PHS0000002  BSWM        C        300  CANDABA SHALLOW
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 PAMPANGA    PHILIPPINE     15.088 120.819 C
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
     G   .13     6   .01    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    29   Apg .2789  .478  .502     1   .06  1.27   .82  77.6  20.2   2.2   -99   6.3   -99   -99   -99	
    55   A1g .2739  .469  .483  .432   .06  1.27   .41  81.6  16.6   1.8   -99   6.7   -99   -99   -99	
    75   A3g .2978  .512  .594    .0   .06  1.17    .5  89.6   7.4     3   -99   6.9   -99   -99   -99	
   140   B2g .2434  .461  .503    .0   .06  1.33   .26  77.6  20.2   2.2   -99   7.5   -99   -99   -99	
   220   C1g  .447  .492  .539    .0   .06  1.17   .26  89.6   3.4     7   -99   7.3   -99   -99   -99	
   300   C2g  .399  .444  .493    .0   .06   1.3   .04  81.6  10.2   8.2   -99     7   -99   -99   -99	
@  SLB  SLPX  SLPT  SLPO CACO3  SLAL  SLFE  SLMN  SLBS  SLPA  SLPB  SLKE  SLMG  SLNA  SLSU  SLEC  SLCA
    29   -99   -99   -99   2.2   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99    24
    55   -99   -99   -99   2.2   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99  30.5
    75   -99   -99   -99   3.3   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99  27.2
   140   -99   -99   -99   1.3   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99    25
   220   -99   -99   -99   2.3   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99  42.8
   300   -99   -99   -99   2.3   -99   -99   -99   -99   -99   -99    .1   -99   -99   -99   -99  27.9
   
*PH00000003  BSWM        SICL     150  MAAPAG  DEEP
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 MALAYBALAY  PHILIPPINE      7.989 125.146 SiC
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6   .25    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    17   -99   .25  .434  .463     1   .15  1.06  3.04  37.8    46  16.2   -99     5   -99  36.1   -99
    39   -99  .241  .376  .412  .571   .06   1.2  1.57  45.8    35  19.2   -99   5.7   -99    26   -99
    65   -99  .251  .357  .382  .353   .06  1.28   .85  53.8    25  21.2   -99   6.4   -99  26.3   -99
    88   -99   .28  .394  .404  .217   .06  1.27   .88  57.8    25  17.3   -99   6.4   -99  18.2   -99
   115   -99  .359   .49  .444  .131   .06   1.3   .79  67.8    25   7.2   -99   6.4   -99    21   -99
   150   -99  .358   .47  .436  .071   .06  1.23     1  71.8    15  13.2   -99   6.2   -99  24.4   -99
@  SLB  SLPX  SLPT  SLPO CACO3  SLAL  SLFE  SLMN  SLBS  SLPA  SLPB  SLKE  SLMG  SLNA  SLSU  SLEC  SLCA
    17   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .9   -99   -99   -99   -99   6.8
    39   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99   4.5
    65   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .5   -99   -99   -99   -99   4.5
    88   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .5   -99   -99   -99   -99   4.9
   115   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .6   -99   -99   -99   -99   4.2
   150   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99   6.1

*PHS0000003  BSWM        SICL     150  MAAPAG SHALLOW 
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 MALAYBALAY  PHILIPPINE      7.989 125.146 SiC
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6   .25    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    17   -99   .25  .434  .463     1   .15  1.06  3.04  37.8    46  16.2   -99     5   -99  36.1   -99
    39   -99  .241  .376  .412  .571   .06   1.2  1.57  45.8    35  19.2   -99   5.7   -99    26   -99
    65   -99  .251  .357  .382  .353   .06  1.28   .85  53.8    25  21.2   -99   6.4   -99  26.3   -99
    88   -99   .28  .394  .404    .0   .06  1.27   .88  57.8    25  17.3   -99   6.4   -99  18.2   -99
   115   -99  .359   .49  .444    .0   .06   1.3   .79  67.8    25   7.2   -99   6.4   -99    21   -99
   150   -99  .358   .47  .436    .0   .06  1.23     1  71.8    15  13.2   -99   6.2   -99  24.4   -99
@  SLB  SLPX  SLPT  SLPO CACO3  SLAL  SLFE  SLMN  SLBS  SLPA  SLPB  SLKE  SLMG  SLNA  SLSU  SLEC  SLCA
    17   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .9   -99   -99   -99   -99   6.8
    39   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99   4.5
    65   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .5   -99   -99   -99   -99   4.5
    88   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .5   -99   -99   -99   -99   4.9
   115   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .6   -99   -99   -99   -99   4.2
   150   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99   6.
   
*PH00000004  BSWM        C        150  BABUYAN DEEP
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 NARRA       PHILIPPINE       9.91 118.142 C
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6    .6    84     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    20     A  .126  .356  .365     1   .06  1.26  1.35  48.8  25.8  25.4   -99   5.5   -99  37.4   -99
    40   Bt1  .185  .377  .403  .549   .06  1.27   .91  56.8  25.8  17.4   -99   6.2   -99  38.5   -99
    90   Bt2  .217  .394  .422  .273   .06  1.31   .52  62.8  19.8  17.4   -99   6.3   -99    44   -99
   120   Bt3  .221  .389  .446  .122   .06  1.33    .3  64.8  17.8  17.4   -99   6.8   -99  47.7   -99
   150   Bt4   .22  .389  .439  .067   .06  1.33   .27  64.8  17.8  17.4   -99   7.4   -99  53.4   -99
@  SLB  SLPX  SLPT  SLPO CACO3  SLAL  SLFE  SLMN  SLBS  SLPA  SLPB  SLKE  SLMG  SLNA  SLSU  SLEC  SLCA
    20   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .2   -99   -99   -99   -99  16.5
    40   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .2   -99   -99   -99   -99  22.1
    90   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  26.5
   120   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  31.7
   150   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  36.7

*PHS0000004  BSWM        C        150  BABUYAN SHALLOW
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 NARRA       PHILIPPINE       9.91 118.142 C
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6    .6    84     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    20     A  .126  .356  .365     1   .06  1.26  1.35  48.8  25.8  25.4   -99   5.5   -99  37.4   -99
    40   Bt1  .185  .377  .403  .549   .06  1.27   .91  56.8  25.8  17.4   -99   6.2   -99  38.5   -99
    90   Bt2  .217  .394  .422    .0   .06  1.31   .52  62.8  19.8  17.4   -99   6.3   -99    44   -99
   120   Bt3  .221  .389  .446    .0   .06  1.33    .3  64.8  17.8  17.4   -99   6.8   -99  47.7   -99
   150   Bt4   .22  .389  .439    .0   .06  1.33   .27  64.8  17.8  17.4   -99   7.4   -99  53.4   -99
@  SLB  SLPX  SLPT  SLPO CACO3  SLAL  SLFE  SLMN  SLBS  SLPA  SLPB  SLKE  SLMG  SLNA  SLSU  SLEC  SLCA
    20   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .2   -99   -99   -99   -99  16.5
    40   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .2   -99   -99   -99   -99  22.1
    90   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  26.5
   120   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  31.7
   150   -99   -99   -99   -99   -99   -99   -99   -99   -99   -99    .3   -99   -99   -99   -99  36.7
   
*PH00000005  UPLB        C        190  TORAN  DEEP
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 Aparri      PHILIPPINE     18.319  121.66 CL
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
     G   .13     6   .25    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC	
    20   Apg  .357  .479  .538     1   .06  1.26  1.58  58.9  39.4   1.7   -99   5.6   -99   -99   -99	
    40  Bgwg  .347  .471  .523  .549   .09  1.29  1.31  58.1  40.3   1.6   -99   5.8   -99   -99   -99	
    60  Bgw2  .343  .455  .503  .368   .06  1.34   .81  60.3  37.8   1.8   -99   6.4   -99   -99   -99	
    75  Bgw3   .31  .432  .441  .259   .06   1.3   .71  59.6  30.6   9.8   -99   6.7   -99   -99   -99	
   110  Bgw4   .36  .447   502  .157   .06  1.36   .49    66  31.4   2.6   -99   6.9   -99   -99   -99
   130    BC  .272  .391  .409  .091   .06  1.28   .77  55.3  28.8  15.8   -99   6.8   -99   -99   -99
   190     C   .23   .49  .509  .041   .68   .87  5.75  18.8  64.6  16.7   -99   3.5   -99   -99   -99

*PHS0000005  UPLB        C        190  TORAN SHALLOW
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 Aparri      PHILIPPINE     18.319  121.66 CL
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
     G   .13     6   .25    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    20   Apg  .357  .479  .538      1   .06  1.26  1.58  58.9  39.4   1.7   -99   5.6   -99   -99   -99
    40  Bgwg  .347  .471  .523   .549   .09  1.29  1.31  58.1  40.3   1.6   -99   5.8   -99   -99   -99
    60  Bgw2  .343  .455  .503   .368   .06  1.34   .81  60.3  37.8   1.8   -99   6.4   -99   -99   -99
    75  Bgw3   .31  .432  .441     .0   .06   1.3   .71  59.6  30.6   9.8   -99   6.7   -99   -99   -99
   110  Bgw4   .36  .447   502     .0   .06  1.36   .49    66  31.4   2.6   -99   6.9   -99   -99   -99
   130    BC  .272  .391  .409     .0   .06  1.28   .77  55.3  28.8  15.8   -99   6.8   -99   -99   -99
   190     C   .23   .49  .509     .0   .68   .87  5.75  18.8  64.6  16.7   -99   3.5   -99   -99   -99
   
*PH00000007  UPLB        SICL     105  SAN MANUEL  DEEP
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 CAGAYAN     PHILIPPINE        -99     -99 SiCL
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6    .4    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    10    Ap  .092  .173  .264     1  1.32  1.43  1.56  34.1  58.8   7.1   -99   6.9   -99   -99   -99
    45   AB1  .223  .376  .429  .577   .09  1.29   .78  40.7  48.3    11   -99   6.9   -99   -99   -99
    68   AB2  .228  .395  .429  .323   .09  1.36   .62  40.2  53.4   6.4   -99   6.8   -99   -99   -99
    88   Bw1  .197  .354  .419   .21   .15  1.35   .46  35.9  54.6   9.5   -99   6.8   -99   -99   -99
   105   Bw2  .189  .338  .414  .145   .15  1.34   .37  35.4  53.2  11.4   -99   6.7   -99   -99   -99

*PHS0000007  UPLB        SICL     105  SAN MANUEL  SHALLOW
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 CAGAYAN     PHILIPPINE        -99     -99 SiCL
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6    .4    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    10    Ap  .092  .173  .264     1  1.32  1.43  1.56  34.1  58.8   7.1   -99   6.9   -99   -99   -99
    45   AB1  .223  .376  .429  .577   .09  1.29   .78  40.7  48.3    11   -99   6.9   -99   -99   -99
    68   AB2  .228  .395  .429  .323   .09  1.36   .62  40.2  53.4   6.4   -99   6.8   -99   -99   -99
    88   Bw1  .197  .354  .419    .0   .15  1.35   .46  35.9  54.6   9.5   -99   6.8   -99   -99   -99
   105   Bw2  .189  .338  .414    .0   .15  1.34   .37  35.4  53.2  11.4   -99   6.7   -99   -99   -99
   
*PH00000006  UPLB        SIL      135  SAN MANUEL  DEEP
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 ILAGAN      PHILIPPINE        -99     -99 SiL
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6    .4    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    15   Apg  .124  .261  .403     1   .68  1.36   .93  21.7  55.6  22.7   -99   7.3   -99   -99   -99
    40   ABg  .152   .27  .378  .577   .23  1.48   .33    32    46    22   -99   7.4   -99   -99   -99
    70   Bg1  .129  .244  .371  .333   .23  1.48    .2  27.5  48.9  23.6   -99   7.3   -99   -99   -99
    95   Bg2   .12  .224  .341  .192  1.32  1.42   .15  26.9  45.8  27.3   -99   7.5   -99   -99   -99
   135   Bwg  .076  .157  .271    .1  1.32  1.38    .1  19.1  42.4  38.5   -99   7.5   -99   -99   -99

*PHS0000006  UPLB        SIL      135  SAN MANUEL  SHALLOW
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 ILAGAN      PHILIPPINE        -99     -99 SiL
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6    .4    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    15   Apg  .124  .261  .403     1   .68  1.36   .93  21.7  55.6  22.7   -99   7.3   -99   -99   -99
    40   ABg  .152   .27  .378  .577   .23  1.48   .33    32    46    22   -99   7.4   -99   -99   -99
    70   Bg1  .129  .244  .371  .333   .23  1.48    .2  27.5  48.9  23.6   -99   7.3   -99   -99   -99
    95   Bg2   .12  .224  .341    .0  1.32  1.42   .15  26.9  45.8  27.3   -99   7.5   -99   -99   -99
   135   Bwg  .076  .157  .271    .0  1.32  1.38    .1  19.1  42.4  38.5   -99   7.5   -99   -99   -99
   
*PH00000008  UPLB        C         29  FARAON  DEEP
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 BUKIDNON    PHILIPPINE     16.503 121.114 C
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6   .25    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
     8    Ap  .286  .452  .457     1   .06  1.17     2  48.6  39.7  11.7   -99   6.4   -99   -99   -99
    29    Bt  .394  .473  .544     1   .06   1.2  1.71  70.3  22.8     7   -99   6.6   -99   -99   -99
     
*PH00000009  UPLB        C        150  LA CASTELLANA DEEP  
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 BUKIDNON    PHILIPPINE     10.127  122.95 C
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6   .25    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    15     A  .372   .46  .516     1   .06  1.14  2.24    68  19.2  12.8   -99   4.6   -99   -99   -99	   
    34   Bt1  .411  .466  .534  .613   .06  1.22   1.1    78  14.2   7.8   -99   4.7   -99   -99   -99	   
    64   Bt2  .392  .452  .509  .375   .06  1.25   .86    76  15.2   8.8   -99   4.8   -99   -99   -99	   
    92   Bt3  .359  .429  .465   .21   .06   1.3   .44    73  16.2  10.8   -99   4.8   -99   -99   -99	   
   118   BC1  .339  .419  .442  .122   .06  1.32    .3    70  18.2  11.8   -99   4.8   -99   -99   -99	   
   150   BC2  .285  .391  .399  .069   .06  1.33   .35    60  24.8  15.2   -99   4.8   -99   -99   -99

*PHS0000009  UPLB        C        150  LA CASTELLANA  SHALLOW
@SITE        COUNTRY          LAT     LONG SCS FAMILY
 BUKIDNON    PHILIPPINE     10.127  122.95 C
@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE
    BN   .13     6   .25    81     1     1 IB001 IB001 IB001
@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC
    15     A  .372   .46  .516     1   .06  1.14  2.24    68  19.2  12.8   -99   4.6   -99   -99   -99
    34   Bt1  .411  .466  .534  .613   .06  1.22   1.1    78  14.2   7.8   -99   4.7   -99   -99   -99
    64   Bt2  .392  .452  .509  .375   .06  1.25   .86    76  15.2   8.8   -99   4.8   -99   -99   -99
    92   Bt3  .359  .429  .465    .0   .06   1.3   .44    73  16.2  10.8   -99   4.8   -99   -99   -99
   118   BC1  .339  .419  .442    .0   .06  1.32    .3    70  18.2  11.8   -99   4.8   -99   -99   -99
   150   BC2  .285  .391  .399    .0   .06  1.33   .35    60  24.8  15.2   -99   4.8   -99   -99   -99