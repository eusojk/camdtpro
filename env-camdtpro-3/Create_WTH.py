#Author: Eunjin Han
#Institute: IRI-Columbia University
#Date: 5/25/2019
#Purpose: Generate daily weather realizations by 1) calling WGEN and 2) combineing with latest weather observations
#########################
# (a) read long-term daily weather observations
# (b) call WGEN to generate weather realizations basedon on "Monthly" SCF and climatology if SCF is not available
# *note: WGEN program modified by EJ will generate full 365 days of data for a target year
# (c) combine latest observed weather time series + n-realizations => generate 100 WTH files for a year
#    If crop growing period goes beyond a year (e.g., planting in December), create two WTH files for two consecutative years
#########################
import numpy as np
import math
import scipy
import pandas as pd
import calendar
from scipy import stats
from scipy.stats import kurtosis, skew
from numpy.linalg import inv
from scipy.stats import gamma
from scipy.optimize import curve_fit
import time
import os   #operating system
import subprocess  #to run executable
import matplotlib.pyplot as plt
from WGEN_main_monthly_36 import WGEN_MAIN
from write_WTH_ensemble import write_WTH_ensemble
from write_WTH_ensemble import write_WTH_obs_ensemble
from write_WTH_ensemble import write_WTH_obs

start_time = time.perf_counter() # => not compatible with Python 2.7
def create_WTH_main(wdir,WTD_fname, station_name, LAT, LONG, ELEV, TAV, AMP, plt_date1, plt_date2, d_step, hv_date, frst_date1):
    #===================================================
    m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
    m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365] #ending date of each month for regular years
    m_doys_list2 = [1,32,61,92,122,153,183,214,245,275,306,336]  #starting date of each month for leap years
    m_doye_list2 = [31,60,91,121,152,182,213,244,274,305,335,366] #ending date of each month for leap years
    #m_doye_list2 = [31,59,91,121,152,182,213,244,274,305,335,366] #ignore 29th of Feb in case of leap year
    numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
    #======================================================================

    #===================================================
    #Input from GUI
    #===================================================
    # station_name = 'SANJ'   #===> Hard coded for now  
    # #==== header info for WTH file
    # LAT = 12.35    
    # LONG = 121.033
    # ELEV = 3
    # TAV = 28.3
    # AMP = 2.1
    # REFHT = -99.0
    # WNDHT = -99.0
    # #==== end of header info for WTH file
    # wdir = "C:\\Users\\Eunjin\\IRI\\PH_FAO\\WGEN_PH_36\\"  #===> Hard coded
    # #Year of the first planting date
    # ##plt_year1 = 2015
    # #target planting window to test (e.g., DJF for rice 2nd season)
    # plt_date1 = 2018121  # Dec 1, 2015   ********
    # plt_date2 = 2018181   # Feb 29, 2016
    # d_step = 10 #days
    IC_date = plt_date1 - 30 #for weather generation for IC => IC starts 30 days before the first planting date
    # hv_date = plt_date2 + 180 #tentative harvest date => long enough considering delayed growth

    # #The first date of SCF season => From this date, n realizations of synthetic weather data are written in *.WTH
    # #    Before, this date, observed weather data is written in *.WTH
    # #This is needed in case of hindcast from which obs weather is available throughout the crop growing season
    # #frst_date1 = 2015335   #******* from which ensemble 100 realizations are attached to the observed
    # frst_date1 = 2018121  #<<<<<<<<<==================== from which ensemble 100 realizations are attached to the observed

    #adjust IC_date if initial conditionn belong to the previous year
    if plt_date1%1000 <= 30:
        yr_end_date = 365
        if calendar.isleap(plt_date2//1000): yr_end_date = 366
        IC_date = (plt_date1//1000 -1)*1000 + (yr_end_date + (plt_date1%1000 - 30))

    # #adjust hv_date if harvest moves to a next year
    # yr_end_date = 365
    # if calendar.isleap(plt_date2//1000): yr_end_date = 366
    # if hv_date%1000 > yr_end_date:
    #     hv_date = hv_date - yr_end_date + 1000

    #================== MAIN PROGRAM ======================================
    print( '==START WEATHER GENERATION PROCEDURES')
    print( '==READ HISTORICAL WEATHER OBSERVATIONS (WTD) FILE')
    #1) read observed weather *.WTD (skip 1st row - heading)
    #obs_file = r'C:\IRI\WGEN_Richardson\FResampler_Python\PILI.WTD'
    # WTD_fname = r'C:\Users\Eunjin\IRI\PH_FAO\WGEN_PH_36\SANJ.WTD'
    data1 = np.loadtxt(WTD_fname,skiprows=1)
    #convert numpy array to dataframe
    WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
                'DOY':data1[:,0].astype(int)%1000,
                'SRAD':data1[:,1],
                'TMAX':data1[:,2],
                'TMIN':data1[:,3],
                'RAIN':data1[:,4]})
    year_array = WTD_df.YEAR.unique()
    nyears = year_array.shape[0]

    #2) MONTHLY tercile probability forecasts for 12 months of the main season (it can be 2nd year)
    #a) Depending on IC, the first planting date and last planting date, check how many "years"/csv files needed for reading
    if plt_date1//1000 == hv_date//1000 and plt_date1//1000 == IC_date//1000:
        #frst_fname = r'C:\Users\Eunjin\IRI\PH_FAO\WGEN_PH_36\SCF_input_monthly1.csv'
        frst_fname = wdir.replace("/","\\") + "\\SCF_input_monthly1.csv"
        df_scf = pd.read_csv(frst_fname, header = 0) #read *.csv data into a dataframe

        #Call WGEN function to generate 100 weather realizations for a full year (365/366 days)
        df_WGEN_bcorr = WGEN_MAIN(WTD_df,df_scf, wdir, station_name, LAT, plt_date1//1000)   #plt_date1//1000 => target year
    elif plt_date1//1000 != IC_date//1000:
        frst_fname0 = wdir.replace("/","\\") + "\\SCF_input_monthly1.csv"  #weather generation from IC to the first planting date
        df_scf0 = pd.read_csv(frst_fname0, header = 0) #read *.csv data into a dataframe
        #Call WGEN function to generate 100 weather realizations for a full year (365/366 days)
        df_WGEN_bcorr0 = WGEN_MAIN(WTD_df,df_scf0, wdir, station_name, LAT, IC_date//1000)   #IC_date//1000 => target year

        frst_fname = wdir.replace("/","\\") + "\\SCF_input_monthly2.csv"  #main planting period
        df_scf = pd.read_csv(frst_fname, header = 0)  
        #Call WGEN function to generate 100 weather realizations for a full year (365/366 days)
        df_WGEN_bcorr = WGEN_MAIN(WTD_df,df_scf, wdir, station_name, LAT, plt_date1//1000)   #plt_date1//1000 => target year
    elif plt_date1//1000 != plt_date2//1000 :  #crop growing period covers two consecutive years
        frst_fname = wdir.replace("/","\\") + "\\SCF_input_monthly1.csv"  #main planting period
        df_scf = pd.read_csv(frst_fname, header = 0)  
        #Call WGEN function to generate 100 weather realizations for a full year (365/366 days)
        df_WGEN_bcorr = WGEN_MAIN (WTD_df,df_scf, wdir, station_name, LAT, plt_date1//1000)   #plt_date1//1000 => target year

        frst_fname2 = wdir.replace("/","\\") + "\\SCF_input_monthly2.csv"  #weather generation from IC to the first planting date
        df_scf2 = pd.read_csv(frst_fname2, header = 0) #read *.csv data into a dataframe
        #Call WGEN function to generate 100 weather realizations for a full year (365/366 days)
        df_WGEN_bcorr2 = WGEN_MAIN(WTD_df,df_scf2, wdir, station_name, LAT, plt_date2//1000)   #plt_date1//1000 => target year

    elif hv_date//1000 != plt_date2//1000 :  #crop growing period covers two consecutive years
        frst_fname = wdir.replace("/","\\") + "\\SCF_input_monthly1.csv"  #main planting period
        df_scf = pd.read_csv(frst_fname, header = 0)  
        #Call WGEN function to generate 100 weather realizations for a full year (365/366 days)
        df_WGEN_bcorr = WGEN_MAIN(WTD_df,df_scf, wdir, station_name, LAT, plt_date1//1000)   #plt_date1//1000 => target year

        frst_fname2 = wdir.replace("/","\\") + "\\SCF_input_monthly2.csv"  #weather generation for the second year until harvest date
        df_scf2 = pd.read_csv(frst_fname2, header = 0) #read *.csv data into a dataframe
        #Call WGEN function to generate 100 weather realizations for a full year (365/366 days)
        df_WGEN_bcorr2 = WGEN_MAIN(WTD_df,df_scf2, wdir, station_name, LAT, hv_date//1000)   #plt_date1//1000 => target year

    else:
        print('No case selected in opening df_WGEN_bcorr or df_WGEN_bcorr2')
        os.system('pause')
    #=================================================================

    end_time0 = time.perf_counter() # => not compatible with Python 2.7
    print('It took {0:5.1f} sec to run WGEN to create 100 weather realizations in Create_WTH.py'.format(end_time0-start_time))
    start_time1 = time.perf_counter()  # => not compatible with Python 2.7

    #Last observed date of the weather data  
    # Note => In case of hindcast, we assume that observed data is available until start of SCF months
    # WTD_last_date = WTD_df.DOY.values(-1)
    # WTD_last_year = WTD_df.YEAR.values(-1) 
    WTD_last_date = int(repr(WTD_df.YEAR.values[-1]) + repr(WTD_df.DOY.values[-1]).zfill(3))  #e.g., 2017090
    year_array = df_WGEN_bcorr.i_yr.unique()
    nrealiz = year_array.shape[0]

    #write 100 WTH files for each year
    print( '==Create_WTH: Start to write 100 *.WTH files - please wait...')
    # 1) when only one year is needed
    if plt_date1//1000 == hv_date//1000 and plt_date1//1000 == IC_date//1000:
        #(a) write WGEN generated weather realizations from the beginning of crop growht (i.e., IC)
        if frst_date1 <= IC_date: 
            #call a function
            write_WTH_ensemble(IC_date, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
        #(b) write observed weather until frst_date1 and then WGEN generated weather realizations 
        elif frst_date1 > IC_date: 
            if WTD_last_date >= frst_date1:  
                #write observed weather until frst_date1 and then WGEN generated weather realizations 
                #This is the typical case of hindcast simulation
                #call a function
                write_WTH_obs_ensemble(IC_date, frst_date1, WTD_df, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
            
            elif WTD_last_date >= IC_date and WTD_last_date < frst_date1: 
            #write observed weather until WTD_last_date and then Fill the missing values between WTD_last_date and  IC_date with WGEN-output from climatology
                # print( '==Warning (1): Last observed weather data is between IC of crop simulation (i.e., 30 days before plating) and the start date of SCF')
                # print('Missing days between last observed data and frst_date1 is filled with the WGEN-output from climatology')
                # print('Hit ENTER to continue!')
                # os.system('pause')
                write_WTH_obs_ensemble(IC_date, WTD_last_date+1, WTD_df, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)

            elif WTD_last_date < IC_date: #fill missing days between IC_date and frst_date1 with WGEN-output from climatology
                # print( '==Warning (2): Last observed weather data is before IC of crop simulation (i.e., 30 days before plating)')
                # print('Missing days between IC_date and frst_date1 is filled with the WGEN-output from climatology')
                # print('Hit ENTER to continue!')
                # os.system('pause')
                write_WTH_ensemble(IC_date, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
            else:
                print('No case selected in writing WTH: plt_date1//1000 == hv_date//1000 and plt_date1//1000 == IC_date//1000')
                os.system('pause')
        #write obs reference weather data (e.g., SANJ1501.WTH)
        if WTD_last_date >= hv_date:  #write only when obs data is available for full groiwng period
            write_WTH_obs(IC_date, hv_date%1000, 1, WTD_df, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)

    elif plt_date1//1000 != IC_date//1000:  #=> need to generare WTH for two consecutive years
        if frst_date1 <= IC_date:  #(a) write WGEN generated weather realizations from the beginning of crop growht (i.e., IC)
            #call a function for the first year
            write_WTH_ensemble(IC_date, df_WGEN_bcorr0, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
            #call a function for the second year
            tempe_date = int(repr(plt_date1//1000)+'001')
            write_WTH_ensemble(tempe_date, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
        elif frst_date1 > IC_date:  #(b) write observed weather until frst_date1 and then WGEN generated weather realizations 
            if WTD_last_date >= frst_date1:  
                yr_end_date = 365
                if calendar.isleap(IC_date//1000): yr_end_date = 366
                #write observed weather until frst_date1 and then WGEN generated weather realizations 
                #This is the typical case of hindcast simulation
                #call a function i) to write obs weather for the "first" year (i.e., IC date to doy=365)
                write_WTH_obs(IC_date, yr_end_date, nrealiz, WTD_df, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                #call a function 2) to write obs weather (from doy = 1 to frst_date1) + WGEN data for the second year 
                tempe_date = int(repr(plt_date1//1000)+'001')
                write_WTH_ensemble(tempe_date, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)

            elif WTD_last_date >= IC_date and WTD_last_date < frst_date1: 
            #write observed weather until WTD_last_date and then Fill the missing values between WTD_last_date and  IC_date with WGEN-output from climatology
                # print( '==Warning(3): Last observed weather data is between IC of crop simulation (i.e., 30 days before plating) and the start date of SCF')
                # print('Missing days between last observed data and frst_date1 is filled with the WGEN-output from climatology')
                # print('Hit ENTER to continue!')
                # os.system('pause')
                #call a function i) to write obs + ensemble for December of the "first" year (i.e., IC date to doy=365)
                write_WTH_obs_ensemble(IC_date, WTD_last_date+1, WTD_df, df_WGEN_bcorr0, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                #call a function for the second year
                tempe_date = int(repr(plt_date1//1000)+'001')
                write_WTH_ensemble(tempe_date, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)

            elif WTD_last_date < IC_date: #fill missing days between IC_date and frst_date1 with WGEN-output from climatology
                # print( '==Warning(4): Last observed weather data is before IC of crop simulation (i.e., 30 days before plating)')
                # print('Missing days between IC_date and frst_date1 is filled with the WGEN-output from climatology')
                # print('Hit ENTER to continue!')
                # os.system('pause')
                #call a function for the first year
                write_WTH_ensemble(IC_date, df_WGEN_bcorr0, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                #call a function for the second year
                tempe_date = int(repr(plt_date1//1000)+'001')
                write_WTH_ensemble(tempe_date, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)

        #write obs reference weather data (e.g., SANJ1501.WTH)
        if WTD_last_date >= hv_date:  #write only when obs data is available for full groiwng period
            yr_end_date = 365
            if calendar.isleap(IC_date//1000): yr_end_date = 366
            write_WTH_obs(IC_date, yr_end_date, 1, WTD_df, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)  #first year until doy=365
            tempe_date = int(repr(plt_date1//1000)+'001')
            write_WTH_obs(tempe_date, hv_date%1000, 1, WTD_df, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)  #second year until harvest

    elif plt_date1//1000 != plt_date2//1000: #=> need to generare WTH for two consecutive years
        if frst_date1 <= IC_date:  #(a) write WGEN generated weather realizations from the beginning of crop growht (i.e., IC)
            #call a function for the first year for the "first planting date"
            write_WTH_ensemble(IC_date, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
            #call a function for the second year for the "second planting date"
            #tempe_date = int(repr(plt_date1//1000)+'001')
            tempe_date = int(repr(plt_date2//1000)+'001') #EJ(6/14/2019)
            write_WTH_ensemble(tempe_date, df_WGEN_bcorr2, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
        elif frst_date1 > IC_date:  #(b) write observed weather until frst_date1 and then WGEN generated weather realizations 
            if WTD_last_date >= frst_date1:  
                #write observed weather until frst_date1 and then WGEN generated weather realizations 
                #This is the typical case of hindcast simulation
                if frst_date1//1000 == plt_date1//1000:
                    #call a function i) to write obs + ensemble for December of the "first" year (i.e., IC date to doy=365)
                    write_WTH_obs_ensemble(IC_date, frst_date1, WTD_df, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                    #call a function 2) to write obs weather (from doy = 1 to frst_date1) + WGEN data for the second year 
                    tempe_date = int(repr(plt_date2//1000)+'001')
                    write_WTH_ensemble(tempe_date, df_WGEN_bcorr2, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                else:  #frst_date1//1000 == plt_date2//1000:
                    #call a function i) to write obs weather for the "first" year (i.e., IC date to doy=365)
                    yr_end_date = 365
                    if calendar.isleap(IC_date//1000): yr_end_date = 366
                    write_WTH_obs(IC_date,yr_end_date, nrealiz, WTD_df, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                    #call a function ii) to write obs + ensemble for the "second" year
                    tempe_date = int(repr(plt_date2//1000)+'001')
                    write_WTH_obs_ensemble(tempe_date, frst_date1, WTD_df, df_WGEN_bcorr2, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
            elif WTD_last_date >= IC_date and WTD_last_date < frst_date1: 
            #write observed weather until WTD_last_date and then Fill the missing values between WTD_last_date and  IC_date with WGEN-output from climatology
                # print( '==Warning(5): Last observed weather data is between IC of crop simulation (i.e., 30 days before plating) and the start date of SCF')
                # print('Missing days between last observed data and frst_date1 is filled with the WGEN-output from climatology')
                # print('Hit ENTER to continue!')
                # os.system('pause')
                if frst_date1//1000 == plt_date1//1000:
                    #call a function i) to write obs + ensemble for December of the "first" year (i.e., IC date to doy=365)
                    write_WTH_obs_ensemble(IC_date, WTD_last_date+1, WTD_df, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                    #call a function 2) to write obs weather (from doy = 1 to frst_date1) + WGEN data for the second year 
                    tempe_date = int(repr(plt_date2//1000)+'001')
                    write_WTH_ensemble(tempe_date, df_WGEN_bcorr2, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                else:  #frst_date1//1000 == plt_date2//1000:
                    if WTD_last_date//1000 == plt_date1//1000:
                        #call a function ii) to write obs + ensemble for the "first" year
                        write_WTH_obs_ensemble(IC_date, WTD_last_date+1, WTD_df, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                    else:   #WTD_last_date//1000 == plt_date2//1000:
                        #call a function i) to write obs weather for the "first" year (i.e., IC date to doy=365)
                        yr_end_date = 365
                        if calendar.isleap(IC_date//1000): yr_end_date = 366
                        write_WTH_obs(IC_date, yr_end_date, nrealiz, WTD_df, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                        #call a function ii) to write obs + ensemble for the "second" year
                        tempe_date = int(repr(plt_date2//1000)+'001')
                        write_WTH_obs_ensemble(tempe_date, frst_date1, WTD_df, df_WGEN_bcorr2, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
            elif WTD_last_date < IC_date: #fill missing days between IC_date and frst_date1 with WGEN-output from climatology
                # print( '==Warning(6): Last observed weather data is before IC of crop simulation (i.e., 30 days before plating)')
                # print('Missing days between IC_date and frst_date1 is filled with the WGEN-output from climatology')
                # print('Hit ENTER to continue!')
                # os.system('pause')
                #call a function i) to write ensemble for the "first" year
                write_WTH_ensemble(IC_date, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                #call a function i) to write ensemble for the "second" year
                tempe_date = int(repr(plt_date2//1000)+'001')
                write_WTH_ensemble(tempe_date, df_WGEN_bcorr2, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
            else:
                print('No case selected in writing WTH: plt_date1//1000 != plt_date2//1000: => frst_date1 > IC_date')
                os.system('pause')       
        else:
            print('No case selected in writing WTH: plt_date1//1000 != plt_date2//1000: ')
            os.system('pause')
        #write obs reference weather data (e.g., SANJ1501.WTH)
        if WTD_last_date >= hv_date:  #write only when obs data is available for full groiwng period
            yr_end_date = 365
            if calendar.isleap(IC_date//1000): yr_end_date = 366
            write_WTH_obs(IC_date, yr_end_date, 1, WTD_df, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)  #first year until doy=365
            tempe_date = int(repr(plt_date2//1000)+'001')
            write_WTH_obs(tempe_date, hv_date%1000, 1, WTD_df, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)  #second year until harvest
    elif hv_date//1000 != plt_date2//1000 :  #crop growing period covers two consecutive years
    ##==> This is same as the case of 'plt_date1//1000 == hv_date//1000 and plt_date1//1000 == IC_date//1000"
    ##     but 100 emsemeble weather realizations were created for the second year to make weather data until harvest
        #(a) write WGEN generated weather realizations from the beginning of crop growht (i.e., IC)
        if frst_date1 <= IC_date: 
            #call a function
            write_WTH_ensemble(IC_date, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
            #call a function i) to write ensemble for the "second" year
            tempe_date = int(repr(hv_date//1000)+'001')
            write_WTH_ensemble(tempe_date, df_WGEN_bcorr2, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
        #(b) write observed weather until frst_date1 and then WGEN generated weather realizations 
        elif frst_date1 > IC_date: 
            if WTD_last_date >= frst_date1:  
                #write observed weather until frst_date1 and then WGEN generated weather realizations 
                #This is the typical case of hindcast simulation
                #call a function
                write_WTH_obs_ensemble(IC_date, frst_date1, WTD_df, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                #call a function i) to write ensemble for the "second" year
                tempe_date = int(repr(hv_date//1000)+'001')
                write_WTH_ensemble(tempe_date, df_WGEN_bcorr2, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)          
            elif WTD_last_date >= IC_date and WTD_last_date < frst_date1: 
            #write observed weather until WTD_last_date and then Fill the missing values between WTD_last_date and  IC_date with WGEN-output from climatology
                # print( '==Warning (1): Last observed weather data is between IC of crop simulation (i.e., 30 days before plating) and the start date of SCF')
                # print('Missing days between last observed data and frst_date1 is filled with the WGEN-output from climatology')
                # print('Hit ENTER to continue!')
                # os.system('pause')
                write_WTH_obs_ensemble(IC_date, WTD_last_date+1, WTD_df, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                #call a function i) to write ensemble for the "second" year
                tempe_date = int(repr(hv_date//1000)+'001')
                write_WTH_ensemble(tempe_date, df_WGEN_bcorr2, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)  
            elif WTD_last_date < IC_date: #fill missing days between IC_date and frst_date1 with WGEN-output from climatology
                # print( '==Warning (2): Last observed weather data is before IC of crop simulation (i.e., 30 days before plating)')
                # print('Missing days between IC_date and frst_date1 is filled with the WGEN-output from climatology')
                # print('Hit ENTER to continue!')
                # os.system('pause')
                write_WTH_ensemble(IC_date, df_WGEN_bcorr, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)
                #call a function i) to write ensemble for the "second" year
                tempe_date = int(repr(hv_date//1000)+'001')
                write_WTH_ensemble(tempe_date, df_WGEN_bcorr2, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)  
            else:
                print('No case selected in writing WTH: hv_date//1000 != plt_date2//1000')
                os.system('pause')
        #write obs reference weather data (e.g., SANJ1501.WTH)
        if WTD_last_date >= hv_date:  #write only when obs data is available for full groiwng period
            yr_end_date = 365
            if calendar.isleap(IC_date//1000): yr_end_date = 366
            write_WTH_obs(IC_date, yr_end_date, 1, WTD_df, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)  #first year until doy=365
            tempe_date = int(repr(hv_date//1000)+'001')
            write_WTH_obs(tempe_date, hv_date%1000, 1, WTD_df, wdir, station_name, LAT, LONG, ELEV, TAV, AMP)  #second year until harvest
    else:
        print('No case selected in writing WTH files')
        os.system('pause')

    end_time1 = time.perf_counter()  # => not compatible with Python 2.7
    print('It took {0:5.1f} sec to write 100 WTH files in Create_WTH.py'.format(end_time1-start_time1))


    end_time = time.perf_counter()  # => not compatible with Python 2.7
    print("It took {0:5.1f}, sec to finish Create_WTH.py". format(end_time-start_time)) 
    print( '==End of writing 100 *.WTH files')