# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 11:39:58 2019

@author: kpodojos
"""

import glob
import pandas as pd
import numpy as np 
import os.path
# from sklearn.externals import joblib
import joblib
import matplotlib as mpl 
## agg backend is used to create plot as a .png file
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import matplotlib.pyplot as plt 

import sklearn
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression, Lasso
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from numpy import genfromtxt

wdir   = os.path.dirname(os.path.abspath(__file__))
tmp_wtd = wdir + "\\" + "tmp_wtd\\*.WTD"
wdr = wdir + "\\" + "wdr\\*.WTH"
mdlDir = wdir + "\\" + "data"

genMode = 0 # Generator used: 1 (WGEN) and 0 (FRESAMPLER)
colnames = ['index', 'values']
# Saved models:
modelRAINSaved = mdlDir + "\\" + 'lrModelRAIN.pkl'
modelTEMPSaved = mdlDir + "\\" + 'lrModelTEMP.pkl'
modelTRHSaved = mdlDir + "\\" + 'lrModelTRH.pkl'
modelRTRHSaved = mdlDir + "\\" + 'lrModelRTRH.pkl'

eriDonLRSaved = mdlDir + "\\" + 'eriDonLR.pkl'
eriDonLASaved = mdlDir + "\\" + 'eriDonLA.pkl'
eriDonRFSaved = mdlDir + "\\" + 'eriDonRF.pkl'

donLR = mdlDir + "\\" + 'resiDONLR.csv'
donLA = mdlDir + "\\" + 'resiDONLA.csv'
donRF = mdlDir + "\\" + 'resiDONRF.csv'
residualsRaRhTemp = pd.read_csv(mdlDir + "\\" + 'residualsRaRhTemp.csv', names=colnames, header=None)
residualsRhTemp = pd.read_csv(mdlDir + "\\" + 'residualsRhTemp.csv', names=colnames, header=None)
residualsRain = pd.read_csv(mdlDir + "\\" + 'residualsRain.csv', names=colnames, header=None)
residualsTemp = pd.read_csv(mdlDir + "\\" + 'residualsTemp.csv', names=colnames, header=None)
resiDONLR = genfromtxt(donLR, delimiter=',')
resiDONLA = genfromtxt(donLA, delimiter=',')
resiDONRF = genfromtxt(donRF, delimiter=',')



dfSavingDisp = pd.DataFrame()
df30 = pd.DataFrame()

# First day of June and last day of Septemeber as DOY in year 2017
# Retrieve data based on time frame
doyJune17 = 2018152
doySept17 = 2018273
place = 0

whichLR = '' # any of the pickled model
whichReg = '' #0 (Rain), 1(T), 2(RH-T), 3(R-T-RH)
whichRes  = '' # the residuals to use


regListRAIN = [ 'RAIN-Jun-W2', 'RAIN-Sep-W1', 'RAIN-Jul-W3', 'RAIN-Jul-W1', 'RAIN-Sep-W2', 'RAIN-Jul-W4', 'RAIN-Sep-W3', 'RAIN-Aug-W1', 'RAIN-Sep-W4', 'RAIN-Aug-W3', 'RAIN-Jun-W1']
regListTEMP = [  'TEMP-Sep-W1','TEMP-Aug-W1', 'TEMP-Jul-W1',  'TEMP-Sep-W2', 'TEMP-Jul-W4' ]
regListRH = [  'RH-Jul-W2','RH-Jun-W4', 'RH-Jun-W1', 'RH-Jul-W4',  'RH-Jul-W1', 'RH-Aug-W1', 'RH-Jul-W3', 'RH-Jun-W3'   ]
rgLsTRH = [ 'TEMP-Aug-W3', 'TEMP-Sep-W3', 'TEMP-Sep-W2', 'TEMP-Jun-W1', 'TEMP-Jun-W3', 'TEMP-Jun-W3', 'TEMP-Jul-W4',  'TEMP-Aug-W4'   ]
rgLsRTRH = [ 'RH-Aug-W2', 'TEMP-Jun-W4', 'TEMP-Aug-W1', 'RH-Jun-W3', 'TEMP-Jun-W2', 'RH-Sep-W4', 'TEMP-Sep-W4', 'RH-Jul-W4', 'TEMP-Jul-W1', 'TEMP-Aug-W3', 'RH-Jul-W2']


dfRH30 = pd.read_csv(mdlDir + "\\" + 'dfRH30.csv')
dfRAIN30 = pd.read_csv(mdlDir + "\\" + 'dfRAIN30.csv')
dfTEMP30 = pd.read_csv(mdlDir + "\\" + 'dfTEMP30.csv')

dfRH30.drop('Unnamed: 0', axis=1, inplace=True)
dfRAIN30.drop('Unnamed: 0', axis=1, inplace=True)
dfTEMP30.drop('Unnamed: 0', axis=1, inplace=True)

dfSetup1 = pd.DataFrame()
station = ''

def iniVarDf(N, var):
    """
    akes in the type of var (R/T/P) and
    creates a 200 x 4 dataframe | meaning (WTH1...WTH200) X (VarW1...VarW4)
    """
    
    # Make rows names, 200 by default
    rw = N*['WTH']
    rwr=[]
    for i,j in enumerate(rw):
        stri = j + str(i+1)
        rwr.append(stri)
    
    # Make columns name
    colJu = 4*['-Jun-W']
    colJl = 4*['-Jul-W']
    colAu = 4*['-Aug-W']
    colSe = 4*['-Sep-W']
    col = [colJu, colJl, colAu, colSe ]
    colr = []
    for z in range(len(col)):
        coli = col[z]
        for i,j in enumerate(coli):
            stri = str(var) + j + str(i+1)
            colr.append(stri)
        
    return pd.DataFrame(columns=colr, index=rwr)
    
def getAvg(df):
    return df.mean(axis=1)
    
def makeWeekly(df):
    
    # Get all the indexes
    allIndexes = df.index.tolist()
    
    # needed for rolling weekly:
    wkMean = df.rolling(7).mean()
    lstMeanVal = []
    
    for i in allIndexes:
        if i%7 == 6:
            val = wkMean.iloc[i]
            lstMeanVal.append(val)
    
    return lstMeanVal   
    
def setRainProb(value):
    if value>1:
        return 1
    else:
        return 0

def setTempProb(value):
    if value>24:
        return 1
    else:
        return 0

def setRhProb(value):
    if value>80:
        return 1
    else:
        return 0
    
def convRain(rain):
    return rain/25.4
 
def convRainToMilli(rainInch):
    return rainInch*25.4
    
def convTAVToFar(temp):
    return (temp * 1.8) + 32 
    
def convTAVToCel(temp):
    return (temp - 32) / 1.8
    
def applyStSca(z, mu, s):
    return (z - mu) / s

def getMuStd(df, code):
    global lstMuStdRAIN, lstMuStdRH, lstMuStdTEMP
    
    dfSTd = df
    
    if code == 'RAIN':
        dicti = lstMuStdRAIN
    elif code == 'RH':
        dicti = lstMuStdRH
    elif code == 'TEMP':
        dicti = lstMuStdTEMP
    else:
        return
     
    colLst = df.columns.values.tolist()
    for i in range(len(colLst)):
        col = colLst[i]
        muSt= dicti[col]
        std = muSt[0]
        mu  = muSt[1]
        
        dfSTd[col] = dfSTd[col].apply( ( lambda z: applyStSca(z, mu, std) ) )
    
    return dfSTd


def parseWTD(wtdDir, conversion):
    
    global doyJune17, doySept17, genMode 
    
    numWTH = len(wtdDir)
    if genMode == 0:
        skips = 1
    else:
        skips = 5    
    
    # Initialize our main dataframes
    dfRH = iniVarDf(numWTH, 'RH')
    dfRAIN = iniVarDf(numWTH, 'RAIN')
    dfTEMP = iniVarDf(numWTH, 'TEMP')
    
    lstDF = []
    
    for fnum in range(numWTH):
        
        # needs to be iterate over later
        tmpFile = wtdDir[fnum]
        if genMode == 1:
            columns = ['DATE', 'SRAD', 'TMAX', 'TMIN', 'RAIN']
        else:
            columns = ['DATE', 'SRAD', 'TMAX', 'TMIN', 'RAIN', 'RH']
            
        dfTmp  = pd.read_csv(tmpFile, delim_whitespace=True, skiprows=skips, names=columns)
        
        
        
        dfSel = dfTmp[(dfTmp['DATE'] >= doyJune17) & (dfTmp['DATE'] <= doySept17 )].reset_index(drop=True)
        
        dfTempAvg = dfSel[['TMAX', 'TMIN']]
        dfTempAvg = getAvg(dfTempAvg)
        
        if conversion == 1:
            dfTempAvgConv = dfTempAvg.apply(convTAVToFar)
            dfRainConv = dfSel['RAIN'].apply(convRain)
        else:
            dfTempAvgConv = dfTempAvg
            dfRainConv = dfSel['RAIN']
         
        if genMode == 0:   
            dfRHNoConv = dfSel['RH']
        
        meanListRAIN = makeWeekly(dfRainConv)
        meanListTEMP = makeWeekly(dfTempAvgConv)
        if genMode == 0: 
            meanListRH = makeWeekly(dfRHNoConv)
        
        # make sure we have 16 entries
        meanListRAIN.pop() 
        meanListTEMP.pop()
        if genMode == 0:
            meanListRH.pop()
        
        dfRAIN.iloc[fnum] = meanListRAIN
        dfTEMP.iloc[fnum] = meanListTEMP
        if genMode == 0:
            dfRH.iloc[fnum] = meanListRH
            
    if genMode == 0:
        lstDF.append(dfRH)
    lstDF.append(dfRAIN)
    lstDF.append(dfTEMP)
    
    return lstDF


#def main():
def setupWthParser(dfSavingDispInp, dfSavingStp1, gmod, reginp=0, doninp='LR'):
    """
    int_ reginp: 0 (Rain), 1(T), 2(RH-T), 3(R-T-RH)
    str_ doninp: LR (Linear regression), LA (lasso), RF (Random Forest)
    df_ dfSavingDispInp: where to save img paths
    """
    global place, whichReg, whichRes, tmp_wtd, wdr, regListRAIN, regListTEMP, regListRH, dfSavingDisp, modelRAINSaved,\
           modelTEMPSaved, modelTRHSaved, modelRTRHSaved, dfRAIN30, dfRH30, dfTEMP30, doyJune17, doySept17, \
           residualsRain, residualsTemp, residualsRhTemp, residualsRaRhTemp, genMode, dfSetup1, station 
    
    dfSetup1 = dfSavingStp1
    station = dfSetup1['Values'].iloc[0] 
    cyear = dfSetup1['Values'].iloc[1]
    
    # FRESampler mode. Reading from WTD: year ~ 2017152
    doyJune17 = 1000*int(cyear) + 152
    doySept17 = 1000*int(cyear) + 273
    
    
    dfSavingDisp = dfSavingDispInp
    whichReg = reginp
    
    genMode = gmod # Generator used: 1 (WGEN) and 0 (FRESAMPLER)    
    if genMode == 1:
        file_pathX = glob.glob(wdr)
        file_path = []
        # WGEN mode. Reading from WTD: year ~ 17152
        doyJune17 -= 2000000
        doySept17 -= 2000000
        N = len(file_pathX)
        for i in range(N):
            filePath = file_pathX[i]
            filelg = filePath.split('\\')
            bgn = filelg[-1] # e.g. '00011701.WTH'
            bgn = bgn[0] 
            if bgn == '0':
                file_path.append(filePath)
        
    else:
        file_path = glob.glob(tmp_wtd)
        
    numWTH = len(file_path) 
        
    if (numWTH >200 and numWTH < 100):
        print('Missing WTD files, only got {} but should be  100 or 200'.format(numWTH))
    
    else:
        lstDF = parseWTD(file_path, 1)
        lstDFraw = parseWTD(file_path, 0)
        
        # Non standardized values
        if genMode == 0:
            dfRH = lstDF[0]
            dfRHRaw = lstDFraw[0]
            dfRH30Raw = dfRH30
            dfRhTmpProb = iniVarDf(numWTH, 'RHTMP')
            dfRhProb = dfRHRaw.applymap(setRhProb)
            dfRhTmp30Prob = iniVarDf(30, 'RHTMP')
            dfRh30Prob = dfRH30Raw.applymap(setRhProb)
            
            dfRAIN = lstDF[1]
            dfTEMP = lstDF[2]
            dfRAINRaw = lstDFraw[1]
            dfTEMPRaw = lstDFraw[2]
            
        else:
            dfRAIN = lstDF[0]
            dfTEMP = lstDF[1]
            dfRAINRaw = lstDFraw[0]
            dfTEMPRaw = lstDFraw[1]
            
    
        #dfRH30Raw = dfRH30
        dfRAIN30Raw = dfRAIN30.applymap(convRainToMilli)
        dfTEMP30Raw = dfTEMP30.applymap(convTAVToCel)    
    
        #dfRhTmpProb = iniVarDf(numWTH, 'RHTMP')
        dfRainProb = dfRAINRaw.applymap(setRainProb)
        dfTempProb = dfTEMPRaw.applymap(setTempProb)
        #dfRhProb = dfRHRaw.applymap(setRhProb)
        
        #dfRhTmp30Prob = iniVarDf(30, 'RHTMP')
        dfRain30Prob = dfRAIN30Raw.applymap(setRainProb)
        dfTemp30Prob = dfTEMP30Raw.applymap(setTempProb)
        #dfRh30Prob = dfRH30Raw.applymap(setRhProb)
        
        if genMode == 0:
    
            for coli in range(16): # forecast
                colTemp = dfTempProb.iloc[:, coli]
                rowsTemp = colTemp.iteritems()
                rowsTempList = []
                colRh = dfRhProb.iloc[:, coli]
                rowsRh = colRh.iteritems()            
                rowsRhList = []             
                truthTable = []
            
                # append the values 0 or 1 to a list
                for i,j in rowsTemp:
                    rowsTempList.append(j)
                    
                for a,b in rowsRh:
                    rowsRhList.append(b)
            
                # apply AND to corresponding items in list
                for i in range(len(rowsTempList)):
                    truth = rowsTempList[i] and rowsRhList[i] 
                    truthTable.append(truth)
                dfRhTmpProb.iloc[:, coli] = truthTable #np.where(colTemp == colRh     , 1, 0)
            
            for coli in range(16): # climatology
                colTemp = dfTemp30Prob.iloc[:, coli]
                colRh = dfRh30Prob.iloc[:, coli]
                rowsTemp = colTemp.iteritems()
                rowsRh = colRh.iteritems()
                rowsTempList = []
                rowsRhList = []
                truthTable = []
            
                # append the values 0 or 1 to a list
                for i,j in rowsTemp:
                    rowsTempList.append(j)
                    
                for a,b in rowsRh:
                    rowsRhList.append(b)
            
                # apply AND to corresponding items in list
                for i in range(len(rowsTempList)):
                    truth = rowsTempList[i] and rowsRhList[i] 
                    truthTable.append(truth)     
                dfRhTmp30Prob.iloc[:, coli] = truthTable    
        
        
        if genMode == 0:
            problist = [dfRainProb, dfTempProb, dfRhProb, dfRhTmpProb]
            problist30 = [dfRain30Prob, dfTemp30Prob, dfRh30Prob, dfRhTmp30Prob]
        else:
            problist = [dfRainProb, dfTempProb]
            problist30 = [dfRain30Prob, dfTemp30Prob]
            
        
        plotProb(problist, problist30)
            
        col2 = dfRAIN.columns.values.tolist()
        col4 = dfTEMP.columns.values.tolist()
        col5 = dfRAIN30.columns.values.tolist()   
        col7 = dfTEMP30.columns.values.tolist()
        
        if genMode == 0:
            col3 = dfRH.columns.values.tolist()
            col6 = dfRH30.columns.values.tolist()
        
        stdScaler1 = StandardScaler()
        stdScaler2 = StandardScaler()
        stdScaler3 = StandardScaler()
        stdScaler4 = StandardScaler()
        stdScaler5 = StandardScaler()
        stdScaler6 = StandardScaler()
        
        stdScaler1.fit(dfRAIN)
        
        stdScaler3.fit(dfTEMP)
        stdScaler4.fit(dfRAIN30)
        stdScaler6.fit(dfTEMP30)     
        
        if genMode == 0:
            stdScaler2.fit(dfRH)
            stdScaler5.fit(dfRH30)
            
        
        dfRAINSTD = stdScaler1.transform(dfRAIN)
        dfRAINSTD = pd.DataFrame(dfRAINSTD)
        dfRAINSTD = dfRAINSTD.rename(columns=lambda x: col2[int(x)])    
        
        dfRAIN30STD = stdScaler4.transform(dfRAIN30)
        dfRAIN30STD = pd.DataFrame(dfRAIN30STD)
        dfRAIN30STD = dfRAIN30STD.rename(columns=lambda x: col5[int(x)])      
        
        if genMode == 0:
            dfRHSTD = stdScaler2.transform(dfRH)
            dfRHSTD = pd.DataFrame(dfRHSTD)
            dfRHSTD = dfRHSTD.rename(columns=lambda x: col3[int(x)])   
            
            dfRH30STD = stdScaler5.transform(dfRH30)
            dfRH30STD = pd.DataFrame(dfRH30STD)
            dfRH30STD = dfRH30STD.rename(columns=lambda x: col6[int(x)])    
        
        
        dfTEMPSTD = stdScaler3.transform(dfTEMP)
        dfTEMPSTD = pd.DataFrame(dfTEMPSTD)
        dfTEMPSTD = dfTEMPSTD.rename(columns=lambda x: col4[int(x)]) 
        
        dfTEMP30STD = stdScaler6.transform(dfTEMP30)
        dfTEMP30STD = pd.DataFrame(dfTEMP30STD)
        dfTEMP30STD = dfTEMP30STD.rename(columns=lambda x: col7[int(x)])         
        
        
        # working DF needed 
        if reginp == 0:
            wkDF = dfRAINSTD[regListRAIN]
            wkDF30 = dfRAIN30STD[regListRAIN]
            ml = modelRAINSaved
            whichRes  = residualsRain
            # Figure the index where to save the images paths
            if doninp == 'LR':
                place = 0
            elif doninp == 'LA':
                place = 1
            else:
                place = 2
                
        elif reginp == 1:
            wkDF = dfTEMPSTD[regListTEMP]
            wkDF30 = dfTEMP30STD[regListTEMP]
            ml = modelTEMPSaved
            whichRes  = residualsTemp
            # Figure the index where to save the images paths
            if doninp == 'LR':
                place = 4
            elif doninp == 'LA':
                place = 5
            else:
                place = 6
                
        elif reginp == 2:
            wkDF = makeDFRT(dfRHSTD, dfTEMPSTD)
            wkDF30 = makeDFRT(dfRH30STD, dfTEMP30STD)
            ml = modelTRHSaved
            whichRes  = residualsRhTemp
            # Figure the index where to save the images paths
            if doninp == 'LR':
                place = 8
            elif doninp == 'LA':
                place = 9
            else:
                place = 10
                
        elif reginp == 3:
            wkDF = makeDFRTH(dfRAINSTD, dfTEMPSTD, dfRHSTD)
            wkDF30 = makeDFRTH(dfRAIN30STD, dfTEMP30STD, dfRH30STD)
            ml = modelRTRHSaved
            whichRes  = residualsRaRhTemp
            # Figure the index where to save the images paths
            if doninp == 'LR':
                place = 12
            elif doninp == 'LA':
                place = 13
            else:
                place = 14
                
            
        runMLModel(wkDF, wkDF30, doninp, ml)

def makeDFRT(dfR, dfT):
    global rgLsTRH
    res = pd.concat([dfR, dfT], axis=1)
    res = res[rgLsTRH]
    
    return res


def makeDFRTH(dfR, dfT, dfRH):
    global rgLsRTRH
    res = pd.concat([dfR, dfT, dfRH], axis=1)
    res = res[rgLsRTRH]
    
    return res
    
    
def runMLModel(wkDF, wkDF30, code, ml):
    """"""
    if code == 'LR':
        runLR(ml, wkDF, wkDF30)
    elif code == 'LA':
        runLA(ml, wkDF, wkDF30)
    elif code == 'RF':
        runRF(ml, wkDF, wkDF30)
    else:
        print('Error running ML model: ', code)

def runLR(ml, wkDF, wkDF30):
    """"""
    global eriDonLRSaved, resiDONLR, place
    
    modelERI = joblib.load(ml)
    dataPred = modelERI.predict(wkDF)    
    dataPred30 = modelERI.predict(wkDF30)
    
    predERI = getPredERI(dataPred)
    predERI30 = getPredERI(dataPred30)
    
    saveImg(predERI, predERI30)
    
    modelDON = joblib.load(eriDonLRSaved)
    predERI = predERI.values.reshape(-1, 1)
    predERI30 = predERI30.values.reshape(-1, 1)
    
    predDON= modelDON.predict(predERI)    
    predDON30= modelDON.predict(predERI30)    
    
    predDON[predDON<0] = np.nan
    predDON30[predDON30<0] = np.nan
    predDON_noNan = predDON[~np.isnan(predDON)]    
    predDON30_noNan = predDON30[~np.isnan(predDON30)]    
    
    
    predDON = resiDONLR + predDON_noNan.mean()
    predDON30 = resiDONLR + predDON30_noNan.mean()
    
    predDON[predDON<0] = np.nan
    predDON30[predDON30<0] = np.nan
    predDON_noNan = predDON[~np.isnan(predDON)]  
    predDON30_noNan = predDON30[~np.isnan(predDON30)] 
    
    
    saveDONImgs(predDON_noNan, predDON30_noNan, 3)

def runLA(ml, wkDF, wkDF30):
    """"""
    global eriDonLASaved, resiDONLA, place
    
    modelERI = joblib.load(ml)
    dataPred = modelERI.predict(wkDF)    
    dataPred30 = modelERI.predict(wkDF30)
    
    predERI = getPredERI(dataPred)
    predERI30 = getPredERI(dataPred30)
    
    
    saveImg(predERI, predERI30)
    
    modelDON = joblib.load(eriDonLASaved)
    
    predERI = predERI.values.reshape(-1, 1)
    predERI30 = predERI30.values.reshape(-1, 1)
    
    predDON= modelDON.predict(predERI)    
    predDON30= modelDON.predict(predERI30)    
    
    predDON[predDON<0] = np.nan
    predDON30[predDON30<0] = np.nan
    predDON_noNan = predDON[~np.isnan(predDON)]    
    predDON30_noNan = predDON30[~np.isnan(predDON30)]    
    
    predDON = resiDONLA + predDON_noNan.mean()
    predDON30 = resiDONLA + predDON30_noNan.mean()
    
    predDON[predDON<0] = np.nan
    predDON30[predDON30<0] = np.nan
    predDON_noNan = predDON[~np.isnan(predDON)]  
    predDON30_noNan = predDON30[~np.isnan(predDON30)] 
    saveDONImgs(predDON_noNan, predDON30_noNan, 7)

def runRF(ml, wkDF, wkDF30):
    """"""
    global eriDonRFSaved, resiDONRF, place
    
    modelERI = joblib.load(ml)
    dataPred = modelERI.predict(wkDF)    
    dataPred30 = modelERI.predict(wkDF30)
    
    
    predERI = getPredERI(dataPred) 
    predERI30 = getPredERI(dataPred30)
    
    
    saveImg(predERI, predERI30)
    
    modelDON = joblib.load(eriDonRFSaved)
    predERI = predERI.values.reshape(-1, 1)
    predERI30 = predERI30.values.reshape(-1, 1)
    
    predDON= modelDON.predict(predERI)    
    predDON30= modelDON.predict(predERI30)    
    
    predDON[predDON<0] = np.nan
    predDON30[predDON30<0] = np.nan
    predDON_noNan = predDON[~np.isnan(predDON)]    
    predDON30_noNan = predDON30[~np.isnan(predDON30)]    
    
    predDON = resiDONRF + predDON_noNan.mean()
    predDON30 = resiDONRF + predDON30_noNan.mean()
    
    predDON[predDON<0] = np.nan
    predDON30[predDON30<0] = np.nan
    predDON_noNan = predDON[~np.isnan(predDON)]  
    predDON30_noNan = predDON30[~np.isnan(predDON30)] 
    saveDONImgs(predDON, predDON30_noNan, 11)

def getPredERI(data):
    """"""
    global whichRes
    a = pd.DataFrame(data)
    mn = a.loc[:,0].median()
    predERI = whichRes['values'] + mn #data.mean()
    predERI[predERI<0] = np.nan
    predERI[predERI>100] = np.nan
    predERI = predERI[~np.isnan(predERI)]    
    return predERI


def saveImg(array, array30):
    """
    
    """
    global dfSavingDisp, mdlDir, place, dfRH30, dfRAIN30, dfTEMP30
    
    # filter array
    array[array<0] = np.nan
    array[array>100] = np.nan
    array = array[~np.isnan(array)]  
    

    array30[array30<0] = np.nan
    array30[array30>100] = np.nan
    array30 = array30[~np.isnan(array30)]    
    
    data = list([array, array30])
    
    arrayDf = pd.DataFrame(array)
    arrayDf = arrayDf.reset_index(drop=1)
    
    arrayDf30 = pd.DataFrame(array30)
    arrayDf30 = arrayDf30.reset_index(drop=1)    
    
    figs = []
    dfls = []
    
    xlabel = "ERI X-predicted"
    ylabel = "ERI Y-predicted"    
    
    if place == 0:
        figName = "ERI_RAIN_LR"
        figTitle = "ERI Predicted with Rainfall (Linear Regression)"
        savedName = mdlDir + "\\" + figName
        fig0 = plt.figure(figsize=(15, 9))
        fig0.suptitle(figTitle)     
        figs.append(fig0)
    elif place == 1:
        figName = "ERI_RAIN_LA"
        figTitle = "ERI Predicted with Rainfall (Lasso)"
        savedName = mdlDir + "\\" + figName
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
    elif place == 2:
        figName = "ERI_RAIN_RF"
        figTitle = "ERI Predicted with Rainfall (Random Forest)"
        savedName = mdlDir + "\\" + figName
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
    
    elif place == 4:
        figName = "ERI_TEMP_LR"
        figTitle = "ERI Predicted with Temperature (Linear Regression)"
        savedName = mdlDir + "\\" + figName
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
    elif place == 5:
        figName = "ERI_TEMP_LA"
        figTitle = "ERI Predicted with Temperature (Lasso)"
        savedName = mdlDir + "\\" + figName
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
    elif place == 6:
        figName = "ERI_TEMP_RF"
        figTitle = "ERI Predicted with Temperature (Random Forest)"
        savedName = mdlDir + "\\" + figName
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
    
    elif place == 8:
        figName = "ERI_RHTEMP_LR"
        figTitle = "ERI Predicted with RH-Temp. (Linear Regression)"
        savedName = mdlDir + "\\" + figName
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
    elif place == 9:
        figName = "ERI_RHTEMP_LA"
        figTitle = "ERI Predicted with RH-Temp. (Lasso)"
        savedName = mdlDir + "\\" + figName
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
    elif place == 10:
        figName = "ERI_RHTEMP_RF"
        figTitle = "ERI Predicted with RH-Temp. (Random Forest)"
        savedName = mdlDir + "\\" + figName   
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
    
    elif place == 12:
        figName = "ERI_R-RH-TEMP_LR"
        figTitle = "ERI Predicted with R-RH-Temp. (Linear Regression)"
        savedName = mdlDir + "\\" + figName
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
        
    elif place == 13:
        figName = "ERI_R-RH-TEMP_LA"
        figTitle = "ERI Predicted with R-RH-Temp. (Lasso)"
        savedName = mdlDir + "\\" + figName
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
    elif place == 14:
        figName = "ERI_R-RH-TEMP_RF"
        figTitle = "ERI Predicted with R-RH-Temp. (Random Forest)"
        savedName = mdlDir + "\\" + figName    
        fig1= plt.figure(figsize=(15, 9))
        fig1.suptitle(figTitle)     
        figs.append(fig1)
    else:
        print("Error in saving risk analysis ERI or DON at index ", place)  
        
    # Create a figure instance
    #plt.ylim(0, 100)
    columns = ['Forecast', 'Climatology']
    for fgi in figs:
        # Create an axes instance
        axi = fgi.add_subplot(111)
        axi.set_xlabel(xlabel)
        axi.set_ylabel(ylabel)
        #axi.set_title(figTitle)
        # Create the boxplot
        axi.set_xticklabels(columns)
        axi.boxplot(data)    
        plt.savefig(savedName)        
      
    
    dfSavingDisp['MLAnalysis'].iloc[place] = savedName
    print('Successfully saved: ', figName)
        
def saveDONImgs(array, array30, doncode):
    """"""
    global dfSavingDisp, mdlDir
    
    # filter array
    array[array<0] = np.nan
    array30[array30<0] = np.nan
    array[array>100] = np.nan
    array30[array30>100] = np.nan
    array = array[~np.isnan(array)]  
    array30 = array30[~np.isnan(array30)]  
    
    data = list([array, array30])
    
    arrayDf = pd.DataFrame(array)
    arrayDf = arrayDf.reset_index(drop=1)
    
    arrayDf30 = pd.DataFrame(array30)
    arrayDf30 = arrayDf30.reset_index(drop=1)   
    
    figs = []
    plt.ylim(0, 10)
    if doncode == 3:
        figName = "ERI_DON_LR"
        figTitle = "DON Predicted with ERI (Linear Regression)"
        xlabel = "ERI X-predicted"
        ylabel = "ERI Y-predicted"
        savedName = mdlDir + "\\" + figName  
        
        fig3 = plt.figure(figsize=(15, 9))
        fig3.suptitle(figTitle) 
        figs.append(fig3)
            
    elif doncode == 7:
        figName = "ERI_DON_LA"
        figTitle = "DON Predicted with ERI (Lasso)"
        xlabel = "ERI X-predicted"
        ylabel = "ERI Y-predicted"
        savedName = mdlDir + "\\" + figName   
        
        fig7= plt.figure(figsize=(15, 9))
        fig7.suptitle(figTitle) 
        figs.append(fig7)
        
    elif doncode == 11:
        figName = "ERI_DON_RF"
        figTitle = "DON Predicted with ERI (Random Forest)"
        xlabel = "ERI X-predicted"
        ylabel = "ERI Y-predicted"
        savedName = mdlDir + "\\" + figName  
        
        fig11= plt.figure(figsize=(15, 9))
        fig11.suptitle(figTitle) 
        figs.append(fig11)   
        
    # Create a figure instance

    columns = ['Forecast', 'Climatology']    
    for fgi in figs:
        # Create an axes instance
        axi = fgi.add_subplot(111)
        axi.set_xlabel(xlabel)
        axi.set_ylabel(ylabel)
        # Create the boxplot
        axi.set_xticklabels(columns)
        axi.boxplot(data)    
        plt.savefig(savedName)      
    
    
    dfSavingDisp['MLAnalysis'].iloc[doncode] = savedName
    print('Successfully saved: ', figName)        
    
    
def plotProb(problist, problist30):

    global dfSavingDisp, mdlDir, station, genMode
    
    if genMode == 0:
        div = 200
        iterN = 4
    else:
        div = 100
        iterN = 2
    
    forcTempList = []
    climTempList = []    
    
    forcRainList = []
    climRainList = []      
    
    forcRhList = []
    climRhList = []    
    
    forcRhTpList = []
    climRhTpList = []          
    
    for that in range(iterN):
        dfi = problist[that]
        cols = dfi.columns.tolist()
        for coli in cols:
            onescount = dfi.pivot_table(index=[coli], aggfunc='size')    
            try:
                onescount = onescount[1]
            except KeyError:
                onescount = 0
            finally:
                onescount = onescount/div                 
                if that == 0:
                    forcRainList.append(onescount)      
                elif that == 1:
                    forcTempList.append(onescount)      
                elif that == 2:
                    if genMode == 0:
                        forcRhList.append(onescount) 
                elif that == 3:
                    if genMode == 0:
                        forcRhTpList.append(onescount)             
                
    for that30 in range(iterN):
        dfi30 = problist30[that30]
        cols30 = dfi30.columns.tolist()
        for coli30 in cols30:
            onescount = dfi30.pivot_table(index=[coli30], aggfunc='size')    
            try:
                onescount = onescount[1]
            except KeyError:
                onescount = 0
            finally:
                onescount = onescount/30 
                if that30 == 0:
                    climRainList.append(onescount)      
                elif that30 == 1:
                    climTempList.append(onescount)      
                elif that30 == 2:
                    if genMode == 0:
                        climRhList.append(onescount) 
                elif that30 == 3:
                    if genMode == 0:
                        climRhTpList.append(onescount)  
                
    
    wkcols= ['JnW1', 'JnW2', 'JnW3', 'JnW4', 'JlW1', 'JlW2', 'JlW3', 'JlW4', 'AgW1', 'AgW2', 'AgW3', 'AgW4', 'SpW1', 'SpW2', 'SpW3', 'SpW4',]
    dfRainToPlot = pd.DataFrame({'forecast': forcRainList, 'climatology': climRainList}, index=wkcols)  
    dfTempToPlot = pd.DataFrame({'forecast': forcTempList, 'climatology': climTempList}, index=wkcols) 
    if genMode == 0:
        dfRhToPlot = pd.DataFrame({'forecast': forcRhList, 'climatology': climRhList}, index=wkcols)  
        dfRhTpToPlot = pd.DataFrame({'forecast': forcRhTpList, 'climatology': climRhTpList}, index=wkcols)     
    
    fg1, ax1 = plt.subplots()
    fg2, ax2 = plt.subplots()
    fg3, ax3 = plt.subplots()
    fg4, ax4 = plt.subplots()
    
    figName1 = mdlDir + "\\" + 'Probabilities_Rain'
    figName2 = mdlDir + "\\" + 'Probabilities_Temp'
    figName3 = mdlDir + "\\" + 'Probabilities_RH'
    figName4 = mdlDir + "\\" + 'Probabilities_RH-Temp'
    
    ax1 = dfRainToPlot.plot.bar(rot=0, figsize=(15,9))
    ax1.set_title('Probalities Bar - Rainfall ({})'.format(station))
    ax1.set_ylabel('Probabilities')
    ax1.set_xlabel('Number of Weeks (June - September)')
    plt.savefig(figName1) 
    
    ax2 = dfTempToPlot.plot.bar(rot=0, figsize=(15,9))
    ax2.set_title('Probalities Bar - Temperature ({})'.format(station))
    ax2.set_ylabel('Probabilities')
    ax2.set_xlabel('Number of Weeks (June - September)')
    plt.savefig(figName2) 
    
    dfSavingDisp['MLAnalysis'].iloc[15] = figName1
    dfSavingDisp['MLAnalysis'].iloc[16] = figName2    
    
    if genMode == 0:
        ax3 = dfRhToPlot.plot.bar(rot=0, figsize=(15,9))
        ax3.set_title('Probalities Bar - Relative Humidity ({})'.format(station))
        ax3.set_ylabel('Probabilities')
        ax3.set_xlabel('Number of Weeks (June - September)')
        plt.savefig(figName3) 
        
        ax4 = dfRhTpToPlot.plot.bar(rot=0, figsize=(15,9))
        ax4.set_title('Probalities Bar - Rel.Hum and Temp. ({})'.format(station))
        ax4.set_ylabel('Probabilities')
        ax4.set_xlabel('Number of Weeks (June - September)')
        plt.savefig(figName4)     
        
    
        dfSavingDisp['MLAnalysis'].iloc[17] = figName3
        dfSavingDisp['MLAnalysis'].iloc[18] = figName4        
    
    