#Author: Eunjin Han
#Institute: IRI-Columbia University
#Date: 5/25/2019
#Purpose: Improve Richardson(1984)'s weather generator so as to
# 1) link with seasonal climate forecast provided in tercile format for each individual month (e.g., PAGASA's)
# 2) parameters of rainfall model and temperature/srad are estimated by resampling "n" years of weather data
#########################
# (a) call WGEN-PAR function to generate input file for WGEN main function
# (b) run WGEN executable generated by Fortran compiler
# (c) call bias-correction funtion for post-processing
#########################
import numpy as np
import math
import scipy
import pandas as pd
import calendar
from scipy import stats
from scipy.stats import kurtosis, skew
from numpy.linalg import inv
from scipy.stats import gamma
from scipy.optimize import curve_fit
import time
import os   #operating system
import subprocess  #to run executable
import matplotlib.pyplot as plt
from Estimate_target import Estimate_Monthly_target ## function to get monthly target values
from WGEN_PAR_resampled_monthly_36 import WGEN_PAR

#start_time = time.clock()

def WGEN_MAIN (WTD_df,df_scf, wdir, station_name, ALAT, target_year):
    #================== MAIN PROGRAM ======================================
    print( '==WGEN_MAIN: START WEATHER GENERATION PROCEDURES')
    #1) read observed weather *.WTD (skip 1st row - heading)
    #obs_file = r'C:\IRI\WGEN_Richardson\FResampler_Python\PILI.WTD'
    # WTD_fname = r'C:\Users\Eunjin\IRI\PH_FAO\WGEN_PH_36\SANJ.WTD'
    # data1 = np.loadtxt(WTD_fname,skiprows=1)
    # #convert numpy array to dataframe
    # WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
    #               'DOY':data1[:,0].astype(int)%1000,
    #               'SRAD':data1[:,1],
    #               'TMAX':data1[:,2],
    #               'TMIN':data1[:,3],
    #               'RAIN':data1[:,4]})
    year_array = WTD_df.YEAR.unique()
    nyears = year_array.shape[0]

    # #2) MONTHLY tercile probability forecasts for 12 months
    # frst_fname = r'C:\Users\Eunjin\IRI\PH_FAO\WGEN_PH_36\SCF_input_monthly.csv'
    # #read *.csv data into a dataframe
    # df_scf = pd.read_csv(frst_fname, header = 0)
    # #===================================================
    # station_name = 'SANJ'   #===> Hard coded for now  
    # ALAT = 12.35 #latitude of San Jose weather station   #===> Hard coded
    # target_year =2012    #===> Hard coded
    # wdir = "C:\\Users\\Eunjin\\IRI\\PH_FAO\\WGEN_PH_36\\"  #===> Hard coded

    # (a) call WGEN-PAR function to generate input file for WGEN main function
    print( '==WGEN_MAIN:COMPUTE WGEN INPUT PARAMETERS AND CREATE   WGEN_input_param_month_####.txt FILE ')
    #print (time.clock() - start_time, "sec - start to call WGEN_PAR function ")
    #WGEN_PAR(WTD_df,df_scf,wdir, station_name, ALAT, target_year)
    #EJ(4/25/2019) return monthly values from the observed WTD for bias correction
    rain_WTD_m , tmin_WTD_m, tmax_WTD_m, srad_WTD_m, tmax_resampled_m, tmin_resampled_m, srad_resampled_m = WGEN_PAR(WTD_df,df_scf,wdir, station_name, ALAT, target_year)

    # (b) run WGEN executable generated by Fortran compiler
    wdir_new = wdir.replace("/","\\")+'\\' 
    os.chdir(wdir_new) #change directory to keep WGEN output file int the target directory
    #os.chdir(wdir) #change directory
    #param_txt = wdir + 'WGEN_input_param_month_' + station_name+ '.txt'
    param_txt = wdir.replace("/","\\") + '\\WGEN_input_param_month_' + station_name+ '.txt'
    print( '==WGEN_MAIN: RUN WGEN PROGRAM Fortran executable')
    if os.path.isfile(param_txt):
        #args = "WGEN_PH.exe " + param_txt
        args = wdir.replace("/","\\") + '\\WGEN_PH_DCC.exe ' + param_txt
        
        #===Run executable with argument
        subprocess.call(args) #, stdout=FNULL, stderr=FNULL, shell=False)
        #create a new folder to save outputs of the target scenario

    #================================================
    # Apply bias correction (shift cdf of WGEN output realizations 
    #  to theoretical (based on rainfall SCF) and the cdf of 500 resampled (tmin,Tmax and srad))
    #3) read WGEN produced weather *.txt (skip 1st row - heading)
    #2) read WGEN-generated weather realizations
    #===================================================
    m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
    m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365] #ending date of each month for regular years
    m_doys_list2 = [1,32,61,92,122,153,183,214,245,275,306,336]  #starting date of each month for leap years
    #m_doye_list2 = [31,60,91,121,152,182,213,244,274,305,335,366] #ending date of each month for leap years
    m_doye_list2 = [31,59,91,121,152,182,213,244,274,305,335,366] #ignore 29th of Feb in case of leap year
    numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
    #======================================================================
    print( '==WGEN_MAIN: AFTER WGEN, start Bias correction -  please wait....')
    #print (time.clock() - start_time, "sec - start Bias correction")
    #WGEN_fname =  wdir + '\\WGEN_out_' + station_name+ repr(target_year)+'.txt'
    WGEN_fname =  wdir.replace("/","\\") + '\\WGEN_out_' + station_name+ repr(target_year)+'.txt'

    ## Original statement by Dr. Han
    data2 = np.loadtxt(WGEN_fname,skiprows=1)
    ## Edited by Josue K
    # data2 = np.genfromtxt(WGEN_fname, delimiter=' ', skip_header =1)
    # data2 = np.loadtxt(WGEN_fname, delimiter=' ',skiprows =1)

    #convert numpy array to dataframe
    df_WGEN = pd.DataFrame({'i_yr':data2[:,0],    
            'YEAR':data2[:,1],
            'DOY':data2[:,2],
            'SRAD':data2[:,3],
            'TMAX':data2[:,4],
            'TMIN':data2[:,5],
            'RAIN':data2[:,6]})
    year_array = df_WGEN.i_yr.unique()
    nrealiz = year_array.shape[0]

    #empty array to save monthly sum/mean variables to be corrected
    rain_WGEN_m = np.empty((nrealiz,12,))*np.NAN  #nrealiz *  12 months
    tmin_WGEN_m = np.empty((nrealiz,12,))*np.NAN 
    tmax_WGEN_m = np.empty((nrealiz,12,))*np.NAN  
    srad_WGEN_m = np.empty((nrealiz,12,))*np.NAN      
    #empty array to save correction factors for each year & month
    tmax_c = np.empty((nrealiz,12,))*0.0  #by default 0
    tmin_c = np.empty((nrealiz,12,))*0.0   
    srad_c = np.empty((nrealiz,12,))*0.0   
    rain_c = np.empty((nrealiz,12,))+1.0 #by default 1 because its multiplication factor

    for i in range(0,12):  #12 months
        if calendar.isleap(target_year):  #if leap year, ignore Feb. 29 when compute monthly values.
            target_doy1 = m_doys_list2[i]
            target_doy2 = m_doye_list2[i]
        else:
            target_doy1 = m_doys_list[i]
            target_doy2 = m_doye_list[i] 
        
        for j in range(0,nrealiz): #loop for extracting a matrix [n realization * n years]
            Tmin_i = df_WGEN.TMIN[(df_WGEN["i_yr"] == (j+1)) & (df_WGEN["DOY"] >= target_doy1) & (df_WGEN["DOY"] <= target_doy2)].values
            Tmax_i = df_WGEN.TMAX[(df_WGEN["i_yr"] == (j+1)) & (df_WGEN["DOY"] >= target_doy1) & (df_WGEN["DOY"] <= target_doy2)].values
            Srad_i = df_WGEN.SRAD[(df_WGEN["i_yr"] == (j+1)) & (df_WGEN["DOY"] >= target_doy1) & (df_WGEN["DOY"] <= target_doy2)].values
            rain_i = df_WGEN.RAIN[(df_WGEN["i_yr"] == (j+1)) & (df_WGEN["DOY"] >= target_doy1) & (df_WGEN["DOY"] <= target_doy2)].values

            rain_WGEN_m[j, i] = np.sum(rain_i)
            tmin_WGEN_m[j, i] = np.mean(Tmin_i)
            tmax_WGEN_m[j, i] = np.mean(Tmax_i) 
            srad_WGEN_m[j, i] = np.mean(Srad_i)

        #=================================================================
        #=== 1) Rainfall
        rain_temp_WTD = rain_WTD_m[:, i]
        sorted_rain_WTD = np.sort(rain_temp_WTD) #, axis=0) #sort monthly rain from smallest to largest
    #index_rain_WTD = np.argsort(rain_temp_WTD) #** argsort - returns the original indexes of the sorted array

        #a) compute a theoreticalCDF curve from BN:NN:AN values
        BN = df_scf.iloc[0][i+1]  #e.g., 36%
        NN = df_scf.iloc[1][i+1]   #e.g., 33%
        AN = df_scf.iloc[2][i+1]     #e.g., 31%
        fx_theo = np.empty(nyears)*np.nan #initialize
        Fx_theo = np.empty(nyears)*np.nan  #initialize
        #calculate number of years for each 1/3 section (i.e., 33-34-33 for 30 yrs of obs)
        num_13 = math.floor(nyears/3) #This method returns largest integer not greater than x.
        num_1 = float(num_13)
        num_2 = float(num_13)
        num_3 = float(num_13)
        if nyears % 3 == 1:
            num_2+1.0
        elif nyears % 3 == 2:
            num_1+1.0
            num_3+1.0

        for ii in range(0,nyears):
            #compute PDF
            if ii < num_1:
                fx_theo[ii] = (BN/100)/num_1
            elif ii >= num_1 and ii < (nyears - num_3):
                fx_theo[ii] = (NN/100)/num_2
            else:
                fx_theo[ii] = (AN/100)/num_3
                
        Fx_theo = np.cumsum(fx_theo)  #compute CDF

        #b) compute CDF curve from the WGEN-generated
        rain_temp_WGEN = rain_WGEN_m[:, i]
        sorted_rain_WGEN = np.sort(rain_temp_WGEN) #sort monthly rain from smallest to largest
        index_rain_WGEN = np.argsort(rain_temp_WGEN) #** argsort - returns the original indexes of the sorted array
        temp = np.zeros(len(rain_temp_WGEN))+1
        pdf2 = np.divide(temp,len(rain_temp_WGEN ))  #1/100years 
        cdf2 = np.cumsum(pdf2)  #compute CDF
        #c) CDF matching for bias correction
        if Fx_theo[0] > cdf2[0]:  #if the first CDF is greater than the minimum target percentile (5%), then replace the minimum value
            corrected_rain=np.interp(cdf2,Fx_theo,sorted_rain_WTD,left=sorted_rain_WTD[0])# 100 WGEN outputs to theoretical CDF SCF curve
        else:
            corrected_rain=np.interp(cdf2,Fx_theo,sorted_rain_WTD,left=0.0)# 100 WGEN outputs to theoretical CDF SCF curve

        # fig = plt.figure()
        # fig.suptitle('Bias correction of monthly Rainfall', fontsize=14, fontweight='bold')
        # ax = fig.add_subplot(111)
        # ax.set_xlabel('Monthly Rainfall[mm/month]',fontsize=14)
        # ax.set_ylabel('CDF',fontsize=14)
        # ax.plot(sorted_rain_WGEN,cdf2, '--*',label='WGEN')
        # ax.plot(corrected_rain,cdf2, 'o',label='corrected')
        # ax.plot(sorted_rain_WTD,Fx_theo, '--x',label='theoretical')
        # legend = ax.legend(loc='lower right', shadow=True, fontsize='large')
        # plt.show()
        #=================================================================
        #=== 2) Tmin
        #a) compute CDF curve from the resampled (target cdf to correct)
        tmin_temp_RES = tmin_resampled_m[:, i]
        sorted_tmin_RES = np.sort(tmin_temp_RES) #sort monthly rain from smallest to largest
        temp = np.zeros(len(tmin_temp_RES))+1
        pdf = np.divide(temp,len(tmin_temp_RES))  #1/100years 
        cdf = np.cumsum(pdf)  #compute CDF
        #b) compute CDF curve from the WGEN-generated
        tmin_temp_WGEN = tmin_WGEN_m[:, i]
        sorted_tmin_WGEN = np.sort(tmin_temp_WGEN) #sort monthly rain from smallest to largest
        index_tmin_WGEN = np.argsort(tmin_temp_WGEN) #** argsort - returns the original indexes of the sorted array
        temp = np.zeros(len(tmin_temp_WGEN))+1
        pdf2 = np.divide(temp,len(tmin_temp_WGEN))  #1/100years 
        cdf2 = np.cumsum(pdf2)  #compute CDF
        #c) CDF matching for bias correction
        corrected_tmin=np.interp(cdf2,cdf,sorted_tmin_RES,left=0.0)# 100 WGEN outputs to CDF of 500 resampled based on SCF

        # #check how CDF of Tmin from resampled are different from climatollogy (*.wTD)
        # tmin_temp_WTD = tmin_WTD_m[:, i]
        # sorted_tmin_WTD = np.sort(tmin_temp_WTD) 
        # temp = np.zeros(len(tmin_temp_WTD))+1
        # pdf_WTD = np.divide(temp,len(tmin_temp_WTD))   
        # cdf_WTD = np.cumsum(pdf_WTD)  #compute CDF
        # fig = plt.figure()
        # fig.suptitle('Bias correction of monthly Tmin', fontsize=14, fontweight='bold')
        # ax = fig.add_subplot(111)
        # ax.set_xlabel('Monthly avg Tmin [C]',fontsize=14)
        # ax.set_ylabel('CDF',fontsize=14)
        # ax.plot(sorted_tmin_WGEN,cdf2, '--*',label='WGEN')
        # ax.plot(sorted_tmin_RES,cdf, '--^',label='Resampled')
        # ax.plot(sorted_tmin_WTD,cdf_WTD, '--x',label='WTD')
        # ax.plot(corrected_tmin,cdf2, 'o',label='corrected')
        # legend = ax.legend(loc='lower right', shadow=True, fontsize='large')
        # plt.show()
        #=================================================================
        #=== 3) Tmax
        #a) cvompute CDF curve from the resampled (target cdf to correct)
        tmax_temp_RES = tmax_resampled_m[:, i]
        sorted_tmax_RES = np.sort(tmax_temp_RES) #sort monthly rain from smallest to largest
        temp = np.zeros(len(tmax_temp_RES))+1
        pdf = np.divide(temp,len(tmax_temp_RES))  #1/100years 
        cdf = np.cumsum(pdf)  #compute CDF
        #b) compute CDF curve from the WGEN-generated
        tmax_temp_WGEN = tmax_WGEN_m[:, i]
        sorted_tmax_WGEN = np.sort(tmax_temp_WGEN) #sort monthly rain from smallest to largest
        index_tmax_WGEN = np.argsort(tmax_temp_WGEN) #** argsort - returns the original indexes of the sorted array
        temp = np.zeros(len(tmax_temp_WGEN))+1
        pdf2 = np.divide(temp,len(tmax_temp_WGEN))  #1/100years 
        cdf2 = np.cumsum(pdf2)  #compute CDF
        #c) CDF matching for bias correction
        corrected_tmax=np.interp(cdf2,cdf,sorted_tmax_RES,left=0.0)# 100 WGEN outputs to CDF of 500 resampled based on SCF

        # #check how CDF of Tmax from resampled are different from climatollogy (*.wTD)
        # tmax_temp_WTD = tmax_WTD_m[:, i]
        # sorted_tmax_WTD = np.sort(tmax_temp_WTD) 
        # temp = np.zeros(len(tmax_temp_WTD))+1
        # pdf_WTD = np.divide(temp,len(tmax_temp_WTD))   
        # cdf_WTD = np.cumsum(pdf_WTD)  #compute CDF
        # fig = plt.figure()
        # fig.suptitle('Bias correction of monthly tmax', fontsize=14, fontweight='bold')
        # ax = fig.add_subplot(111)
        # ax.set_xlabel('Monthly avg tmax [C]',fontsize=14)
        # ax.set_ylabel('CDF',fontsize=14)
        # ax.plot(sorted_tmax_WGEN,cdf2, '--*',label='WGEN')
        # ax.plot(sorted_tmax_RES,cdf, '--^',label='Resampled')
        # ax.plot(sorted_tmax_WTD,cdf_WTD, '--x',label='WTD')
        # ax.plot(corrected_tmax,cdf2, 'o',label='corrected')
        # legend = ax.legend(loc='lower right', shadow=True, fontsize='large')
        # plt.show()

        #=================================================================
        #=== 4) SRad
        #a) compute CDF curve from the resampled (target cdf to correct)
        srad_temp_RES = srad_resampled_m[:, i]
        sorted_srad_RES = np.sort(srad_temp_RES) #sort monthly rain from smallest to largest
        temp = np.zeros(len(srad_temp_RES))+1
        pdf = np.divide(temp,len(srad_temp_RES))  #1/100years 
        cdf = np.cumsum(pdf)  #compute CDF
        #b) compute CDF curve from the WGEN-generated
        srad_temp_WGEN = srad_WGEN_m[:, i]
        sorted_srad_WGEN = np.sort(srad_temp_WGEN) #sort monthly rain from smallest to largest
        index_srad_WGEN = np.argsort(srad_temp_WGEN) #** argsort - returns the original indexes of the sorted array
        temp = np.zeros(len(srad_temp_WGEN))+1
        pdf2 = np.divide(temp,len(srad_temp_WGEN))  #1/100years 
        cdf2 = np.cumsum(pdf2)  #compute CDF
        #c) CDF matching for bias correction
        corrected_srad=np.interp(cdf2,cdf,sorted_srad_RES,left=0.0)# 100 WGEN outputs to CDF of 500 resampled based on SCF

        #  #check how CDF of srad from resampled are different from climatollogy (*.wTD)
        # srad_temp_WTD = srad_WTD_m[:, i]
        # sorted_srad_WTD = np.sort(srad_temp_WTD) 
        # temp = np.zeros(len(srad_temp_WTD))+1
        # pdf_WTD = np.divide(temp,len(srad_temp_WTD))   
        # cdf_WTD = np.cumsum(pdf_WTD)  #compute CDF
        # fig = plt.figure()
        # fig.suptitle('Bias correction of monthly srad', fontsize=14, fontweight='bold')
        # ax = fig.add_subplot(111)
        # ax.set_xlabel('Monthly avg srad [C]',fontsize=14)
        # ax.set_ylabel('CDF',fontsize=14)
        # ax.plot(sorted_srad_WGEN,cdf2, '--*',label='WGEN')
        # ax.plot(sorted_srad_RES,cdf, '--^',label='Resampled')
        # ax.plot(sorted_srad_WTD,cdf_WTD, '--x',label='WTD')
        # ax.plot(corrected_srad,cdf2, 'o',label='corrected')
        # legend = ax.legend(loc='lower right', shadow=True, fontsize='large')
        # plt.show() 
        #=================================================================

        # d) Save the correction factors to the dataframe
        sorted_rain_WGEN[sorted_rain_WGEN == 0.0] = 0.1  #replace 0 with 0.1 to avoid dividing by zero 
        for j in range(0,nrealiz):
            rain_c[index_rain_WGEN[j],i] = corrected_rain[j]/sorted_rain_WGEN[j]  #multiplication factor
            tmin_c[index_tmin_WGEN[j],i] = corrected_tmin[j]-sorted_tmin_WGEN[j] 
            tmax_c[index_tmax_WGEN[j],i] = corrected_tmax[j]-sorted_tmax_WGEN[j]  
            srad_c[index_srad_WGEN[j],i] = corrected_srad[j]-sorted_srad_WGEN[j]   
    #==================
    #5)Write the bias-corrected daily output into a text file
    #==================
    #===================================================
    m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
    m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365] #ending date of each month for regular years
    m_doys_list2 = [1,32,61,92,122,153,183,214,245,275,306,336]  #starting date of each month for leap years
    m_doye_list2 = [31,60,91,121,152,182,213,244,274,305,335,366] #ending date of each month for leap years
    #m_doye_list2 = [31,59,91,121,152,182,213,244,274,305,335,366] #ignore 29th of Feb in case of leap year
    #numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
    #======================================================================
    print( '==WGEN_MAIN: Start to saving bias corrected WGEN-ouput into df_WGEN_bcorr dataframe')
    #print (time.clock() - start_time, "sec- before saving bias corrected WGEN-ouput into df_WGEN_bcorr dataframe")
    # fWGEN =  wdir + 'WGEN_out_' + station_name+ repr(target_year)+'_bc.txt'
    # f_rz = open(fWGEN,"w") #opens file
    # f_rz.write(" i_yr YEAR DOY SRAD TMAX TMIN RAIN \n")
    d_count = 0   #1 to 365*100 realizations
    ##replace daily data in the target month with the shifted ones
    for i in range(0,nrealiz):
        for j in range(12): #12 months
            if calendar.isleap(target_year):  #if leap year, ignore Feb. 29 when compute monthly values.
                Month_start_doy = m_doys_list2[j]
                Month_end_doy = m_doye_list2[j]
                nday = 366
            else:
                Month_start_doy = m_doys_list[j]
                Month_end_doy = m_doye_list[j]
                nday = 365

            index_sdate = i*nday + Month_start_doy-1
            index_edate = i*nday + Month_end_doy
            df_WGEN.iloc[index_sdate:index_edate,3] = df_WGEN.iloc[index_sdate:index_edate,3] + srad_c[i,j]
            df_WGEN.iloc[index_sdate:index_edate,4] = df_WGEN.iloc[index_sdate:index_edate,4] + tmax_c[i,j]
            df_WGEN.iloc[index_sdate:index_edate,5] = df_WGEN.iloc[index_sdate:index_edate,5] + tmin_c[i,j]
            df_WGEN.iloc[index_sdate:index_edate,6] = df_WGEN.iloc[index_sdate:index_edate,6] * rain_c[i,j]
            d_count = d_count + 1 

    #=========================================================
    #EJ(6/25/2019) To prevent cases of Tmax > Tmin or Tmax == Tmin
    #add a new column "Tmax - Tmin"
    df_WGEN['T_diff'] = df_WGEN['TMAX'] - df_WGEN['TMIN']
    #swaping if Tmin > Tmax
    #https://stackoverflow.com/questions/38901563/pandas-swap-columns-based-on-condition
    #df["Col3"], df["Col2"] = np.where(df['Col3'].isnull(), [df["Col2"], df["Col3"]], [df["Col3"], df["Col2"] ])
    df_WGEN["TMAX"], df_WGEN["TMIN"] = np.where(df_WGEN['T_diff']< 0, [df_WGEN["TMIN"], df_WGEN["TMAX"]], [df_WGEN["TMAX"], df_WGEN["TMIN"] ])

    #Add 0.1 to Tmax if Tmax = Tmin
    df_WGEN["TMAX"] = np.where(df_WGEN['T_diff'] == 0, df_WGEN["TMAX"]+0.1, df_WGEN["TMAX"])
    df_WGEN.drop(columns=['T_diff'])
    #print(df_WGEN)
    #=========================================================
    # for i in range(0,nrealiz):
    #     for j in range(12): #12 months
    #         if calendar.isleap(target_year):  #if leap year, ignore Feb. 29 when compute monthly values.
    #             Month_start_doy = m_doys_list2[j]
    #             Month_end_doy = m_doye_list2[j]
    #         else:
    #             Month_start_doy = m_doys_list[j]
    #             Month_end_doy = m_doye_list[j]

    #         for k in range(Month_start_doy, Month_end_doy+1):
    #             df_WGEN.iloc[d_count,3] = df_WGEN.iloc[d_count,3] + srad_c[i,j]
    #             df_WGEN.iloc[d_count,4] = df_WGEN.iloc[d_count,4] + tmax_c[i,j]
    #             df_WGEN.iloc[d_count,5] = df_WGEN.iloc[d_count,5] + tmin_c[i,j]
    #             df_WGEN.iloc[d_count,6] = df_WGEN.iloc[d_count,6] * rain_c[i,j]
    #             d_count = d_count + 1     
    #print (time.clock() - start_time, "sec - after saving bias corrected WGEN-ouput into df_WGEN_bcorr dataframe")
    print( '==WGEN_MAIN: End of saving bias corrected WGEN-ouput into df_WGEN_bcorr dataframe')

    return df_WGEN
    # df_WGEN_bcorr = df_WGEN.copy()  #make a copy of df_WGEN df to save a bias corrected df
    # for i in range(0,nrealiz):
    #     for j in range(12): #12 months
    #         if calendar.isleap(target_year):  #if leap year, ignore Feb. 29 when compute monthly values.
    #             Month_start_doy = m_doys_list2[j]
    #             Month_end_doy = m_doye_list2[j]
    #         else:
    #             Month_start_doy = m_doys_list[j]
    #             Month_end_doy = m_doye_list[j]

    #         for k in range(Month_start_doy, Month_end_doy+1):
    #             df_WGEN_bcorr.iloc[d_count,0]= df_WGEN.iloc[[d_count]].i_yr.values[0].astype(int)
    #             df_WGEN_bcorr.iloc[d_count,1] = df_WGEN.iloc[[d_count]].YEAR.values[0].astype(int)
    #             df_WGEN_bcorr.iloc[d_count,2] = df_WGEN.iloc[[d_count]].DOY.values[0].astype(int)
    #             df_WGEN_bcorr.iloc[d_count,3] = df_WGEN.iloc[[d_count]].SRAD.values[0] + srad_c[i,j]
    #             df_WGEN_bcorr.iloc[d_count,4] = df_WGEN.iloc[[d_count]].TMAX.values[0] + tmax_c[i,j]
    #             df_WGEN_bcorr.iloc[d_count,5] = df_WGEN.iloc[[d_count]].TMIN.values[0] + tmin_c[i,j]
    #             df_WGEN_bcorr.iloc[d_count,6] = df_WGEN.iloc[[d_count]].RAIN.values[0] * rain_c[i,j]

    #             # df_WGEN_bcorr.iloc[d_count,0]= df_WGEN.iloc[[d_count]].i_yr.values[0].astype(int)
    #             # df_WGEN_bcorr.iloc[d_count,1] = df_WGEN.iloc[[d_count]].YEAR.values[0].astype(int)
    #             # df_WGEN_bcorr.iloc[d_count,2] = df_WGEN.iloc[[d_count]].DOY.values[0].astype(int)
    #             # df_WGEN_bcorr.iloc[d_count,3] = df_WGEN.iloc[[d_count]].SRAD.values[0] + srad_c[i,j]
    #             # df_WGEN_bcorr.iloc[d_count,4] = df_WGEN.iloc[[d_count]].TMAX.values[0] + tmax_c[i,j]
    #             # df_WGEN_bcorr.iloc[d_count,5] = df_WGEN.iloc[[d_count]].TMIN.values[0] + tmin_c[i,j]
    #             # df_WGEN_bcorr.iloc[d_count,6] = df_WGEN.iloc[[d_count]].RAIN.values[0] * rain_c[i,j]

    #             # df_WGEN_bcorr.iloc[d_count,0] = df_WGEN.iloc[[d_count]].i_yr.values[0].astype(int)
    #             # df_WGEN_bcorr.iloc[[d_count]].YEAR = df_WGEN.iloc[[d_count]].YEAR.values[0].astype(int)
    #             # df_WGEN_bcorr.iloc[[d_count]].DOY = df_WGEN.iloc[[d_count]].DOY.values[0].astype(int)
    #             # df_WGEN_bcorr.iloc[[d_count]].SRAD = df_WGEN.iloc[[d_count]].SRAD.values[0] + srad_c[i,j]
    #             # df_WGEN_bcorr.iloc[[d_count]].TMAX = df_WGEN.iloc[[d_count]].TMAX.values[0] + tmax_c[i,j]
    #             # df_WGEN_bcorr.iloc[[d_count]].TMIN = df_WGEN.iloc[[d_count]].TMIN.values[0] + tmin_c[i,j]
    #             # df_WGEN_bcorr.iloc[[d_count]].RAIN = df_WGEN.iloc[[d_count]].RAIN.values[0] * rain_c[i,j] 
    #             #              
    #             # f_rz.write('{0:5d} {1:4d} {2:3d} {3:4.1f} {4:4.1f} {5:4.1f} {6:5.1f}'.format(df_WGEN.iloc[[d_count]].i_yr.values[0].astype(int),
    #             #                                                         df_WGEN.iloc[[d_count]].YEAR.values[0].astype(int),
    #             #                                                         df_WGEN.iloc[[d_count]].DOY.values[0].astype(int),
    #             #                                                         df_WGEN.iloc[[d_count]].SRAD.values[0] + srad_c[i,j],
    #             #                                                         df_WGEN.iloc[[d_count]].TMAX.values[0] + tmax_c[i,j],
    #             #                                                         df_WGEN.iloc[[d_count]].TMIN.values[0] + tmin_c[i,j],
    #             #                                                         df_WGEN.iloc[[d_count]].RAIN.values[0] * rain_c[i,j]))
    #             # f_rz.write("\n")
    #             d_count = d_count + 1        

    # f_rz.close()
    #print( '==WGEN_MAIN: End of wring bias corrected WGEN-ouput into a text file')
    # print (time.clock() - start_time, "- after saving bias corrected WGEN-ouput into df_WGEN_bcorr dataframe")
    # print( '==WGEN_MAIN: End of saving bias corrected WGEN-ouput into df_WGEN_bcorr dataframe')

    # return df_WGEN_bcorr